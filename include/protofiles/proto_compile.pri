# Qt qmake integration with Google Protocol Buffers compiler protoc
#
# To compile protocol buffers with qt qmake, specify PROTOS variable and
# include this file
#
# Example:
# BITSIZE = $$system(getconf LONG_BIT)
# if (contains(BITSIZE, 64)) {
#     LIBS += /usr/lib/x86_64-linux-gnu/libprotobuf.so
# }
# if (contains(BITSIZE, 32)) {
#     LIBS += /usr/lib/libprotobuf.so
# }
# PROTOS = a.proto b.proto
# include(protobuf.pri)
#
# By default protoc looks for .proto files (including the imported ones) in
# the current directory where protoc is run. If you need to include additional
# paths specify the PROTOPATH variable

message("Generating protocol buffer classes from .proto files.")

INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD
#PROTOPATH = protofiles
unix {
    PROTOS = \
        $$PWD/messages_robocup_ssl_geometry_legacy.proto \
        $$PWD/game_event.proto \
        $$PWD/messages_robocup_ssl_detection.proto \
        $$PWD/messages_robocup_ssl_geometry.proto \
        $$PWD/messages_robocup_ssl_refbox_log.proto \
        $$PWD/messages_robocup_ssl_wrapper.proto \
        $$PWD/messages_robocup_ssl_wrapper_legacy.proto \
        $$PWD/rcon.proto \
        $$PWD/referee.proto \
        $$PWD/savestate.proto


    protobuf_decl.name = protobuf headers
    protobuf_decl.input = PROTOS
    protobuf_decl.output = ${QMAKE_FILE_IN_PATH}/${QMAKE_FILE_BASE}.pb.h
    protobuf_decl.commands = protoc --cpp_out=${QMAKE_FILE_IN_PATH} --proto_path=${QMAKE_FILE_IN_PATH} ${QMAKE_FILE_NAME}
    #protobuf_decl.commands = protoc --cpp_out=protofiles --proto_path=${QMAKE_FILE_IN_PATH} ${QMAKE_FILE_NAME}

    protobuf_decl.variable_out = HEADERS
    QMAKE_EXTRA_COMPILERS += protobuf_decl

    protobuf_impl.name = protobuf sources
    protobuf_impl.input = PROTOS
    protobuf_impl.output = ${QMAKE_FILE_IN_PATH}/${QMAKE_FILE_BASE}.pb.cc
    protobuf_impl.depends = ${QMAKE_FILE_IN_PATH}/${QMAKE_FILE_BASE}.pb.h
    protobuf_impl.commands = $$escape_expand(\n)
    protobuf_impl.variable_out = SOURCES
    QMAKE_EXTRA_COMPILERS += protobuf_impl
}

win32 {
    HEADERS += \
        $$PWD/messages_robocup_ssl_geometry_legacy.ph.h \
        $$PWD/game_event.ph.h \
        $$PWD/messages_robocup_ssl_detection.ph.h \
        $$PWD/messages_robocup_ssl_geometry.ph.h \
        $$PWD/messages_robocup_ssl_refbox_log.ph.h \
        $$PWD/messages_robocup_ssl_wrapper.ph.h \
        $$PWD/messages_robocup_ssl_wrapper_legacy.ph.h \
        $$PWD/rcon.ph.h \
        $$PWD/referee.ph.h \
        $$PWD/savestate.ph.h

    SOURCES += \
        $$PWD/messages_robocup_ssl_geometry_legacy.pb.cc \
        $$PWD/game_event.pb.cc \
        $$PWD/messages_robocup_ssl_detection.pb.cc \
        $$PWD/messages_robocup_ssl_geometry.pb.cc \
        $$PWD/messages_robocup_ssl_refbox_log.pb.cc \
        $$PWD/messages_robocup_ssl_wrapper.pb.cc \
        $$PWD/messages_robocup_ssl_wrapper_legacy.pb.cc \
        $$PWD/rcon.pb.cc \
        $$PWD/referee.pb.cc \
        $$PWD/savestate.pb.cc
}
