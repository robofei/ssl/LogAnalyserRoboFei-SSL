/***************************************************************************
 *   Copyright (C) 2020-2021  RoboFEI / Small Size League                       *
 *   Copyright (c) 2013 Robotics Erlangen e.V.                             *
 *   http://www.robotics-erlangen.de/                                      *
 *   info@robotics-erlangen.de                                             *
 *                                                                         *
 *   This file may be licensed under the terms of the GNU General Public   *
 *   License Version 3, as published by the Free Software Foundation.      *
 *   You can find it here: http://www.gnu.org/licenses/gpl.html            *
 *                                                                         *
 ***************************************************************************/

#ifndef MESSAGE_TYPE_H
#define MESSAGE_TYPE_H

enum MessageType
{
    MESSAGE_BLANK = 0,
    MESSAGE_UNKNOWN = 1,
    MESSAGE_SSL_VISION_2010 = 2,
    MESSAGE_SSL_REFBOX_2013 = 3,
    MESSAGE_SSL_VISION_2014 = 4,

    // Not implemented yet, avaliable on go log tools, see
    // <https://github.com/RoboCup-SSL/ssl-go-tools/blob/943cf2c43d5e8f33a0cc380aa347caf7b1858e90/pkg/persistence/message.go#L28>
    MESSAGE_SSL_VISION_TRACKER = 5
};

#endif // MESSAGE_TYPE_H
