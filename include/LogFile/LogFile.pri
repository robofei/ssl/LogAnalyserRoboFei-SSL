###########################################################################
#   Copyright (C) 2020-2021  RoboFEI / Small Size League                       #
#   Copyright (c) 2013 Robotics Erlangen e.V.                             #
#   http://www.robotics-erlangen.de/                                      #
#   info@robotics-erlangen.de                                             #
#                                                                         #
#   This file may be licensed under the terms of the GNU General Public   #
#   License Version 3, as published by the Free Software Foundation.      #
#   You can find it here: http://www.gnu.org/licenses/gpl.html            #
#                                                                         #
###########################################################################

HEADERS += \
    $$PWD/common/logfile.h \
    $$PWD/common/multicastsocket.h \
    $$PWD/common/network.h \
    $$PWD/common/qtiocompressor.h \
    $$PWD/format/file_format.h \
    $$PWD/format/file_format_legacy.h \
    $$PWD/format/file_format_timestamp_type_size_raw_message.h \
    $$PWD/format/message_type.h

SOURCES += \
    $$PWD/common/logfile.cpp \
    $$PWD/common/multicastsocket.cpp \
    $$PWD/common/network.cpp \
    $$PWD/common/qtiocompressor.cpp \
    $$PWD/format/file_format_legacy.cpp \
    $$PWD/format/file_format_timestamp_type_size_raw_message.cpp
