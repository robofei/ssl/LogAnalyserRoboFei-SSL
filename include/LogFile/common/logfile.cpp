/***************************************************************************
 *   Copyright (C) 2020-2021  RoboFEI / Small Size League                       *
 *   Copyright (c) 2013 Robotics Erlangen e.V.                             *
 *   http://www.robotics-erlangen.de/                                      *
 *   info@robotics-erlangen.de                                             *
 *                                                                         *
 *   This file may be licensed under the terms of the GNU General Public   *
 *   License Version 3, as published by the Free Software Foundation.      *
 *   You can find it here: http://www.gnu.org/licenses/gpl.html            *
 *                                                                         *
 ***************************************************************************/

#include "logfile.h"
#include "LogFile/common/qtiocompressor.h"
#include "LogFile/format/file_format_legacy.h"
#include "LogFile/format/file_format_timestamp_type_size_raw_message.h"
#include <QDataStream>
#include <iostream>

const int LogFile::DEFAULT_FILE_FORMAT_VERSION;

LogFile::LogFile(const QString& filename, bool compressed, int formatVersion) :
    m_filename(filename),
    m_compressed(compressed),
    m_formatVersion(formatVersion),
    m_io(nullptr),
    m_file(nullptr),
    m_compressor(nullptr)
{
    addFormat(new FileFormatLegacy());
    addFormat(new FileFormatTimestampTypeSizeRawMessage());
}

LogFile::~LogFile()
{

}

bool LogFile::addFormat(FileFormat* format)
{
    if (m_formatMap.contains(format->version())) {
        qCritical() << "Error adding log format!" << '\n';
        qWarning()<< "Format version " << format->version() << " has been used twice." << '\n';

        return false;
    }

    m_formatMap[format->version()].reset(format);

    return true;
}

bool LogFile::openRead()
{
    foreach (QSharedPointer<FileFormat> format, m_formatMap) {
        close();

        m_file.reset( new QFile(m_filename) );

        if (!m_file->open(QIODevice::ReadOnly)) {
            qCritical() << "Error opening log file \"" << m_filename<< "\"!" << '\n';
            close();

            return false;
        }

        if (m_compressed) {
            m_compressor.reset( new QtIOCompressor(m_file.get()) );
            m_compressor->setStreamFormat(QtIOCompressor::GzipFormat);
            m_compressor->open(QIODevice::ReadOnly);

            m_io = m_compressor.get();
        } else {
            m_io = m_file.get();
        }

        QDataStream stream(m_io);

        if (format->readHeaderFromStream(stream)) {
            qInfo() << "Detected log file format version " << format->version() << "." << '\n';
            m_formatVersion = format->version();

            return true;
        }
    }

    qCritical()<< "Error log file corrupted or format is not supported!" << '\n';
    close();

    return false;
}

void LogFile::close()
{
    m_io = nullptr;
    m_file.reset();
    m_compressor.reset();
}

bool LogFile::readMessage(QByteArray& data, qint64& time, MessageType& type)
{
    if (m_io == nullptr || !m_io->isReadable()) {
        return false;
    }

    FileFormat* format = m_formatMap.value(m_formatVersion, nullptr).get();

    if (format == nullptr) {
        qCritical() << "Error log file format is not supported!" << '\n';

        return false;
    }

    QDataStream stream(m_io);
    stream.setVersion(QDataStream::Qt_4_6);

    if (stream.atEnd()) {
        return false;
    }

    return format->readMessageFromStream(stream, data, time, type);
}
