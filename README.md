# [LogAnalyserRoboFEI-SSL](https://gitlab.com/robofei/ssl/LogAnalyserRoboFei-SSL)

![](./example_recording.gif)

## Introduction

This software is based on the [Robocup Small Size League C++ Log Tools](https://github.com/RoboCup-SSL/ssl-logtools) and on the [RoboFEI Small Size League Main Software](https://gitlab.com/robofei/ssl/SSL-Strategy).

It provides a simple log player combined with a graphical client can that read vision and referee messages (from the network or directly from the log player). Furthermore it provides more options for loading and playing a match log file; for example it is possible to ignore the match states when the game is stopped or halted and consider the other states. The Kalman filter from our main software is also embedded with the application.

Other additional functionalities like download the Match League Log Files directly from the software and the use of predefined shortcuts provides an easy environment for the user watch Small Size League matches recorded or in real time.

At least, the `Enable Analysis` option activates a feature that can check when a shoot move or pass move occurs and evaluates this move in a 0 to 250 scale. At the end a file is generated with the results.

## Installation process

### GNU/Linux

Initially install the dependencies for build and run LogAnalyserRoboFEI-SSL.

```sh
sudo pacman -S base-devel zlib qt5-base eigen protobuf # Arch Linux and derivatives
sudo apt-get install build-essential git qt5-default protobuf-compiler zlib1g libeigen3-dev # Ubuntu and derivatives
```

Then clone and build LogAnalyserRoboFEI-SSL from source:
```sh
git clone https://gitlab.com/robofei/ssl/LogAnalyserRoboFei-SSL.git
cd LogAnalyserRoboFei-SSL
mkdir build && cd build
qmake .. CONFIG+=release
make -jN
```

On the last step replace `N` on the `-jN` option for the number of parallel jobs on the compilation of the program.
The executable will be generated on the build directory and can be executed with `./LogAnalyserRoboFEI-SSL`.

### Windows

Firstly, install [Visual Studio 2019](https://visualstudio.microsoft.com/downloads/) with the workload **Desktop development with C++**. Install [git](https://git-scm.com/downloads) as well.

Then, install [vcpkg](https://github.com/Microsoft/vcpkg), preferably on the location `C:\dev\vcpkg`. For that purpose, open the Windows PowerShell **as administrator** and do the follow commands:
```
cd \
mkdir dev
cd dev
git clone https://github.com/Microsoft/vcpkg.git
cd vcpkg
.\bootstrap-vcpkg.bat

```

Integrate vcpkg with Visual Studio:
```
.\vcpkg integrate install
```

Then, install the project dependencies:
```
.\vcpkg install protobuf:x64-windows protobuf[zlib]:x64-windows zlib:x64-windows
```

At least, install [Qt5 + QtCreator](https://www.qt.io/download-qt-installer?hsCtaTracking=99d9dd4f-5681-48d2-b096-470725510d34%7C074ddad0-fdef-4e53-8aa8-5e8a876d6ab4).

After that, we can proceed with the building process.

* Clone this Repository wherever you want with:
```
git clone https://gitlab.com/robofei/ssl/LogAnalyserRoboFei-SSL.git
```

* First, is necessary to generate the protobuf files. For that purpose, open a terminal, go to the directory `LogAnalyserRoboFei-SSL\include\protofiles` and run:
```
C:\dev\vcpkg\installed\x64-windows\tools\protobuf\protoc.exe --cpp-out=. *.proto
```

* Open the Project with QtCreator (open the .pro file);

* Select your preferred build kit (the default should work properly);

* Opt to do a build of type release;

* Build and run the program inside QtCreator.

Note for Windows users: the tool that allows the user to download the log files within the software does not work on Windows.
