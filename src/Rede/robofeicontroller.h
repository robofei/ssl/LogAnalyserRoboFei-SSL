/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "protofiles/messages_robocup_ssl_detection.pb.h"
#include "protofiles/messages_robocup_ssl_geometry.pb.h"
#include "protofiles/messages_robocup_ssl_refbox_log.pb.h"
#include "protofiles/messages_robocup_ssl_wrapper.pb.h"
#include "protofiles/referee.pb.h"
#include <QFile>
#include <QMutex>
#include <QString>
#include <QTextStream>
#include <QVector>
#include <sstream>
#include "Environment/environment.h"

#define QTD_MAX_PCT 200

#ifndef ROBOFEICONTROLLER_H
#define ROBOFEICONTROLLER_H

/**
 * @brief
 *
 */
class RoboFeiController
{
    private:
    //trocado de vector por qlist em 09/09/2017 - MAL
    QList <SSL_WrapperPacket> ssl_vision; /**< TODO: describe */
    QList <SSL_Referee> ssl_referee; /**< TODO: describe */
    //controle por mutex em 09/09/2017 - MAL
    QMutex mutex_vision; /**< TODO: describe */
    QMutex mutex_referee; /**< TODO: describe */
public:
    RoboFeiController();

  void addVision( const SSL_WrapperPacket& _s);

  void addReferee( const SSL_Referee& _s );

  SSL_WrapperPacket getLastVision();

  SSL_Referee getLastReferee();

  QString toStringReferee( const  SSL_Referee& _s);

  QString toStringWrapperPacket(const  SSL_WrapperPacket& _p );

  static QString getStage(const SSL_Referee & ssl_referee);

  static QString getCommand(const SSL_Referee & ssl_referee);
};

#endif // ROBOFEICONTROLLER_H
