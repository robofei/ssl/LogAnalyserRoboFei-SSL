/*
 * LogAnalyserRoboFEI-SSL
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef CONSTANTES_H
#define CONSTANTES_H
#include <QtCore>


#define TIME_LOOP_INTERFACE      20 /**< Período de atualização da interface [ms]. */
#define TIME_LOOP_KALMAN_FILTER   4 /**< Período de atualização do Filtro de Kalman [ms]. */
#define TIME_LOOP_ANALYSIS        3

#define XPLUS  +1
#define XMINUS -1

//****************************************** Parametros do Campo *****************

const int NUMBER_OF_CAMERAS = 4;   /**< Número de câmeras no campo. */
const int BALL_DIAMETER       = 43;  /**< Diâmetro da bola [mm]. */
const int ROBOT_DIAMETER       = 180; /**< Diâmetro de um robô [mm]. */
const int LINE_THICKNESS     = 0;   /**< Espessura das linhas do campo. Deve ser removido futuramente. */

const int iLarguraDivisaoCampo = 1500;
const int KALMAN_BUFFER_SIZE = 10;
const int OFFSET = 600;

const quint8 iAnaliseFramesSemAcharReceptor {10};

enum Team : quint8
{
    teamNone = 0,
    teamBlue = 1,
    teamYelllow = 2,
};

const quint8 PLAYERS_PER_SIDE {16};

const int FRAMES_LOST_TO_GET_OUT_OF_THE_FIELD = 30;
const float BALL_POSSESSION_MINIUM_DISTANCE = 250;
const float BALL_POSSESSION_MAXIMUM_DISTANCE = 500;
const float BALL_POSSESSION_DISTANCE = (BALL_POSSESSION_MINIUM_DISTANCE + BALL_POSSESSION_MAXIMUM_DISTANCE)/2.f;
const float ANGLE_FOR_ROBOT_IS_RECEIVING_THE_PASS = 20;
const float MINIMUM_SPEED_FOR_MOVEMENT_BALL = 1;

#endif // CONSTANTES_H
