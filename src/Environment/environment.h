﻿/*
 * LogAnalyserRoboFEI-SSL
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <QPair>
#include <QVector>
#include <QVector2D>


#include "protofiles/referee.pb.h"
#include "Constants/constants.h"
#include "Atributtes/attributes.h"
#include "Robot/robot.h"
#include "Common/common.h"

/**
 * @brief Estrutura que contém todos os componentes do ambiente de jogo, i.e., robôs, robôs adversários, bola, dimensões do campo, estado do jogo, quem está com a bola e outros.
 *
 */
class Environment
{
private:
    Ball ball; /**< Objeto da bola. */
    QVector2D vt2dFieldSize;/**< Tamanho absoluto do campo [mm]. */

    //
    //  *************************************    ---
    //  *                                   *     |
    //  *                                   *     |
    //  *                                   *   Height
    //  *                                   *     |
    //  *         |-- Goal Width -|         *     |
    //  *         +++++++++++++++++         *    ---
    // ___________+_______________+___________
    //  |----------- Width -----------------|


    int iHeightDefenseArea; /**< Profundidade da área de defesa [mm]. */
    int iWidthDefenseArea; /**< Largura da área de defesa [mm]. */
    int iGoalWidth; /**< Largura do gol [mm]. */
    int iGoalDepth; /**< Profundidade do gol [mm]. */
    int iRadiusCenter; /**< Raio do circulo do centro de campo [mm]. */

public:
    Robot rbtBlue[PLAYERS_PER_SIDE]; /**< Vetor de objetos dos robôs Azuis. */
    Robot rbtYellow[PLAYERS_PER_SIDE]; /**< Veotr de objetos dos robôs oponentes. */
    qint8 iIdBlueGoalKepper; /**< ID do goleiro do time Azul. */
    qint8 iIdYellowGoalKepper; /**< ID do goleiro do time Amarelo. */

    //-------------------  FILTRO  -----------------------
    QVector<QVector2D> vtBolaVisao; /**< Vetor de posições da bola na visão (Sem filtro) [mm]. */
    QVector<QVector2D> vtRobosAzuisVisao[PLAYERS_PER_SIDE]; /**< Vetor de posições dos robôs Azuis na visão (Sem filtro) [mm]. */
    QVector<QVector2D> vtRobosAmarelosVisao[PLAYERS_PER_SIDE]; /**< Vetor de posições dos robôs oponentes na visão (Sem filtro) [mm]. */

    QVector<QVector2D> vtBolaKALMAN; /**< Vetor de posicões filtradas da bola [mm]. */
    QVector<QVector2D> vtRobosAzuisKALMAN[PLAYERS_PER_SIDE]; /**< Vetor de posições filtradas dos robôs Azuis [mm]. */
    QVector<QVector2D> vtRobosAmarelosKALMAN[PLAYERS_PER_SIDE]; /**< Vetor de posições filtradas dos robôs oponentes [mm]. */

    QVector<QVector2D> vtPredicaoBola; /**< Vetor com os pontos de predição da posição da bola [mm]. */
    QVector<QVector2D> vtPredicaoAmarelo[PLAYERS_PER_SIDE]; /**< Vetor com os pontos de predição da posição dos robôs oponentes[mm]. */
    QVector<QVector2D> vtPredicaoAzul[PLAYERS_PER_SIDE]; /**< Vetor com os pontos de predição da posição dos robôs Azuis [mm]. */
    // ----------------------------------------------------------------------------------

    qint8 iBlueSide; /**< Lado de defesa, pode ser XNegativo (-1) ou XPositivo (+1). */

    qint8 iYellowSide;

    SSL_Referee_Command currentRefereeCommand; /**< Comando atual do referee */
    SSL_Referee_Stage currentRefereeStage; /**< Estágio atual de jogo. */

    QVector2D vt2dBallPlacementPosition {QVector2D(0, 0)};

    Team teamWithBall{teamNone};



public:
    /**
     * @brief Construtor da classe.
     *
     */
    Environment();

    /**
     * @brief Construtor por cópia da classe.
     *
     * @param _acAmbiente
     */
    Environment(const Environment &that);

    //AmbienteCampo &  operator= ( const AmbienteCampo &_acAmbiente);

    /**
     * @brief Destrutor da classe.
     *
     */
    ~Environment();

    /**
     * @brief Seta a posição da bola [mm].
     *
     * @param _vt2dPos - Nova posição.
     */
    void vSetBallPosition(QVector2D _vt2dPos);

    /**
     * @brief Seta a velocidade escalar da bola [m/s].
     *
     * @param dVelocidade - Nova velocidade.
     */
    void vSetBallVelocity(double dVelocidade);

    /**
     * @brief Seta a velocidade vetorial da bola [m/s].
     *
     * @param dVelocidade - Nova velocidade.
     */
    void vSetBallVelocity(QVector2D dVelocidade);

    /**
     * @brief Incrementa o tempo da bola na quantidade de tempo fornecida [s].
     *
     * @param tempo - Incremento de tempo.
     */
    void vIncreaseBallTime(double tempo);

    const Ball& getBall()const;

    Ball& getBall();

    /**
     * @brief Retorna a posição atual da bola [mm].
     *
     * @return QVector2D - Posição atual da bola.
     */
    QVector2D vt2dBallCurrentPosition() const;

    /**
     * @brief Retorna a posição anterior da bola [mm].
     *
     * @return QVector2D - Posição anterior da bola.
     */
    QVector2D vt2dBallPreviousPosition()const;

    /**
     * @brief Retorna a velocidade vetorial da bola [m/s].
     *
     * @return QVector2D - Velocidade vetorial da bola.
     */
    QVector2D vt2dBallVelocity()const;

    /**
     * @brief Retorna a velocidade escalar da bola [m/s].
     *
     * @return double - Velocidade escalar da bola.
     */
    double dBallVelocity()const;

    /**
     * @brief Retorna se a bola está sendo detectada na visão.
     *
     * @return bool - Bola detectada -> True, senão False.
     */
    bool bBallOnField()const;

    /**
     * @brief Reseta o contador de frames da bola.
     *
     */
    void vBallResetFrame();

    /**
     * @brief Seta o tamanho do campo [mm].
     *
     * @param _campo - Tamanho do campo.
     */
    void setFieldSize(QVector2D _campo);

    /**
     * @brief Seta a profundidade da área de defesa [mm].
     *
     * @param _iProfundidadeArea - Profundidade da área de defesa.
     */
    void vSetDefenseAreaHeight(int _iProfundidadeArea);

    /**
     * @brief Seta a largura da área de defesa [mm].
     *
     * @param _iLarguraArea - Largura da área de defesa.
     */
    void vSetDefenseAreaWidth(int _iLarguraArea);

    /**
     * @brief Seta a largura do gol [mm].
     *
     * @param _larguraGol - Largura do gol.
     */
    void vSetGoalWidth(int _larguraGol);

    /**
     * @brief Seta a profundidade do gol [mm].
     *
     * @param _iProfundidadeGol - Profundidade do gol.
     */
    void vSetGoalDepth(int _iProfundidadeGol);

    /**
     * @brief Seta o raio do circulo do centro do campo [mm]
     *
     * @param _raio - Raio do circulo do centro do campo.
     */
    void vSetCenterRadius(int _raio);

    /**
     * @brief Retorna o tamanho do campo [mm].
     *
     * @return QVector2D - Tamanho do campo.
     */
    QVector2D getFieldSize() const;

    /**
     * @brief Retorna o centro do gol Azul [mm].
     *
     * @return QVector2D - Centro do gol Azul.
     */
    QVector2D vt2dCenterBlueGoalPosition() const;

    /**
     * @brief Retorna o centro do gol oponente [mm].
     *
     * @return QVector2D - Centro do gol oponente.
     */
    QVector2D vt2dCenterYellowGoalPosition() const;

    /**
     * @brief Retorna a profundidade da área de defesa [mm].
     *
     * @return int - Profundidade da área de defesa.
     */
    int iGetDefenseAreaHeigth() const;

    /**
     * @brief Retorna a largura da área de defesa [mm].
     *
     * @return int - Largura da área de defesa.
     */
    int iGetDefenseAreaWidth() const;

    /**
     * @brief Retorna a largura do gol [mm].
     *
     * @return int - Largura do gol.
     */
    int iGetGoalWidth() const;

    /**
     * @brief Retorna a profundidade do gol [mm].
     *
     * @return int - Profundidade do gol.
     */
    int iGetGoalDepth() const;

    /**
     * @brief Retorna o raio do circulo do centro do campo [mm].
     *
     * @return int - Raio do circulo do centro do campo.
     */
    int iGetCenterRadius() const;

    /**
     * @brief Retorna a posição e tipo de todos os objetos de acordo com os parâmetros fornecidos.
     *
     * @param otObjeto - Tipo do objeto, pode ser, otAmarelo(0), otAzul(1) ou otBola(2).
     * @param _bEnviarGoleiro - Deve ser True para enviar o goleiro também.
     * @return QVector<QVector3D> - Posição X,Y dos objetos e tipo do objeto na coordenada Z.
     */
    QVector<QVector3D> vt3dGetAllPositions(TipoObjeto otObjeto, bool _bEnviarGoleiro = true) const;

    void vVisionUpdate(const Environment& ambienteVisao);
    /**
     * @brief Retorna a posição e tipo de todos os objetos de acordo com os parâmetros fornecidos.
     *
     * @param otObjeto - Tipo do objeto, pode ser, otAmarelo(0), otAzul(1) ou otBola(2).
     * @param _bEnviarGoleiro - Deve ser True para enviar o goleiro também.
     * @param _iIDIgnorado - Vetor de IDs a serem ignorados.
     * @return QVector<QVector3D> - Posição X,Y dos objetos e tipo do objeto na coordenada Z.
     */
    QVector<QVector3D> vt3dGetAllPositions(TipoObjeto otObjeto, bool _bEnviarGoleiro, QVector<int> _iIDIgnorado) const;

    /**
     * @brief Retorna a posição e tipo de todos os objetos de acordo com os parâmetros fornecidos.
     *
     * @param otObjeto  - Tipo do objeto, pode ser, otAmarelo(0), otAzul(1) ou otBola(2).
     * @param iRegiaoInicial - Região inicial em que os objetos devem estar contidos (linhas tracejadas no desenho do campo).
     * @param iRegiaoFinal - Região final em que os objetos devem estar contidos (linhas tracejadas no desenho do campo).
     * @param _iIDIgnorado - ID do robô a ser ignorado.
     * @return QVector<QVector3D> - Posição X,Y dos objetos e tipo do objeto na coordenada Z.
     */
    QVector<QVector3D> vt3dGetAllPositions(TipoObjeto otObjeto, int iRegiaoInicial, int iRegiaoFinal, int _iIDIgnorado = -1) const;

    /**
     * @brief Retorna true se o ponto fornecido estiver dentro da área de defesa.
     *
     * @param pos - Ponto.
     * @return bool - True se o ponto estiver dentro da área de defesa, senão, false.
     */
    bool bPointInsideDefenseArea(const QVector2D pos) const;

    void vUpdateBallBlacementPosition(const QVector2D posDesignada);

    void vUpdateSides(const quint8 idGoleiroAmarelo,
                      const quint8 idGoleiroAzul);

    void vUpdateTeamWithTheBall();

    bool bRobotReceivingThePass(const qint8 ID_Receptor,
                                const Team time)const;

    bool bGoalShootInProgress(const Team timeAtacante)const;

    qint8 iIdBallOwner(const Team time)const;




    quint16 iRobotScoreForReceiving(const Team timeAtacante,
                                    const quint8 id)const;

    quint16 iGoolShootScore(const Team timeAtacante)const;

    bool bBallIsMoving()const;

    bool bReceiverHasReceivedThePass(const Team timeAtacante,
                                     const quint8 id, const QVector2D vt2dBallPositionBeginOfShoot)const;

    bool bBallInsideTheDefenseArea(const Team timeAtacante)const;

    bool bBallDirectionHasChanged(const QVector2D posBolaInicioDaJogada, const float angle = 20)const;

    bool bGoolkepperHasDefended(const Team timeAtacante)const;

    bool bIsGool(const Team timeAtacante)const;

    bool bBallOutOfField()const;

    const Robot& rbt(const Team team, const qint8 id)const;
};

Q_DECLARE_METATYPE(Environment)

#endif // ENVIRONMENT_H
