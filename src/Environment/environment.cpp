﻿/*
 * LogAnalyserRoboFEI-SSL
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "environment.h"
#include <QDebug>
#include <QTimer>

Environment::Environment()
{
    iIdBlueGoalKepper = 0;
    iIdYellowGoalKepper = 0;

    for (int n=0; n< PLAYERS_PER_SIDE; n++)
    {
        rbtBlue[n] = Robot(n);
        rbtYellow[n] =Robot(n);
        vtRobosAzuisVisao[n].clear(); // pos dos robos antes do filtro
        vtRobosAmarelosVisao[n].clear();

        vtRobosAzuisKALMAN[n].clear();
        vtRobosAmarelosKALMAN[n].clear();

        vtPredicaoAmarelo[n].clear(); // para predicao dos robos
        vtPredicaoAzul[n].clear();

    }

    currentRefereeCommand = SSL_Referee_Command_HALT;
    currentRefereeStage = SSL_Referee_Stage_NORMAL_FIRST_HALF_PRE;
    vt2dFieldSize = QVector2D(0,0);
    iRadiusCenter = 0;
}



Environment::Environment(const Environment& that)
{
    this->iIdBlueGoalKepper = that.iIdBlueGoalKepper;
    this->iIdYellowGoalKepper = that.iIdYellowGoalKepper;

    Ball blBola_;
    Attributes atbRobo;
    QVector2D tamanhoCampo;

//    mtTeste.lock();

    this->ball = that.ball;

    //Atualiza informacoes dos robos e da bola
    for(int n=0; n < PLAYERS_PER_SIDE; ++n)
    {
        this->rbtBlue[n] = that.rbtBlue[n];
        this->rbtYellow[n] = that.rbtYellow[n];

        this->vtRobosAzuisVisao[n] = that.vtRobosAzuisVisao[n];
        this->vtRobosAmarelosVisao[n] = that.vtRobosAmarelosVisao[n];

        this->vtRobosAmarelosKALMAN[n] = that.vtRobosAmarelosKALMAN[n];
        this->vtRobosAzuisKALMAN[n] = that.vtRobosAzuisKALMAN[n];

        this->vtPredicaoAmarelo[n] = that.vtPredicaoAmarelo[n];

        this->vtPredicaoAzul[n] = that.vtPredicaoAzul[n];
        //Kalman
    }




    //Filtro de Kalman
    this->vtBolaKALMAN = (that.vtBolaKALMAN);
    this->vtPredicaoBola = (that.vtPredicaoBola);

    // teste
    this->iBlueSide = that.iBlueSide;
    this->iYellowSide = -that.iBlueSide;

    //Dimensoes campo
    tamanhoCampo = that.getFieldSize();
    this->setFieldSize(tamanhoCampo);
    this->iRadiusCenter = that.iRadiusCenter;
    this->iGoalWidth = that.iGoalWidth;
    this->iGoalDepth = that.iGoalDepth;
    this->iWidthDefenseArea = that.iWidthDefenseArea;
    this->iHeightDefenseArea = that.iHeightDefenseArea;

    //Dados do referee e teste atual
    this->currentRefereeCommand = that.currentRefereeCommand;
    this->currentRefereeStage = that.currentRefereeStage;
}


Environment::~Environment()
{

}

void Environment::vSetBallPosition(QVector2D _vt2dPos)
{
    ball.vt2dLastPosition = ball.vt2dCurrentPosition;
    ball.vt2dCurrentPosition = _vt2dPos;
}

void Environment::vSetBallVelocity(double dVelocidade)
{
    ball.dVelocity = dVelocidade;
}

void Environment::vSetBallVelocity(QVector2D dVelocidade)
{
    ball.vt2dVelocity = dVelocidade;
}

void Environment::vIncreaseBallTime(double tempo)
{
    ball.dBallTime += tempo;
    if(ball.dBallTime > 30)
        ball.dBallTime = 0;
}

const Ball& Environment::getBall() const
{
    return ball;
}

Ball& Environment::getBall()
{
    return ball;
}

QVector2D Environment::vt2dBallCurrentPosition() const
{
    return ball.vt2dCurrentPosition;
}

QVector2D Environment::vt2dBallPreviousPosition()const
{
    return ball.vt2dLastPosition;
}

QVector2D Environment::vt2dBallVelocity()const
{
   return ball.vt2dVelocity;
}

double Environment::dBallVelocity()const
{
   return ball.dVelocity;
}

bool Environment::bBallOnField()const
{
    return ball.bEmCampo;
}

void Environment::vBallResetFrame()
{
    for(unsigned int & i : ball.iCameraFrame)
    {
        i = 0;
    }
}


void Environment::setFieldSize(QVector2D _campo)
{
    vt2dFieldSize = _campo;
}



void Environment::vSetDefenseAreaHeight(int _iProfundidadeArea)
{
    iHeightDefenseArea = _iProfundidadeArea;
}



void Environment::vSetDefenseAreaWidth(int _iLarguraArea)
{
    iWidthDefenseArea = _iLarguraArea;
}



void Environment::vSetGoalWidth(int _larguraGol)
{
    iGoalWidth = _larguraGol;
}



void Environment::vSetGoalDepth(int _iProfundidadeGol)
{
    iGoalDepth = _iProfundidadeGol;
}



void Environment::vSetCenterRadius(int _raio)
{
    iRadiusCenter = _raio;
}

QVector2D Environment::getFieldSize() const
{
    return vt2dFieldSize;
}

QVector2D Environment::vt2dCenterBlueGoalPosition() const
{
    return QVector2D(iBlueSide*vt2dFieldSize.x()/2, 0);
}

QVector2D Environment::vt2dCenterYellowGoalPosition() const
{
    return QVector2D(iYellowSide*vt2dFieldSize.x()/2, 0);
}

int Environment::iGetDefenseAreaHeigth() const
{
    return iHeightDefenseArea;
}



int Environment::iGetDefenseAreaWidth() const
{
    return iWidthDefenseArea;
}



int Environment::iGetGoalWidth() const
{
    return iGoalWidth;
}



int Environment::iGetGoalDepth() const
{
    return iGoalDepth;
}



int Environment::iGetCenterRadius() const
{
    int tempCircRad = iRadiusCenter;
    return tempCircRad;
}


QVector<QVector3D> Environment::vt3dGetAllPositions(TipoObjeto otObjeto, bool _bEnviarGoleiro, QVector<int> _iIDIgnorado) const
{
    QVector<QVector3D> vt3dPosicaoObjetos;
    vt3dPosicaoObjetos.clear();

    if(otObjeto == otAzul)
    {
        for(const Robot& i : this->rbtBlue)
        {
            if(i.getAttributes().bOnField)
            {
                if(i.getAttributes().id == iIdBlueGoalKepper && _bEnviarGoleiro == true &&  _iIDIgnorado.contains(i.getAttributes().id) == false)
                    vt3dPosicaoObjetos.append(QVector3D(i.getAttributes().vt2dCurrentPosition, otAzul));
                else if(i.getAttributes().id != iIdBlueGoalKepper &&  _iIDIgnorado.contains(i.getAttributes().id) == false)
                    vt3dPosicaoObjetos.append(QVector3D(i.getAttributes().vt2dCurrentPosition, otAzul));
            }
        }
    }
    else if(otObjeto == otAmarelo)
    {
        for(auto i : this->rbtYellow)
        {
            if(i.getAttributes().bOnField)
            {
                if(i.getAttributes().id == iIdYellowGoalKepper && _bEnviarGoleiro == true)
                    vt3dPosicaoObjetos.append(QVector3D(i.getAttributes().vt2dCurrentPosition, otAmarelo));
                else if(i.getAttributes().id != iIdYellowGoalKepper && _iIDIgnorado.contains(i.getAttributes().id) == false)
                    vt3dPosicaoObjetos.append(QVector3D(i.getAttributes().vt2dCurrentPosition, otAmarelo));
            }
        }
    }
    else if(otObjeto == otBola)
    {
        if(this->ball.bEmCampo)
            vt3dPosicaoObjetos.append(QVector3D(this->ball.vt2dCurrentPosition, otBola));
    }

    return vt3dPosicaoObjetos;
}

QVector<QVector3D> Environment::vt3dGetAllPositions(TipoObjeto otObjeto, bool _bEnviarGoleiro) const
{
    QVector<QVector3D> vt3dPosicaoObjetos;
    vt3dPosicaoObjetos.clear();

    if(otObjeto == otAzul)
    {
        for(const Robot& i : this->rbtBlue)
        {
            if(i.getAttributes().bOnField)
            {
                if(i.getAttributes().id == iIdBlueGoalKepper && _bEnviarGoleiro == true)
                    vt3dPosicaoObjetos.append(QVector3D(i.getAttributes().vt2dCurrentPosition, otAzul));
                else if(i.getAttributes().id != iIdBlueGoalKepper )
                    vt3dPosicaoObjetos.append(QVector3D(i.getAttributes().vt2dCurrentPosition, otAzul));
            }
        }
    }
    else if(otObjeto == otAmarelo)
    {
        for(const Robot& i : this->rbtYellow)
        {
            if(i.getAttributes().bOnField)
            {
                if(i.getAttributes().id == iIdYellowGoalKepper && _bEnviarGoleiro == true)
                    vt3dPosicaoObjetos.append(QVector3D(i.getAttributes().vt2dCurrentPosition, otAmarelo));
                else if(i.getAttributes().id != iIdYellowGoalKepper)
                    vt3dPosicaoObjetos.append(QVector3D(i.getAttributes().vt2dCurrentPosition, otAmarelo));
            }
        }
    }
    else if(otObjeto == otBola)
    {
        if(this->ball.bEmCampo)
            vt3dPosicaoObjetos.append(QVector3D(this->ball.vt2dCurrentPosition, otBola));
    }

    return vt3dPosicaoObjetos;
}



void Environment::vVisionUpdate(const Environment& ambienteVisao)
{
    // Atualiza a bola
    this->ball = ambienteVisao.ball;

    this->setFieldSize(ambienteVisao.getFieldSize());
    this->vSetDefenseAreaHeight(ambienteVisao.iGetDefenseAreaHeigth());
    this->vSetDefenseAreaWidth(ambienteVisao.iGetDefenseAreaWidth());
    this->vSetGoalWidth(ambienteVisao.iGetGoalWidth());
    this->vSetGoalDepth(ambienteVisao.iGetGoalDepth());
    this->vSetCenterRadius(ambienteVisao.iGetCenterRadius());

    Attributes atbAux;

    for(int i=0; i < PLAYERS_PER_SIDE; ++i)
    {
        this->rbtBlue[i].vUpdateVisionAttributes(ambienteVisao.rbtBlue[i].getAttributes());
    }

    for(int i=0; i < PLAYERS_PER_SIDE; ++i)
    {
        this->rbtYellow[i].vUpdateVisionAttributes(ambienteVisao.rbtYellow[i].getAttributes());
    }

    this->vtBolaVisao.clear();
    this->vtBolaVisao.append(ambienteVisao.vtBolaVisao);
    this->vtBolaKALMAN.clear();
    this->vtBolaKALMAN.append(ambienteVisao.vtBolaKALMAN);
    this->vtPredicaoBola.clear();
    this->vtPredicaoBola.append(ambienteVisao.vtPredicaoBola);

    for (int i=0; i < PLAYERS_PER_SIDE; ++i)
    {
        this->vtRobosAzuisVisao[i].clear();
        this->vtRobosAzuisVisao[i].append(ambienteVisao.vtRobosAzuisVisao[i]);

        this->vtRobosAmarelosVisao[i].clear();
        this->vtRobosAmarelosVisao[i].append(ambienteVisao.vtRobosAmarelosVisao[i]);

        this->vtRobosAzuisKALMAN[i].clear();
        this->vtRobosAzuisVisao[i].append(ambienteVisao.vtRobosAzuisVisao[i]);

        this->vtRobosAmarelosKALMAN[i].clear();
        this->vtRobosAmarelosKALMAN[i].append(ambienteVisao.vtRobosAmarelosVisao[i]);

        this->vtPredicaoAzul[i].clear();
        this->vtPredicaoAzul[i].append(ambienteVisao.vtPredicaoAzul[i]);
        this->vtPredicaoAmarelo[i].clear();
        this->vtPredicaoAmarelo[i].append(ambienteVisao.vtPredicaoAmarelo[i]);
    }
}



void Environment::vUpdateBallBlacementPosition(const QVector2D posDesignada){
    this->vt2dBallPlacementPosition = posDesignada;
}


void Environment::vUpdateSides(const quint8 idGoleiroAmarelo,
                               const quint8 idGoleiroAzul){

    if(idGoleiroAzul >= PLAYERS_PER_SIDE||
       idGoleiroAmarelo>= PLAYERS_PER_SIDE)
        return;

    if(rbtBlue[idGoleiroAzul].vt2dGetCurrentPosition().x() < 0 &&
       rbtYellow[idGoleiroAmarelo].vt2dGetCurrentPosition().x() > 0 ){

        iBlueSide = XMINUS;
        iYellowSide = XPLUS;
    }
    else if(rbtBlue[idGoleiroAzul].vt2dGetCurrentPosition().x() > 0 &&
            rbtYellow[idGoleiroAmarelo].vt2dGetCurrentPosition().x() < 0 ){

        iBlueSide = XPLUS;
        iYellowSide = XMINUS;
    }

}


void Environment::vUpdateTeamWithTheBall(){

    teamWithBall = Common::timeTimeComABola(vt2dBallCurrentPosition(),
                                            vt3dGetAllPositions(otAmarelo, false),
                                            vt3dGetAllPositions(otAzul, false),
                                            BALL_POSSESSION_MAXIMUM_DISTANCE,
                                            BALL_POSSESSION_MINIUM_DISTANCE);
}


bool Environment::bRobotReceivingThePass(const qint8 ID_Receptor, const Team time)const {
    // função usada para verificar se o receptor -- ou algum outro robô -- está na trajetória da bola
    // Nota: se o robô estiver muito próximo da bola considera-se que este está recebendo o passe

    if (ID_Receptor == -1)
        return false;

    if( dBallVelocity() < 1)
        return false;

    if(time == teamBlue)
    {
        if(rbtBlue[ID_Receptor].vt2dGetCurrentPosition().distanceToPoint(vt2dBallCurrentPosition()) < 600)
            return true;

        return Common::bPointOnThePathOfTheBall(rbtBlue[ID_Receptor].vt2dGetCurrentPosition(),
                                                vt2dBallCurrentPosition(),
                                                vt2dBallVelocity(),
                                                ANGLE_FOR_ROBOT_IS_RECEIVING_THE_PASS);
    }

    if(time == teamYelllow)
    {
        if(rbtYellow[ID_Receptor].vt2dGetCurrentPosition().distanceToPoint(vt2dBallCurrentPosition()) < 600)
            return true;

        return Common::bPointOnThePathOfTheBall(rbtYellow[ID_Receptor].vt2dGetCurrentPosition(),
                                                vt2dBallCurrentPosition(),
                                                vt2dBallVelocity(),
                                                ANGLE_FOR_ROBOT_IS_RECEIVING_THE_PASS);
    }

    return false;

}

bool Environment::bGoalShootInProgress(const Team timeAtacante)const {

    if(timeAtacante == teamNone)
        return false;

    if(!bBallIsMoving())
        return false;


    QVector2D pontoCentroGolAdversario = vt2dCenterBlueGoalPosition();

    if(timeAtacante == teamBlue)
        pontoCentroGolAdversario = vt2dCenterYellowGoalPosition();

    for (quint8 i=0; i<3; i++)
    {
        QVector2D pontoCentroDaRegiao = QVector2D(pontoCentroGolAdversario.x(),
                                                  (i-1)*iGetGoalWidth()/2);

        if(Common::bPointOnThePathOfTheBall(pontoCentroDaRegiao,
                                                 vt2dBallCurrentPosition(),
                                                 vt2dBallVelocity()))
        {
            return true;
        }
    }

    return false;
}

qint8 Environment::iIdBallOwner(const Team time)const {

    if(time == teamNone)
        return -1;

    if(time == teamBlue){

        QVector2D posBola = vt2dBallCurrentPosition();
        QVector<QVector2D> vt_vt2dXIdYDistanciaBola{};

        for (qint8 id=0; id<PLAYERS_PER_SIDE; id++){
            if(rbtBlue[id].getAttributes().bOnField /*&&
               id != iIDGoleiroAzul*/)
                vt_vt2dXIdYDistanciaBola.append(
                            QVector2D(id,
                                      posBola.distanceToPoint(rbtBlue[id].vt2dGetCurrentPosition())
                                      )
                            );
        }

        vt_vt2dXIdYDistanciaBola = Common::vSortVectorByParameterY(
                    vt_vt2dXIdYDistanciaBola
                    );

        return static_cast<qint8>(vt_vt2dXIdYDistanciaBola.first().x());

    }

    if(time == teamYelllow){

        QVector2D posBola = vt2dBallCurrentPosition();
        QVector<QVector2D> vt_vt2dXIdYDistanciaBola{};

        for (qint8 id=0; id<PLAYERS_PER_SIDE; id++){
            if(rbtYellow[id].getAttributes().bOnField /*&&
               id != iIDGoleiroAmarelo*/)
                vt_vt2dXIdYDistanciaBola.append(
                            QVector2D(id,
                                      posBola.distanceToPoint(rbtYellow[id].vt2dGetCurrentPosition())
                                      )
                            );
        }

        vt_vt2dXIdYDistanciaBola = Common::vSortVectorByParameterY(
                    vt_vt2dXIdYDistanciaBola
                    );

        return static_cast<qint8>(vt_vt2dXIdYDistanciaBola.first().x());

    }

    return -1;
}


quint16 Environment::iRobotScoreForReceiving(const Team timeAtacante, const quint8 id)const
{
    if(!bBallIsMoving())
        return 0;

    if(timeAtacante == teamBlue){
        return Common::iPontuacaoReceptor(rbtBlue[id].vt2dGetCurrentPosition(),
                                                     vt2dBallCurrentPosition(),
                                                     vt2dBallVelocity());
    }

    if(timeAtacante == teamYelllow){
        return Common::iPontuacaoReceptor(rbtYellow[id].vt2dGetCurrentPosition(),
                                                     vt2dBallCurrentPosition(),
                                                     vt2dBallVelocity());
    }

    return 0;
}

quint16 Environment::iGoolShootScore(const Team timeAtacante)const {

    if(timeAtacante == teamYelllow){

        if(vt2dBallVelocity().x()*iYellowSide >= 0)
            return 0;

        float xFaltante = fabs(vt2dCenterBlueGoalPosition().x() - vt2dBallCurrentPosition().x());

        QVector2D vetorDeslocamentoBolaAteGolAdversario {
            (fabs(xFaltante/vt2dBallVelocity().normalized().x())) * vt2dBallVelocity().normalized()};

        QVector2D pontoProjecao { vt2dBallCurrentPosition() + vetorDeslocamentoBolaAteGolAdversario };

        if (pontoProjecao.y() > iGetGoalWidth()/2){
            return Common::iPontuacaoReceptor(
                        QVector2D(vt2dCenterBlueGoalPosition().x(), iGetGoalWidth()/2),
                        vt2dBallCurrentPosition(),
                        vt2dBallVelocity());
        }

        if(pontoProjecao.y() < -iGetGoalWidth()/2){
            return Common::iPontuacaoReceptor(
                        QVector2D(vt2dCenterBlueGoalPosition().x(), -iGetGoalWidth()/2),
                        vt2dBallCurrentPosition(),
                        vt2dBallVelocity());
        }

        return 900;
    }
    if(timeAtacante == teamBlue){

        if(vt2dBallVelocity().x()*iBlueSide>= 0)
            return 0;

        float xFaltante = fabs(vt2dCenterYellowGoalPosition().x() - vt2dBallCurrentPosition().x());

        QVector2D vetorDeslocamentoBolaAteGolAdversario {
            (fabs(xFaltante/vt2dBallVelocity().normalized().x())) * vt2dBallVelocity().normalized()};

        QVector2D pontoProjecao { vt2dBallCurrentPosition() + vetorDeslocamentoBolaAteGolAdversario };

        if (pontoProjecao.y() > iGetGoalWidth()/2){
            return Common::iPontuacaoReceptor(
                        QVector2D(vt2dCenterYellowGoalPosition().x(), iGetGoalWidth()/2),
                        vt2dBallCurrentPosition(),
                        vt2dBallVelocity());
        }

        if(pontoProjecao.y() < -iGetGoalWidth()/2){
            return Common::iPontuacaoReceptor(
                        QVector2D(vt2dCenterYellowGoalPosition().x(), -iGetGoalWidth()/2),
                        vt2dBallCurrentPosition(),
                        vt2dBallVelocity());
        }

        return 900;
    }

    return 0;

}

bool Environment::bBallIsMoving()const
{
    return dBallVelocity() > MINIMUM_SPEED_FOR_MOVEMENT_BALL;
}

bool Environment::bReceiverHasReceivedThePass(const Team timeAtacante,
                                              const quint8 id,
                                              const QVector2D vt2dBallPositionBeginOfShoot)const{

    if(timeAtacante == teamYelllow){

//        QVector2D pontoMira = rbtAmarelo[id].vt2dPontoMiraRobo();
        QVector2D posRobo = rbtYellow[id].vt2dGetCurrentPosition();

        return (vt2dBallCurrentPosition().distanceToPoint(posRobo) < 400 ) &&
                (bBallDirectionHasChanged(vt2dBallPositionBeginOfShoot) ||
                 !bBallIsMoving());
    }

    if(timeAtacante == teamBlue){

//        QVector2D pontoMira = rbtAzul[id].vt2dPontoMiraRobo();
        QVector2D posRobo = rbtBlue[id].vt2dGetCurrentPosition();

        return (vt2dBallCurrentPosition().distanceToPoint(posRobo) < 400 ) &&
                (bBallDirectionHasChanged(vt2dBallPositionBeginOfShoot) ||
                 !bBallIsMoving());
    }
    return false;
}

bool Environment::bBallInsideTheDefenseArea(Team timeAtacante)const{

    if(timeAtacante == teamYelllow){

        return Common::bPontoDentroArea(getFieldSize(),
                                        iGetDefenseAreaWidth(),
                                        iGetDefenseAreaHeigth(),
                                        iBlueSide,
                                        vt2dBallCurrentPosition());
    }

    if(timeAtacante == teamBlue){

        return Common::bPontoDentroArea(getFieldSize(),
                                        iGetDefenseAreaWidth(),
                                        iGetDefenseAreaHeigth(),
                                        iYellowSide,
                                        vt2dBallCurrentPosition());
    }

    return false;
}

bool Environment::bBallDirectionHasChanged(const QVector2D posBolaInicioDaJogada, const float angle)const
{
    return Common::dAngleBetweenTwoVectors(vt2dBallVelocity(), vt2dBallCurrentPosition() - posBolaInicioDaJogada) > angle;
}

bool Environment::bGoolkepperHasDefended(const Team timeAtacante)const
{
    if(timeAtacante == teamYelllow)
    {
        return (rbtBlue[iIdBlueGoalKepper].vt2dGetCurrentPosition().distanceToPoint(vt2dBallCurrentPosition()) < 400);
    }

    if(timeAtacante == teamBlue)
    {
        return (rbtYellow[iIdYellowGoalKepper].vt2dGetCurrentPosition().distanceToPoint(vt2dBallCurrentPosition()) < 400);
    }

    return false;
}

bool Environment::bIsGool(const Team timeAtacante)const
{
    qint8 iLadoCampoTimeDefensor = XMINUS;

    if(
       (timeAtacante == teamYelllow &&
        iYellowSide == XMINUS)
       ||
       (timeAtacante == teamBlue &&
        iBlueSide == XMINUS)
       )
    {
        iLadoCampoTimeDefensor = XPLUS;
    }

    if(vt2dBallCurrentPosition().y() <= iGoalWidth/2 &&
       vt2dBallCurrentPosition().y() >= -iGoalWidth/2){

        if(
           (iLadoCampoTimeDefensor == XMINUS &&
            vt2dBallCurrentPosition().x() <= -getFieldSize().x()/2)
           ||
           (iLadoCampoTimeDefensor == XPLUS &&
            vt2dBallCurrentPosition().x() >= getFieldSize().x()/2))

            return true;
    }

    return false;
}

bool Environment::bBallOutOfField() const
{
    if(vt2dBallCurrentPosition().x() < - getFieldSize().x()/2 ||
       vt2dBallCurrentPosition().x() >   getFieldSize().x()/2 ||
       vt2dBallCurrentPosition().y() < - getFieldSize().y()/2 ||
       vt2dBallCurrentPosition().y() >   getFieldSize().y()/2)
        return true;

    return false;
}

const Robot &Environment::rbt(const Team team, const qint8 id) const
{
    if(team == teamYelllow)
        return rbtYellow[id];
    if(team == teamBlue)
        return rbtBlue[id];
}


#include "environment.h"
