/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <utility>

#ifndef DRAWMAP_H
#define DRAWMAP_H

#include <QPainter>
#include <QPen>
#include <QImage>
#include <QVector2D>
#include <QPainterPath>
#include <qmath.h>

#include "Constants/constants.h"
#include "Environment/environment.h"

const double dLarguraLinhasBrancas = 2.5; /**< Largura das linhas brancas */


/**
 * @brief Classe responsável por desenhar qualquer coisa na imagem do campo.
 *
 */
class DrawMap
{
    int iLarguraGol;             /**< Largura do gol. */
    int iProfundidadeGol;        /**< Profundidade do gol. */
    int iLarguraDefesa;         /**< Largura da área de defesa. */
    int iProfundidadeDefesa;    /**< Profundidade da área de defesa. */
    bool bAntiAliasing;          /**< Indica se o Anti-Aliasing deve ser utilizado. */
    QVector2D vt2dTamanhoCampo;  /**< Tamanho do campo real */
    QSize sizeTamanhoCampo;      /**< Tamanho do campo real */
    QSize sizeTamanhoLabel;      /**< Tamanho do label da aba de jogo */
    float fEscalaX;              /**< Valor da escala no eixo X da imagem real para o tamanho do label da aba de jogo */
    float fEscalaY;              /**< Valor da escala no eixo Y da imagem real para o tamanho do label da aba de jogo */
    QPen *pen;                   /**< Caneta utilizada para fazer os desenhos. */
    QImage pngBola;              /**< Imagem da bola. */
    QImage pngPosicaoTimeout;    /**< Imagem da posição inicial do timeout. */
    QImage pngPadreSaboia;       /**< Imagem do Padre Sabóia. */
    QImage pngAmarelo;           /**< Imagem do oponente. */
    QVector<QImage> pngRobos;    /**< Imagens das capas dos robôs. */
    QVector<QString> pngAddress; /**< Endereços das imagens dos robôs. */

public:

    /**
     * @brief Inicializa o objeto que faz os desenhos do campo.
     * @param _vt2dTamanhoCampo - Tamanho do campo.
     * @param _sizeTamanhoLabel- Tamanho do label utilizado para mostrar a imagem.
     * @param sizeGol - Tamanho do gol.
     * @param sizeDefesa - Tamanho da área de defesa.
     */
    DrawMap(QVector2D _vt2dTamanhoCampo, QSize _sizeTamanhoLabel, QSize sizeGol, QSize sizeDefesa);

    /**
     * @brief Destrutor da classe.
     *
     */
    ~DrawMap();

    /**
     * @brief Desenha o campo, os robôs e a bola.
     *
     * @param acAmbiente - Ambiente do campo.
     * @param _bSaboia - Indica se deve desenhar o Sabóia.
     * @param _bDesenharPosicaoTimeout - Indica se deve desenhar a posição do timeout.
     * @param _vt2dPosicaoTimeout - Posição do timeout.
     * @param imImagem - Imagem de saída.
     * @param _bVerticalHorizontal = 0 - Indica se o desenho deve ser feito na vertical ou horizontal. *Remover*
     */
    void vDesenhaAmbiente(Environment *acAmbiente, QImage &imImagem, bool _bVerticalHorizontal = 0);

    /**
     * @brief Desenha os gols.
     *
     * @param pntPincel- Objeto do pincel inicializado.
     * @param _time - Cor do time Azul.
     * @param _iLadoCampoTimeAzul - Lado do campo do time Azul.
     */
    void vDesenhaGol(QPainter *pntPincel, int _iLadoCampoTimeAzul);

    /**
     * @brief Desenha as áreas de defesa.
     *
     * @param pntPincel  - Objeto do pincel inicializado.
     */
    void vDesenhaAreaDefesa(QPainter *pntPincel);

    /**
     * @brief Desenha as regiões do campo.
     *
     * @param pntPincel - Objeto do pincel inicializado.
     */
    void vDesenhaRegioesCampo(QPainter *pntPincel);

    /**
     * @brief Converte o ponto fornecido da escala do campo para a escala da imagem.
     *
     * @param _vt2dPonto - Ponto.
     */
    void vConvertePontoRealParaImagem(QVector2D &_vt2dPonto);

    /**
     * @brief Converte o vetor de pontos fornecidos da escala do campo para a escala da imagem.
     *
     * @param _caminho - Vetor de pontos.
     */
    void vConverteCaminhoRealParaImagem(QVector<QVector2D> &_caminho);

    /**
     * @brief Desenha os pontos fornecidos no campo.
     *
     * @param _pontos - Vetor de pontos.
     * @param outMap - Imagem de saída.
     * @param _cor - Cor dos pontos.
     * @param _dTamanhoPonto - Tamanho do pontos [pixels].
     * @param _bVerticalHorizontal = 0
     */
    void vDesenhaPontos(QVector<QVector2D> _pontos, QImage &outMap, const QColor &_cor, const double _dTamanhoPonto, bool _bVerticalHorizontal = 0);

    /**
     * @brief Desenha as linhas definidas pelos pontos fornecidos.
     *
     * @param _pontos - Vetor de pontos.
     * @param outMap - Imagem de saída.
     * @param _cor - Cor das linhas.
     * @param _bVerticalHorizontal = 0
     */
    void vDesenhaLinhas(QVector<QVector2D> _pontos, QImage &outMap, const QColor &_cor, bool _bVerticalHorizontal = 0);

    /**
     * @brief Desenha as linhas definidas pelos pontos fornecidos.
     *
     * @param _pontos - Vetor de pontos.
     * @param outMap - Imagem de saída.
     * @param _cor - Cor das linhas.
     * @param _iEspessuraLinha - Espessura das linhas.
     * @param _bVerticalHorizontal = 0
     */
    void vDesenhaLinhas(QVector<QVector2D> _pontos, QImage &outMap, const QColor &_cor, int _iEspessuraLinha, bool _bVerticalHorizontal = 0);

    /**
     * @brief Desenha o espaço livre da bola em direção ao gol.
     *
     * @param _vt2dMiraBola - Espaço livre do gol.
     * @param _vt2dPosicaoBola - Posição da bola.
     * @param outMap - Imagem de saída.
     * @param _cor - Cor da mira.
     */
    void vDesenhaMiraBola(QVector<QVector2D> _vt2dMiraBola, QVector2D _vt2dPosicaoBola, QImage &outMap, const QColor &_cor);

    /**
     * @brief Desenha um robô no campo.
     * @param pntPincel - Objeto do pincel inicializado.
     * @param atbRobo - Atributos do robô.
     * @param timeRobo - Time que o robô pertence. False = oponente, True = Azul.
     * @param opacidade - Opacidade em que o robô é desenhado.
     */
    void vDesenhaRobo(QPainter *pntPincel, Attributes atbRobo, bool timeRobo, float opacidade = 1.0);

    /**
     * @brief Desenha um robô no campo.
     * @param outmap - Imagem de saída.
     * @param atbRobo - Atributos do robô.
     * @param opacidade - Opacidade em que o robô é desenhado.
     */
    void vDesenhaRobo(QImage &outmap, Attributes atbRobo, float opacidade);




    void vDesenhaBallPlacement(QVector2D posBallPlacement,
                               QVector2D posBolaAtual,
                               QImage &outMap);

    void vDestacaPontoComUmXis(QVector2D posPonto,
                               const QString color,
                               const float distanciaDiagonal,
                               QImage &outMap);

    void vDestacaReceptor(QVector2D posReceptor,
                          QVector2D posBola,
                          QString cor,
                          QImage &outMap);

    void vDesenhaVetorDaBola(QVector2D posBola,
                             QVector2D vt2dVelocidadeBola,
                             QImage &outMap);



};

#endif // DRAWMAP_H
