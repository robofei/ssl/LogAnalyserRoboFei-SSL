﻿/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "drawmap.h"

DrawMap::DrawMap(QVector2D _vt2dTamanhoCampo, QSize _sizeTamanhoLabel, QSize sizeGol, QSize sizeDefesa)
{
    fEscalaX = (2*OFFSET + _vt2dTamanhoCampo.x())/_sizeTamanhoLabel.width();
    fEscalaY = (2*OFFSET +_vt2dTamanhoCampo.y())/_sizeTamanhoLabel.height();

    bAntiAliasing = false;

    iLarguraGol          = sizeGol.width();
    iProfundidadeGol     = sizeGol.height();
    iLarguraDefesa      = sizeDefesa.width();
    iProfundidadeDefesa = sizeDefesa.height();

    sizeTamanhoCampo = QSize(_vt2dTamanhoCampo.x(), _vt2dTamanhoCampo.y());
    vt2dTamanhoCampo = _vt2dTamanhoCampo;

    sizeTamanhoLabel  = _sizeTamanhoLabel;

    pen = new QPen(Qt::white, dLarguraLinhasBrancas, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);
}

DrawMap::~DrawMap()
{
    delete pen;
}

void DrawMap::vDesenhaAmbiente(Environment *acAmbiente, QImage &imImagem, bool _bVerticalHorizontal)
{
    QRect retanguloCampo(OFFSET/fEscalaX ,
                         (-1)*(OFFSET/fEscalaY ),
                         qRound((vt2dTamanhoCampo.x()) / fEscalaX ),
                         (-1)*qRound((vt2dTamanhoCampo.y()) / fEscalaY ));

    QPainter paint(&imImagem);

    imImagem.fill(QColor(0,128,0));
    if(bAntiAliasing == true)
        paint.setRenderHint(QPainter::Antialiasing);


    if(_bVerticalHorizontal == 0)
        paint.translate(0,((vt2dTamanhoCampo.y()+ OFFSET*2 - LINE_THICKNESS) / fEscalaY ));

    else
    {
        paint.rotate(270);
        paint.translate( -((vt2dTamanhoCampo.x()+ OFFSET*2 - LINE_THICKNESS) / fEscalaX ), ((vt2dTamanhoCampo.y()+ OFFSET*2 - LINE_THICKNESS) / fEscalaY ));//Desloca a origem do sistema de coordenadas do QT para baixo
    }

    paint.setPen(*pen);
    paint.drawRect(retanguloCampo);

    retanguloCampo = QRect(OFFSET/fEscalaX , (OFFSET/fEscalaY ), qRound(vt2dTamanhoCampo.x() / fEscalaX ), qRound(vt2dTamanhoCampo.y() / fEscalaY ));
    paint.drawLine((retanguloCampo.width())/2 + OFFSET/fEscalaX , (-1)*(OFFSET/fEscalaY ),
                   (retanguloCampo.width())/2 + OFFSET/fEscalaX , (-1)*(retanguloCampo.height()+OFFSET/fEscalaY ) );
    paint.drawLine((OFFSET + iProfundidadeDefesa)/fEscalaX                         , (-1)*((retanguloCampo.height())/2 + OFFSET/fEscalaY ),
                    retanguloCampo.width()+(OFFSET - iProfundidadeDefesa)/fEscalaX , (-1)*((retanguloCampo.height())/2 + OFFSET/fEscalaY ) );

    vDesenhaGol(&paint, acAmbiente->iBlueSide);
    vDesenhaAreaDefesa(&paint);
    vDesenhaRegioesCampo(&paint);

    paint.drawEllipse(QPoint( (retanguloCampo.width())/2 + OFFSET/fEscalaX, (-1)*((retanguloCampo.height())/2 + OFFSET/fEscalaY) ),
                      qRound(acAmbiente->iGetCenterRadius()/fEscalaX), qRound(acAmbiente->iGetCenterRadius()/fEscalaY) );

    Attributes atbAmarelo, atbAzul;
    QVector2D vt2dPontoRobo;

    for(auto &n : acAmbiente->rbtYellow)
    {
        atbAmarelo = n.getAttributes();

        if(atbAmarelo.bOnField == true){
            paint.setPen(QPen(Qt::yellow, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
            paint.setBrush(QBrush(QColor(255, 255, 80)));
            vDesenhaRobo(&paint, atbAmarelo, false);

        }

    }

    for(auto &n : acAmbiente->rbtBlue)
    {
        atbAzul = n.getAttributes();

        if(atbAzul.bOnField == true)
        {
            paint.setPen(QPen(Qt::cyan, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
            paint.setBrush(Qt::blue);

            vDesenhaRobo(&paint, atbAzul, true);

//            if(acAmbiente->bGolContra(atbAzul.id) == true)
//                clrCor = Qt::red;
//            else
//                clrCor = Qt::darkGray;

            //Direcao da mira do robo se ele esta proximo da bola
        }
    }

    if(acAmbiente->getBall().bEmCampo == true)
    {
        paint.setPen(QPen(QColor(255,180,0), 2.5, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));//Cor Laranja
        paint.setBrush(QColor(255,180,0));
    }
    else
    {
        paint.setPen(QPen(Qt::red, 2.5, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
        paint.setBrush(Qt::red);
    }

    vt2dPontoRobo = acAmbiente->getBall().vt2dCurrentPosition;

    vConvertePontoRealParaImagem(vt2dPontoRobo);

    vt2dPontoRobo.setY(vt2dPontoRobo.y()*-1);
    paint.drawEllipse(vt2dPontoRobo.toPoint(), qRound(BALL_DIAMETER/(fEscalaX)), qRound(BALL_DIAMETER/(fEscalaY)));

    paint.setBrush(Qt::transparent);
    paint.drawEllipse(vt2dPontoRobo.toPoint(), qRound(200/fEscalaX), qRound(200/fEscalaY));


    paint.setPen(QPen(Qt::white, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
    paint.end();

}

void DrawMap::vDesenhaGol(QPainter *pntPincel,int _iLadoCampoTimeAzul)
{
    QPen penXPositivo = pntPincel->pen();
    QPen penXNegativo = pntPincel->pen();

    if(_iLadoCampoTimeAzul == XMINUS)
    {
        penXPositivo = QPen(Qt::yellow, 4, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
        penXNegativo = QPen(Qt::blue, 4, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    }
    else
    {
        penXPositivo = QPen(Qt::blue, 4, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
        penXNegativo = QPen(Qt::yellow, 4, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
    }

    //Desenha o gol no lado direito, X Positivo
    pntPincel->setPen(penXPositivo);

    pntPincel->drawLine( (vt2dTamanhoCampo.x() + OFFSET) / fEscalaX                   , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraGol/2 + OFFSET) / fEscalaY,
                         (vt2dTamanhoCampo.x() + OFFSET + iProfundidadeGol) / fEscalaX, (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraGol/2 + OFFSET) / fEscalaY);

    pntPincel->drawLine( (vt2dTamanhoCampo.x() + OFFSET) / fEscalaX                   , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraGol/2 + OFFSET) / fEscalaY,
                         (vt2dTamanhoCampo.x() + OFFSET + iProfundidadeGol) / fEscalaX, (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraGol/2 + OFFSET) / fEscalaY);

    pntPincel->drawLine( (vt2dTamanhoCampo.x() + OFFSET + iProfundidadeGol) / fEscalaX, (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraGol/2 + OFFSET) / fEscalaY,
                         (vt2dTamanhoCampo.x() + OFFSET + iProfundidadeGol) / fEscalaX, (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraGol/2 + OFFSET) / fEscalaY);


    //Desenha o gol no lado esquerdo, X Negativo
    pntPincel->setPen(penXNegativo);

    pntPincel->drawLine( OFFSET/fEscalaX                        , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraGol/2 + OFFSET) / fEscalaY,
                         (OFFSET - iProfundidadeGol) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraGol/2 + OFFSET) / fEscalaY);

    pntPincel->drawLine( OFFSET/fEscalaX                        , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraGol/2 + OFFSET) / fEscalaY,
                         (OFFSET - iProfundidadeGol) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraGol/2 + OFFSET) / fEscalaY);

    pntPincel->drawLine( (OFFSET - iProfundidadeGol) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraGol/2 + OFFSET) / fEscalaY,
                         (OFFSET - iProfundidadeGol) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraGol/2 + OFFSET) / fEscalaY);

    pntPincel->setPen(QPen(Qt::white, dLarguraLinhasBrancas, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
}


void DrawMap::vDesenhaAreaDefesa(QPainter *pntPincel)
{
    //Desenha a area do penalty no lado direito
    pntPincel->drawLine( (vt2dTamanhoCampo.x() + OFFSET) / fEscalaX                        ,
                         (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraDefesa/2 + OFFSET) / fEscalaY,
                         (vt2dTamanhoCampo.x() + OFFSET - iProfundidadeDefesa) / fEscalaX ,
                         (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraDefesa/2 + OFFSET) / fEscalaY);

    pntPincel->drawLine( (vt2dTamanhoCampo.x() + OFFSET) / fEscalaX                        ,
                         (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraDefesa/2 + OFFSET) / fEscalaY,
                         (vt2dTamanhoCampo.x() + OFFSET - iProfundidadeDefesa) / fEscalaX ,
                         (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraDefesa/2 + OFFSET) / fEscalaY);

    pntPincel->drawLine( (vt2dTamanhoCampo.x() + OFFSET - iProfundidadeDefesa) / fEscalaX ,
                         (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraDefesa/2 + OFFSET) / fEscalaY,
                         (vt2dTamanhoCampo.x() + OFFSET - iProfundidadeDefesa) / fEscalaX ,
                         (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraDefesa/2 + OFFSET) / fEscalaY);

    //Desenha a area do penalty no lado esquerdo
    pntPincel->drawLine( OFFSET/fEscalaX                            ,
                         (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraDefesa/2 + OFFSET) / fEscalaY,
                         (OFFSET + iProfundidadeDefesa) / fEscalaX ,
                         (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraDefesa/2 + OFFSET) / fEscalaY);

    pntPincel->drawLine( OFFSET/fEscalaX                            ,
                         (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraDefesa/2 + OFFSET) / fEscalaY,
                         (OFFSET + iProfundidadeDefesa) / fEscalaX ,
                         (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraDefesa/2 + OFFSET) / fEscalaY);

    pntPincel->drawLine( (OFFSET + iProfundidadeDefesa) / fEscalaX ,
                         (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraDefesa/2 + OFFSET) / fEscalaY,
                         (OFFSET + iProfundidadeDefesa) / fEscalaX ,
                         (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraDefesa/2 + OFFSET) / fEscalaY);
}

void DrawMap::vDesenhaRegioesCampo(QPainter *pntPincel)
{
    int y1,y2, xRegiao, nRegioes;
    QPoint P1, P2;

    pntPincel->setPen(QPen(Qt::white, dLarguraLinhasBrancas, Qt::DashLine, Qt::FlatCap, Qt::RoundJoin));
    pntPincel->setOpacity(0.3);

    nRegioes = qCeil((vt2dTamanhoCampo.x())/iLarguraDivisaoCampo);

    y1 = (vt2dTamanhoCampo.y() + OFFSET)/fEscalaY;
    y2 = OFFSET/fEscalaY;

    P1.setY(-y1);
    P2.setY(-y2);

    for(int n=0; n < nRegioes; ++n)
    {
        xRegiao = (OFFSET + iLarguraDivisaoCampo*n)/fEscalaX;
        P1.setX(xRegiao);
        P2.setX(xRegiao);

        pntPincel->drawLine(P1,P2);
    }

    pntPincel->setPen(QPen(Qt::white, dLarguraLinhasBrancas, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    pntPincel->setOpacity(1);
}


void DrawMap::vConvertePontoRealParaImagem(QVector2D &_vt2dPonto)
{
    QVector2D vetorDeConversao(((vt2dTamanhoCampo.x() - LINE_THICKNESS) / 2) + OFFSET,
                               ((vt2dTamanhoCampo.y() - LINE_THICKNESS) / 2) + OFFSET);//torna as coordenadas dos robôs positivas

    _vt2dPonto.setX((_vt2dPonto.x() + vetorDeConversao.x() ) /fEscalaX);
    _vt2dPonto.setY((_vt2dPonto.y() + vetorDeConversao.y() ) /fEscalaY);

}

void DrawMap::vConverteCaminhoRealParaImagem(QVector<QVector2D> &_caminho)
{
    QVector2D vetorDeConversao(((vt2dTamanhoCampo.x() - LINE_THICKNESS) / 2) + OFFSET,
                               ((vt2dTamanhoCampo.y() - LINE_THICKNESS) / 2) + OFFSET);//torna as coordenadas dos robôs positivas

    for(auto & n : _caminho)
    {
        n.setX((n.x() + vetorDeConversao.x() ) /fEscalaX);
        n.setY((n.y() + vetorDeConversao.y() ) /fEscalaY);
    }
}

void DrawMap::vDesenhaPontos(QVector<QVector2D> _pontos, QImage &outMap, const QColor &_cor, const double _dTamanhoPonto, bool _bVerticalHorizontal)
{
    QVector<QVector2D> caminho;

    QImage *temp ;
    temp = &outMap;

    caminho = std::move(_pontos);

    vConverteCaminhoRealParaImagem(caminho);

    QPainter paint(temp);
    if(bAntiAliasing == true)
        paint.setRenderHint(QPainter::Antialiasing);


    if(_bVerticalHorizontal == 0)
        paint.translate(0,((vt2dTamanhoCampo.y()+ OFFSET*2 - LINE_THICKNESS) / fEscalaY ));

    else
    {
        paint.rotate(270);
        paint.translate( -((vt2dTamanhoCampo.x()+ OFFSET*2 - LINE_THICKNESS) / fEscalaX ),
                          ((vt2dTamanhoCampo.y()+ OFFSET*2 - LINE_THICKNESS) / fEscalaY ) );//Desloca a origem do sistema de coordenadas do QT para baixo
    }

    paint.setPen(QPen(_cor, _dTamanhoPonto, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    for(int n=0; n < (int)caminho.size(); ++n)
    {
        paint.drawPoint(caminho[n].x(), (-1)*caminho[n].y());
    }

    paint.end();

}

void DrawMap::vDesenhaLinhas(QVector<QVector2D> _pontos, QImage &outMap, const QColor &_cor, bool _bVerticalHorizontal)
{
    QVector<QVector2D> caminho;

    QImage *temp ;
    temp = &outMap;

    caminho = std::move(_pontos);

    vConverteCaminhoRealParaImagem(caminho);

    QPainter paint(temp);
    if(bAntiAliasing == true)
        paint.setRenderHint(QPainter::Antialiasing);

    if(_bVerticalHorizontal == 0)
        paint.translate(0,((vt2dTamanhoCampo.y()+ OFFSET*2 - LINE_THICKNESS) / fEscalaY ));

    else
    {
        paint.rotate(270);
        paint.translate( -((vt2dTamanhoCampo.x()+ OFFSET*2 - LINE_THICKNESS) / fEscalaX ), ((vt2dTamanhoCampo.y()+ OFFSET*2 - LINE_THICKNESS) / fEscalaY ));//Desloca a origem do sistema de coordenadas do QT para baixo
    }



    for(int n=0; n < (int)caminho.size()-1; ++n)
    {
        paint.setPen(QPen(_cor, 3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

        paint.drawLine(caminho[n].x(), (-1)*caminho[n].y() , caminho[n+1].x(), (-1)*caminho[n+1].y() );

        paint.setPen(QPen(_cor, 7, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        paint.drawPoint(caminho[n].x(), (-1)*caminho[n].y());

        paint.setPen(QPen(Qt::black, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    }

    paint.end();

}

void DrawMap::vDesenhaLinhas(QVector<QVector2D> _pontos, QImage &outMap, const QColor &_cor, int _iEspessuraLinha, bool _bVerticalHorizontal)
{
    QVector<QVector2D> caminho;

    QImage *temp ;
    temp = &outMap;

    caminho = std::move(_pontos);

    vConverteCaminhoRealParaImagem(caminho);

    QPainter paint(temp);
    if(bAntiAliasing == true)
        paint.setRenderHint(QPainter::Antialiasing);

    if(_bVerticalHorizontal == 0)
        paint.translate(0,((vt2dTamanhoCampo.y()+ OFFSET*2 - LINE_THICKNESS) / fEscalaY ));

    else
    {
        paint.rotate(270);
        paint.translate( -((vt2dTamanhoCampo.x()+ OFFSET*2 - LINE_THICKNESS) / fEscalaX ), ((vt2dTamanhoCampo.y()+ OFFSET*2 - LINE_THICKNESS) / fEscalaY ));//Desloca a origem do sistema de coordenadas do QT para baixo
    }



    for(int n=0; n < (int)caminho.size()-1; ++n)
    {
        paint.setPen(QPen(_cor, _iEspessuraLinha, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

        paint.drawLine(caminho[n].x(), (-1)*caminho[n].y() , caminho[n+1].x(), (-1)*caminho[n+1].y() );

    }

    paint.end();

}

void DrawMap::vDesenhaMiraBola(QVector<QVector2D> _vt2dMiraBola, QVector2D _vt2dPosicaoBola, QImage &outMap, const QColor &_cor)
{
    QImage *temp ;
    temp = &outMap;
    QPainter paint(temp);

    if(bAntiAliasing == true)
        paint.setRenderHint(QPainter::Antialiasing);

    paint.translate(0,((vt2dTamanhoCampo.y()+ OFFSET*2 - LINE_THICKNESS) / fEscalaY ));

    paint.setPen(QPen(_cor, 3, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    paint.setBrush(_cor);
    paint.setOpacity(0.25);

    QVector2D P1 = _vt2dMiraBola.first(),
              P2 = _vt2dMiraBola.last();

    vConvertePontoRealParaImagem(_vt2dPosicaoBola);
    vConvertePontoRealParaImagem(P1);
    vConvertePontoRealParaImagem(P2);

    _vt2dPosicaoBola.setY(_vt2dPosicaoBola.y()*-1);
    P1.setY(P1.y() *-1);
    P2.setY(P2.y() *-1);

    QPainterPath path;
    path.moveTo(P1.toPointF());
    path.lineTo(P2.toPointF());
    path.lineTo(_vt2dPosicaoBola.toPointF());
    path.lineTo(P1.toPointF());

    paint.drawPath(path);

    paint.setOpacity(1);
    paint.end();
}


void DrawMap::vDesenhaRobo(QPainter *pntPincel, Attributes atbRobo, bool timeRobo, float opacidade)
{
    QVector2D vt2dPontoRobo = atbRobo.vt2dCurrentPosition;
    vConvertePontoRealParaImagem(vt2dPontoRobo);

    vt2dPontoRobo.setY(vt2dPontoRobo.y()*-1);

    pntPincel->setOpacity(opacidade);
    //Desenha o circulo do robô
    QRectF rectRobo;
    rectRobo.setTopLeft(QPointF(vt2dPontoRobo.x()-ROBOT_DIAMETER*0.5/fEscalaX,
                                vt2dPontoRobo.y()-ROBOT_DIAMETER*0.5/fEscalaY) );
    rectRobo.setWidth(ROBOT_DIAMETER/fEscalaX);
    rectRobo.setHeight(ROBOT_DIAMETER/fEscalaY);

    float teta = qAcos(1.0-336.0/289.0); // Angulo da frente do robô
    float angRobo = atbRobo.rotation;
    Common::vConvertRadsToDegrees(angRobo);

    pntPincel->drawChord(rectRobo, ((angRobo+teta/2)*(180/M_PI))*16, (360-teta*180/M_PI)*16);

    pntPincel->setBrush(Qt::transparent);


    //Desenha o ID do robô
    vt2dPontoRobo = vt2dPontoRobo - QVector2D(ROBOT_DIAMETER/(2*fEscalaX), ROBOT_DIAMETER/(2*fEscalaY));
    pntPincel->setPen(QPen(Qt::black, 3.5, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
    pntPincel->drawText(vt2dPontoRobo.toPoint(), QString("  %1").arg(atbRobo.id));

}


void DrawMap::vDesenhaRobo(QImage &outmap, Attributes atbRobo, float opacidade)
{
    QPainter paint(&outmap);

    if(bAntiAliasing == true)
        paint.setRenderHint(QPainter::Antialiasing);

    paint.translate(0,((vt2dTamanhoCampo.y()+ OFFSET*2 - LINE_THICKNESS) / fEscalaY ));

    if(opacidade < 1.0)
    {
        paint.setPen(QPen(Qt::cyan, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
        paint.setBrush(Qt::blue);
    }
    else
    {
        paint.setPen(QPen(Qt::red, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
        paint.setBrush(Qt::darkRed);
    }
    paint.setOpacity(opacidade);

    QVector2D vt2dPontoRobo = atbRobo.vt2dCurrentPosition;
    vConvertePontoRealParaImagem(vt2dPontoRobo);

    vt2dPontoRobo.setY(vt2dPontoRobo.y()*-1);

    paint.setOpacity(opacidade);
    //Desenha o circulo do robô
    paint.drawEllipse(vt2dPontoRobo.toPointF(),
                      static_cast<int>(ROBOT_DIAMETER*0.5/fEscalaX),
                      static_cast<int>(ROBOT_DIAMETER*0.5/fEscalaY));

    //Desenha ângulo do robô
    QVector2D aux = atbRobo.vt2dCurrentPosition +
            QVector2D(150*qCos(atbRobo.rotation), 150*qSin(atbRobo.rotation));
    vConvertePontoRealParaImagem(aux);
    aux.setY(aux.y()*-1);
    paint.setOpacity(1.0);

    QVector<QVector2D> vt2dAnguloRobo;
    vt2dAnguloRobo.clear();
    vt2dAnguloRobo.append(vt2dPontoRobo);
    vt2dAnguloRobo.append(aux);
    paint.drawLine(vt2dAnguloRobo.constFirst().toPoint(),
                    vt2dAnguloRobo.constLast().toPoint());

    paint.end();
}



void DrawMap::vDesenhaBallPlacement(QVector2D posBallPlacement,
                                    QVector2D posBolaAtual,
                                    QImage &outMap){
    float fRaio = 150; /// 150 mm

    posBallPlacement.setY(-posBallPlacement.y());

    posBolaAtual.setY(-posBolaAtual.y());

    QPainter paint(&outMap);
    paint.setOpacity(.9);

    if(posBolaAtual.distanceToPoint(posBallPlacement) < 150)
        paint.setPen(QPen(QColor(Qt::green), 2, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    else
        paint.setPen(QPen(QColor(Qt::red), 2, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));


    vConvertePontoRealParaImagem(posBallPlacement);

    vConvertePontoRealParaImagem(posBolaAtual);

    paint.drawEllipse(posBallPlacement.toPointF(),
                      static_cast<int>(fRaio/fEscalaX),
                      static_cast<int>(fRaio/fEscalaY));

    paint.setPen(QPen(QColor(Qt::black), 2, Qt::DashLine, Qt::FlatCap, Qt::RoundJoin));

    paint.drawLine(posBallPlacement.toPointF(), posBolaAtual.toPointF());


}

void DrawMap::vDestacaPontoComUmXis(QVector2D posPonto,
                                     const QString color,
                                     const float distanciaDiagonal,
                                     QImage &outMap){

    QPainter paint(&outMap);

    posPonto.setY(-posPonto.y());

    vConvertePontoRealParaImagem(posPonto);

    paint.setPen(QPen(QColor(color), 2, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    paint.drawLine(QPointF(posPonto.x()-(distanciaDiagonal*sqrt(2)/2)/fEscalaX,
                           posPonto.y()-(distanciaDiagonal*sqrt(2)/2)/fEscalaY),
                   QPointF(posPonto.x()+(distanciaDiagonal*sqrt(2)/2)/fEscalaX,
                           posPonto.y()+(distanciaDiagonal*sqrt(2)/2)/fEscalaY));

    paint.drawLine(QPointF(posPonto.x()+(distanciaDiagonal*sqrt(2)/2)/fEscalaX,
                           posPonto.y()-(distanciaDiagonal*sqrt(2)/2)/fEscalaY),
                   QPointF(posPonto.x()-(distanciaDiagonal*sqrt(2)/2)/fEscalaX,
                           posPonto.y()+(distanciaDiagonal*sqrt(2)/2)/fEscalaY));
}

void DrawMap::vDestacaReceptor(QVector2D posReceptor,
                               QVector2D posBola,
                               QString cor,
                               QImage &outMap){

    vDestacaPontoComUmXis(posReceptor,
                           cor,
                           ROBOT_DIAMETER,
                           outMap);

    QPainter paint(&outMap);

    posBola.setY(-posBola.y());

    posReceptor.setY(-posReceptor.y());

    vConvertePontoRealParaImagem(posBola);

    vConvertePontoRealParaImagem(posReceptor);

    paint.setPen(QPen(QColor(Qt::red), 2, Qt::DashLine, Qt::FlatCap, Qt::RoundJoin));

    paint.drawLine(posBola.toPointF(), posReceptor.toPointF());

}

void DrawMap::vDesenhaVetorDaBola(QVector2D posBola,
                                  QVector2D vt2dVelocidadeBola,
                                  QImage &outMap){


    QColor corDaReta = QColor("#0000ff");
    if(vt2dVelocidadeBola.length() > 6){ // 6 é a velocidade máxima permitida
        corDaReta = QColor("#ff0000");
    }
    QVector2D posFinal = posBola + vt2dVelocidadeBola*8000/10;

    posBola.setY(-posBola.y());

    posFinal.setY(-posFinal.y());

    vConvertePontoRealParaImagem(posBola);

    vConvertePontoRealParaImagem(posFinal);

    QPainter paint(&outMap);

    paint.setPen(QPen(corDaReta, 2, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    paint.drawLine(posBola.toPointF(), posFinal.toPointF());

    paint.setPen(QPen(corDaReta, 5, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    paint.drawPoint(posFinal.toPointF());
}
