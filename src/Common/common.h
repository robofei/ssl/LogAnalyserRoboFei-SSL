/*
 * LogAnalyserRoboFEI-SSL
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef FUNCOESAUXILIARES_H
#define FUNCOESAUXILIARES_H

#include "Constants/constants.h"
#include "Robot/robot.h"

#include <QtCore>
#include <QVector>
#include <QVector2D>
#include <QVector4D>

namespace Common
{
double dAngleBetweenTwoVectors(QVector2D vetor1, QVector2D vetor2);

void vConvertRadsToDegrees(float &angulo);

QVector4D vt4dCalculaRetaMedia(QVector<QVector2D> _pontos);

QVector<QVector2D> vSortVectorByParameterY(QVector<QVector2D> vt_vt2d_X_ID__Y_param);

///
/// \brief bInterseccaoObjetosTriangulo_angulo
/// \param A
/// \param B
/// \param _bIgnorarBola
/// \param _vt3dObjetosEmCampo
/// \param graus_2_alfa
/// \return bool
///
bool bInterseccaoObjetosTrianguloAngulo(QVector2D A,
                                        QVector2D B,
                                        bool _bIgnorarBola,
                                        const QVector<QVector3D> &_vt3dObjetosEmCampo,
                                        float graus_2_alfa);

///
/// \brief bInterseccaoObjetosTriangulo_distancia
/// \param A
/// \param B
/// \param _bIgnorarBola
/// \param _vt3dObjetosEmCampo
/// \param distancia_CD
/// \param bLinhaY
/// \return bool
///
bool bInterseccaoObjetosTrianguloDistancia(QVector2D A /** ponta do triângulo */,
                                           QVector2D B,
                                           bool _bIgnorarBola,
                                           const QVector<QVector3D> &_vt3dObjetosEmCampo,
                                           float distancia_CD,
                                           bool bLinhaY = false);

///
/// \brief bRoboNaRetaDaBola
/// \param posRobo
/// \param vt4dRetaBola
/// \return bool
///
bool bPointOnThePathOfTheBall(const QVector2D posRobo,
                              const QVector2D posBola,
                              const QVector2D vt2dVelocidadeBola,
                              const float anguloAdmissivel = 20);

///
/// \brief vt2dProjecao_de_A_em_B
/// \param A
/// \param B
/// \return QVector2D
///
QVector2D vt2dProjectionOfVectorA_onVectorB(QVector2D A,
                                            QVector2D B);

QVector2D vt2dPerpendicularVector(QVector2D V1, QVector2D V2);


bool bIntersectionObjectsLine(const QVector2D _vt2dP1,
                              const QVector2D _vt2dP2,
                              const bool _bIgnorarBola,
                              const float _fDistanciaMinima,
                              const QVector<QVector3D>& _vt3dObjetosEmCampo);


double dAnguloEmGrausLivreParaChute(const QVector2D posBola,
                                    const int iLadoCampo,
                                    const int iLarguraGol,
                                    const QVector2D vt2dTamanhoCampo,
                                    const QVector<QVector3D>& _vt3dPosicoesOponentes);


double dAnguloEmGrausLivreParaPasse(const QVector2D posBola,
                                    const QVector2D posicaoReceptor,
                                    const QVector<QVector3D>& _vt3dPosicoesOponentes);


double dAnguloDeRotacao(const QVector2D posicaoDoRobo,
                        const QVector2D pontoMiraAtual,
                        const QVector2D pontoMiraDestino);


double dLiberdadeDeMarcacao(const QVector2D posicaoReceptor,
                            const QVector<QVector3D>& _vt3dPosicoesOponentes);


QVector<QVector2D> vt2dIntervaloLivreGol(QVector2D _vt2dPosicaoDeChute,
                                         int _iLadoCampoAliado,
                                         int _iLarguraGol,
                                         int _iProfundidadeGol,
                                         QVector2D _vt2dTamanhoCampo,
                                         const QVector<QVector3D>& _vt3dPosicoesOponentes,
                                         int larguraMinimaDaMira = ROBOT_DIAMETER/2);

bool bPontoContidoRetangulo(QVector2D ponto,
                            QVector2D R1,
                            QVector2D R2);

bool bPontoDentroArea(QVector2D tamanhoCampo,
                      float larguraArea,
                      float profundidadeArea,
                      int ladoCampo,
                      QVector2D ponto);

Team timeTimeComABola(const QVector2D posBola,
                      const QVector<QVector3D>& _vt3dPosicoesRobosAmarelos,
                      const QVector<QVector3D>& _vt3dPosicoesRobosAzuis,
                      const float distanciaMaxima = 300,
                      const float distanciaMinima = 200);

quint8 iEscalaVariavel(const double valor,
                       const double valorMinimo,
                       const double valorMaximo);

quint16 iPontuacaoReceptor(const QVector2D posRobo,
                           const QVector2D posBola,
                           const QVector2D vetorVelocidadeBola);

double dDeltaXis(const QVector2D posicaoReceptor,
                 const QVector2D posicaoPassador,
                 const qint8 iLadoCampoDefesa);

};

#endif // FUNCOESAUXILIARES_H
