/*
 * LogAnalyserRoboFEI-SSL
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "common.h"


double Common::dAngleBetweenTwoVectors(QVector2D vetor1, QVector2D vetor2)
{
    double ang = (180/M_PI) * qAcos(vetor1.dotProduct(vetor1, vetor2)/(vetor1.length()*vetor2.length()));
    if(qIsNaN(ang))
    {
        ang = 0;
    }
    return abs(ang);
}

void Common::vConvertRadsToDegrees(float &angulo)
{
    //Converte o angulo atual do robo para ser positivo e entre -180 e 180 graus
    if(qAbs(angulo) > 2*M_PI)
    {
        float fatorConversao = angulo/(2*M_PI) - trunc(angulo/(2*M_PI));
        angulo = fatorConversao * 2*M_PI;
    }
    if(angulo < 0)
        angulo += 2*M_PI;
    if(angulo > 2*M_PI)
        angulo = 2*M_PI - angulo;
    //
}


QVector4D Common::vt4dCalculaRetaMedia(QVector<QVector2D> _pontos)//**
{
    float fMediaX = 0, fMediaY = 0, /*fVarianciaX = 0, fVarianciaY = 0,*/ numb = 0 , denb = 0;
    float a,b;
    QVector2D Pi, Pf;

    for(int i=0; i<_pontos.size() ; i++){

        fMediaX += _pontos.at(i).x();
        fMediaY += _pontos.at(i).y();

    }

    fMediaX   = fMediaX/(_pontos.size());
    fMediaY   = fMediaY/(_pontos.size());


    for(int j=0; j<_pontos.size(); j++)
    {
        numb += _pontos.at(j).x() * (_pontos.at(j).y() - fMediaY);
        denb += _pontos.at(j).x() * (_pontos.at(j).x() - fMediaX);

    }

    b = numb/denb;

    a = fMediaY - b*fMediaX;


    if(qIsFinite(a) && qIsFinite(b))
    {
        Pi = QVector2D(_pontos.at(0).x(), b*_pontos.at(0).x() + a);
        Pf = QVector2D(_pontos.last().x(), b*_pontos.last().x() + a);
    }
    else
    {
        Pi = Pf = _pontos.last();
    }

    return {Pi.x(), Pi.y(), Pf.x(), Pf.y()};
}


QVector<QVector2D> Common::vSortVectorByParameterY(QVector<QVector2D> vt_vt2dXIdYParametro){
    /// X = ID do robô
    /// Y = parâmetro
    ///
    /// a função retorna o vetor de entrada colocando os parâmetros em ordem crescente

    if(vt_vt2dXIdYParametro.size()<=1)
        return vt_vt2dXIdYParametro;

    QVector2D auxiliar_0;
    QVector2D auxiliar_1;
    int n = vt_vt2dXIdYParametro.size();

    for (int i=0; i<n; i++){
        for (int j=i+1; j<n; j++){
            if(vt_vt2dXIdYParametro.at(j).y() < vt_vt2dXIdYParametro.at(i).y()){
                auxiliar_0 = vt_vt2dXIdYParametro.at(i);
                auxiliar_1 = vt_vt2dXIdYParametro.at(j);

                vt_vt2dXIdYParametro.replace(i,auxiliar_1);
                vt_vt2dXIdYParametro.replace(j,auxiliar_0);
            }
        }
    }

    return vt_vt2dXIdYParametro;

}

bool Common::bInterseccaoObjetosTrianguloAngulo(QVector2D A /** ponta do triângulo */, QVector2D B, bool _bIgnorarBola, const QVector<QVector3D> &_vt3dObjetosEmCampo, float graus_2_alfa){
    //
    //             * C
    //          *--*
    //       *-----*
    //  A *--------* B
    //       *-----*
    //          *--*
    //             * D
    //
    //
    // a função verifica a intersecção de objetos dentro do triângulo ACBD
    // graus_2_alfa é o ângulo CÂD

    const QVector2D unitario_l = QVector2D( - (B - A).y(), (B - A).x()).normalized(); /// versor perpendicular
    const float d = B.distanceToPoint(A) * tan((.5) * (M_PI/180) * graus_2_alfa);
    const QVector2D C = B - unitario_l*d;
    const QVector2D D = B + unitario_l*d;

    const float
            Ax = A.x(),
            Ay = A.y(),
            Cx = C.x(),
            Cy = C.y(),
            Dx = D.x(),
            Dy = D.y();

    for(const QVector3D n : _vt3dObjetosEmCampo)
    {
        if(static_cast<TipoObjeto>(n.z()) == otBola && _bIgnorarBola)
            continue;

        for (qint8 i=0; i<=8; i++){
            QVector2D P = n.toVector2D();

            switch (i){ // considera 9 pontos do robô
                case 0:
                    break;
                case 1:
                    P.setX(P.x() - ROBOT_DIAMETER/2);
                    break;
                case 2:
                    P.setX(P.x() + ROBOT_DIAMETER/2);
                    break;
                case 3:
                    P.setY(P.y() - ROBOT_DIAMETER/2);
                    break;
                case 4:
                    P.setY(P.y() + ROBOT_DIAMETER/2);
                    break;
                case 5:
                    P.setY(P.y() + ROBOT_DIAMETER/sqrtf(8));
                    P.setX(P.x() + ROBOT_DIAMETER/sqrtf(8));
                    break;
                case 6:
                    P.setY(P.y() + ROBOT_DIAMETER/sqrtf(8));
                    P.setX(P.x() - ROBOT_DIAMETER/sqrtf(8));
                    break;
                case 7:
                    P.setY(P.y() - ROBOT_DIAMETER/sqrtf(8));
                    P.setX(P.x() - ROBOT_DIAMETER/sqrtf(8));
                    break;
                case 8:
                    P.setY(P.y() - ROBOT_DIAMETER/sqrtf(8));
                    P.setX(P.x() + ROBOT_DIAMETER/sqrtf(8));
                    break;
            }
            const float Px = P.x();
            const float Py = P.y();

            const float w_2 = ((Cx - Ax)*(Py - Ay) - (Cy - Ay)*(Px - Ax))
                              /
                              ((Dy - Ay)*(Cx - Ax) - (Dx - Ax)*(Cy - Ay));

            const float w_1 = ((Px - Ax) - w_2*(Dx - Ax))
                              /
                              ((Cx - Ax));

            // verifica se o ponto P está dentro do triângulo ACD
            if( w_1 >= 0    &&
                w_2 >= 0    &&
                (w_1+w_2) <= 1)
            {
                return true;
            }
        }
    }
    return false;
}

bool Common::bInterseccaoObjetosTrianguloDistancia(QVector2D A /** ponta do triângulo */, QVector2D B, bool _bIgnorarBola, const QVector<QVector3D> &_vt3dObjetosEmCampo, float distancia_CD, bool bLinhaY){

    ///
    ///             * C
    ///          *--*
    ///       *-----*
    ///  A *--------* B
    ///       *-----*
    ///          *--*
    ///             * D
    ///
    ///
    /// a função verifica a intersecção de objetos dentro do triângulo ACBD
    ///
    /// se bLinhaY for declarada true a reta CBD será paralela ao eixo Y
    /// caso contrário a reta será perpendicular à reta AB

    QVector2D C, D, P, aux = B - A,
            unitario_l = QVector2D(0,1);


    if (bLinhaY ==false)
        unitario_l = QVector2D( - aux.y(), aux.x()).normalized(); /// versor perpendicular

    C = B - unitario_l*distancia_CD/2;
    D = B + unitario_l*distancia_CD/2;

    float Ax = A.x(),
            Ay = A.y(),
            Cx = C.x(),
            Cy = C.y(),
            Dx = D.x(),
            Dy = D.y(),
            Px        ,
            Py;

    float w_1, w_2;


    for(auto & n : _vt3dObjetosEmCampo)
    {
        if(static_cast<TipoObjeto>(n.z()) == otBola && _bIgnorarBola)
            continue;
        else{
            for (int i=0; i<=4; i++){
                P = n.toVector2D();

                switch (i){ /// considera 5 pontos do robô
                    case 0:
                        break;
                    case 1:
                        P.setX(P.x() - ROBOT_DIAMETER/2);
                        break;
                    case 2:
                        P.setX(P.x() + ROBOT_DIAMETER/2);
                        break;
                    case 3:
                        P.setY(P.y() - ROBOT_DIAMETER/2);
                        break;
                    case 4:
                        P.setY(P.y() + ROBOT_DIAMETER/2);
                        break;
                }
                Px = P.x();
                Py = P.y();

                w_2 = ((Cx - Ax)*(Py - Ay) - (Cy - Ay)*(Px - Ax))
                      /
                      ((Dy - Ay)*(Cx - Ax) - (Dx - Ax)*(Cy - Ay));

                w_1 = ((Px - Ax) - w_2*(Dx - Ax))
                      /
                      ((Cx - Ax));

                /// verifica se o ponto P está dentro do triângulo ACD
                if( w_1 >= 0    &&
                    w_2 >= 0    &&
                    (w_1+w_2) <= 1)
                    return true;
            }
        }
    }
    return false;
}


bool Common::bPointOnThePathOfTheBall(const QVector2D posRobo,
                                      const QVector2D posBola,
                                      const QVector2D vt2dVelocidadeBola,
                                      const float anguloAdmissivel){
    /// verifica se o robô está na reta da bola com erro de 10°

    if (abs(Common::dAngleBetweenTwoVectors((posRobo - posBola), vt2dVelocidadeBola)) < anguloAdmissivel)
        return true;

    else
        return false;
}

QVector2D Common::vt2dProjectionOfVectorA_onVectorB(QVector2D A, QVector2D B){
    return (QVector2D::dotProduct(A,B)/(B.length()))*(B.normalized());
}

QVector2D Common::vt2dPerpendicularVector(QVector2D V1, QVector2D V2)
{
    return (V1 - ((V1.dotProduct(V1, V2)/V2.lengthSquared()) * V2));// W→ = V1→ - ( ( (V1→ . V2→)/ |V2→|²) * V2→)
}



bool Common::bIntersectionObjectsLine(const QVector2D _vt2dP1,
                                      const QVector2D _vt2dP2,
                                      const bool _bIgnorarBola,
                                      const float _fDistanciaMinima,
                                      const QVector<QVector3D> &_vt3dObjetosEmCampo)
{
    //Checa se o ponto P3 intercepta o segmento de reta formado por P1 e P2
    QVector2D vt2dV, vt2dP3, vt2dU, vt2dW, vt2dA;
    float fDistanciaPontoLinha = 0;
    QVector2D vt2dP, Pa, Pb, Pc;


    vt2dV = _vt2dP2 - _vt2dP1;// V→ = P2 - P1

    for(auto & n : _vt3dObjetosEmCampo)
    {
        if(static_cast<TipoObjeto>(n.z()) == otBola && _bIgnorarBola)continue;

        else
        {
            vt2dP3 = n.toVector2D();

            vt2dU = vt2dP3 - _vt2dP1;// U→ = P3 - P1
            vt2dW = Common::vt2dPerpendicularVector(vt2dU, vt2dV);//vt2dU - ((vt2dU.dotProduct(vt2dU, vt2dV)/vt2dV.lengthSquared()) * vt2dV);// W→ = U→ - ( ( (U→ . V→)/ |V→|²) * V→)

            fDistanciaPontoLinha = qFloor(vt2dW.length());// d = |W→|

            if(fDistanciaPontoLinha < _fDistanciaMinima)
            {
                float fP_PaPb = 0, fPaPb_PaPb = 0, fP_PcPb = 0, fPcPb_PcPb =0;

                vt2dA = vt2dW;
                vt2dA.normalize(); // A^ = W→/|W→|
                vt2dA *= _fDistanciaMinima;

                Pa = vt2dA + _vt2dP1; Pb = _vt2dP2 + vt2dA; Pc = _vt2dP2 - vt2dA;
                vt2dP = vt2dP3 - Pb;

                fP_PaPb = vt2dP.dotProduct(vt2dP, (Pa-Pb));
                fPaPb_PaPb = vt2dP.dotProduct((Pa-Pb), (Pa-Pb));
                fP_PcPb = vt2dP.dotProduct(vt2dP, (Pc-Pb));
                fPcPb_PcPb = vt2dP.dotProduct((Pc-Pb), (Pc-Pb));

                if(fP_PaPb >= 0 && fP_PaPb <= fPaPb_PaPb && // Se 0 < P→ . (Pa-Pb)→ < |Pa-Pb|² E 0 < P→ . (Pc-Pb)→ < |Pc-Pb|² o ponto P3 está
                   fP_PcPb >= 0 && fP_PcPb <= fPcPb_PcPb  ) // dentro do retângulo
                    return true;


            }
            else if(fDistanciaPontoLinha <= 0)
            {
                if(vt2dP3.distanceToPoint(_vt2dP1) < _vt2dP1.distanceToPoint(_vt2dP2) &&
                   vt2dP3.distanceToPoint(_vt2dP2) < _vt2dP1.distanceToPoint(_vt2dP2))
                    return true;
            }
        }
    }
    return false;
}


double Common::dAnguloEmGrausLivreParaChute(const QVector2D posBola,
                                            const int iLadoCampo,
                                            const int iLarguraGol,
                                            const QVector2D vt2dTamanhoCampo,
                                            const QVector<QVector3D>& _vt3dPosicoesOponentes){

    double anguloLivre = 0;
    double anguloLivreMaximo = 0;





    QVector2D vt2dPontoGol_1 = QVector2D( vt2dTamanhoCampo.x()/2 * iLadoCampo * -1 ,
                                          -iLarguraGol/2);

    for(int i=0; i < iLarguraGol/(BALL_DIAMETER/2); ++i)
    {
        //Se o ponto está livre, adiciona-o na lista
        if( ! Common::bIntersectionObjectsLine( posBola,
                                                vt2dPontoGol_1,
                                                true,
                                                ROBOT_DIAMETER/2,
                                                _vt3dPosicoesOponentes)){
            anguloLivre += Common::dAngleBetweenTwoVectors(
                               QVector2D(vt2dPontoGol_1.x(), vt2dPontoGol_1.y() + BALL_DIAMETER/2) - posBola,
                               QVector2D(vt2dPontoGol_1.x(), vt2dPontoGol_1.y() - BALL_DIAMETER/2) - posBola
                               );
        }
        else {
            if(anguloLivreMaximo < anguloLivre)
                anguloLivreMaximo = anguloLivre;

            anguloLivre = 0;
        }
        vt2dPontoGol_1.setY( vt2dPontoGol_1.y() + BALL_DIAMETER/2 );

    }

    vt2dPontoGol_1.setY( vt2dPontoGol_1.y() + BALL_DIAMETER/2 );

    if(anguloLivreMaximo < anguloLivre)
        anguloLivreMaximo = anguloLivre;

    return anguloLivreMaximo;
}

double Common::dAnguloEmGrausLivreParaPasse(const QVector2D posBola,
                                            const QVector2D posicaoReceptor,
                                            const QVector<QVector3D> &_vt3dPosicoesOponentes){


    for(quint8 i=1; i<=45; i+=1)
    {
        if(bInterseccaoObjetosTrianguloAngulo(posBola,
                                              posicaoReceptor,
                                              true,
                                              _vt3dPosicoesOponentes,
                                              i))
        {
            return (i-1);
        }

    }
    return 45;
}

double Common::dAnguloDeRotacao(const QVector2D posicaoDoRobo,
                                const QVector2D pontoMiraAtual,
                                const QVector2D pontoMiraDestino)
{

    return Common::dAngleBetweenTwoVectors(pontoMiraAtual - posicaoDoRobo, pontoMiraDestino - posicaoDoRobo);
}

double Common::dLiberdadeDeMarcacao(const QVector2D posicaoReceptor,
                                    const QVector<QVector3D> &_vt3dPosicoesOponentes){

    float menorDistancia = 1e10;

    for (const QVector3D &n : _vt3dPosicoesOponentes){
        const float& distancia = (n.toVector2D()).distanceToPoint(posicaoReceptor);

        if(distancia < menorDistancia)
            menorDistancia = distancia;
    }

    return -menorDistancia;
}


QVector<QVector2D> Common::vt2dIntervaloLivreGol(QVector2D _vt2dPosicaoDeChute,
                                                 int _iLadoCampoAliado,
                                                 int _iLarguraGol,
                                                 int _iProfundidadeGol,
                                                 QVector2D _vt2dTamanhoCampo,
                                                 const QVector<QVector3D> &_vt3dPosicoesOponentes,
                                                 int larguraMinimaDaMira)
{
    QVector2D vt2dPontoGol_1;
    QVector<QVector2D> vt2dIntervaloChute;
    QVector<QVector2D> vt2dIntervaloChuteFinal;
    vt2dIntervaloChuteFinal.clear();
    vt2dIntervaloChute.clear();
    _iProfundidadeGol = 0; // Retirado do calculo para que o robô mire na linha do gol e não atrás do gol

    //Devemos inverter o sinal do iLadoCampo pois ele sinaliza o lado que nós defendemos, portanto, o lado que atacamos é o inverso dele
    vt2dPontoGol_1.setX( (_vt2dTamanhoCampo.x()/2 + _iProfundidadeGol) * _iLadoCampoAliado * -1);
    vt2dPontoGol_1.setY(-_iLarguraGol/2);

    for(int i=0; i < _iLarguraGol/(BALL_DIAMETER/2); ++i)
    {
        //Se o ponto está livre, adiciona-o na lista
        if(Common::bIntersectionObjectsLine( _vt2dPosicaoDeChute,
                                             vt2dPontoGol_1,
                                             true,
                                             larguraMinimaDaMira,
                                             _vt3dPosicoesOponentes) == false)
        {
            vt2dIntervaloChute.append(vt2dPontoGol_1);
        }
        else if(vt2dIntervaloChuteFinal.isEmpty()){
            vt2dIntervaloChuteFinal = vt2dIntervaloChute;
            vt2dIntervaloChute.clear();
        }
        else if(!vt2dIntervaloChute.isEmpty()){
            double anguloAtual = Common::dAngleBetweenTwoVectors(
                                     QVector2D(vt2dIntervaloChute.first().x(), vt2dIntervaloChute.first().y() - BALL_DIAMETER/2) - _vt2dPosicaoDeChute,
                                     QVector2D(vt2dIntervaloChute.last().x(), vt2dIntervaloChute.last().y() + BALL_DIAMETER/2) - _vt2dPosicaoDeChute
                                     );
            double anguloAtualMaximo = Common::dAngleBetweenTwoVectors(
                                           QVector2D(vt2dIntervaloChuteFinal.first().x(), vt2dIntervaloChuteFinal.first().y() - BALL_DIAMETER/2) - _vt2dPosicaoDeChute,
                                           QVector2D(vt2dIntervaloChuteFinal.last().x(), vt2dIntervaloChuteFinal.last().y() + BALL_DIAMETER/2) - _vt2dPosicaoDeChute
                                           );
            if(anguloAtual > anguloAtualMaximo)
                vt2dIntervaloChuteFinal = vt2dIntervaloChute;

            vt2dIntervaloChute.clear();

        }

        /*
        else//Fim de um intervalo livre
        {
            //Salva o maior intervalo de pontos
            if(vt2dIntervaloChute.size() > vt2dIntervaloChuteFinal.size())
            {
                vt2dIntervaloChuteFinal.clear();
                vt2dIntervaloChuteFinal = vt2dIntervaloChute;
                vt2dIntervaloChute.clear();
            }
        }*/

        vt2dPontoGol_1.setY( vt2dPontoGol_1.y() + BALL_DIAMETER/2 );
    }

    if(vt2dIntervaloChute.size() > vt2dIntervaloChuteFinal.size())
        vt2dIntervaloChuteFinal = vt2dIntervaloChute;

    return vt2dIntervaloChuteFinal;
}


bool Common::bPontoContidoRetangulo(QVector2D ponto,
                                    QVector2D R1,
                                    QVector2D R2){

    QRect retangulo(R1.toPoint(), R2.toPoint());
    return retangulo.contains(ponto.toPoint());

}


bool Common::bPontoDentroArea(QVector2D tamanhoCampo,
                              float larguraArea,
                              float profundidadeArea,
                              int ladoCampo,
                              QVector2D ponto){

    QVector2D P1Def = QVector2D(tamanhoCampo.x()/2*ladoCampo,
                                larguraArea/2),
            P2Def = QVector2D((tamanhoCampo.x()/2 - profundidadeArea)*ladoCampo,
                              -larguraArea/2);

    return Common::bPontoContidoRetangulo(ponto, P1Def, P2Def);

}


Team Common::timeTimeComABola(const QVector2D posBola,
                              const QVector<QVector3D>& _vt3dPosicoesRobosAmarelos,
                              const QVector<QVector3D>& _vt3dPosicoesRobosAzuis,
                              const float distanciaMaxima,
                              const float distanciaMinima ){

    QVector2D posRoboAmareloMaisProximo = QVector2D(9999999999.f, 999999999.f);

    QVector2D posRoboAzulMaisProximo = QVector2D(9999999999.f, 999999999.f);

    for (auto n : _vt3dPosicoesRobosAmarelos){
        if(n.toVector2D().distanceToPoint(posBola) < posRoboAmareloMaisProximo.distanceToPoint(posBola))
            posRoboAmareloMaisProximo = n.toVector2D();
    }

    for (auto n : _vt3dPosicoesRobosAzuis){
        if(n.toVector2D().distanceToPoint(posBola) < posRoboAzulMaisProximo.distanceToPoint(posBola))
            posRoboAzulMaisProximo = n.toVector2D();
    }

    float distanciaRoboAmarelo = posRoboAmareloMaisProximo.distanceToPoint(posBola);
    float distanciaRoboAzul = posRoboAzulMaisProximo.distanceToPoint(posBola);

    if(distanciaRoboAzul < distanciaMinima &&
       distanciaRoboAmarelo < distanciaMinima)
        return teamNone;

    if(distanciaRoboAzul > distanciaMaxima &&
       distanciaRoboAmarelo > distanciaMaxima)
        return teamNone;


    if(distanciaRoboAzul < distanciaMinima &&
       distanciaRoboAmarelo > distanciaMinima)
        return teamBlue;

    if(distanciaRoboAmarelo < distanciaMinima &&
       distanciaRoboAzul > distanciaMinima)
        return teamYelllow;


    if(distanciaRoboAzul < distanciaMaxima &&
       distanciaRoboAmarelo > distanciaMaxima)
        return teamBlue;

    if(distanciaRoboAmarelo < distanciaMaxima &&
       distanciaRoboAzul > distanciaMaxima)
        return teamYelllow;


    return teamNone;

}

quint8 Common::iEscalaVariavel(const double valor,
                               const double valorMinimo,
                               const double valorMaximo){


    if(valor < valorMinimo)
        return 0;

    if(valor > valorMaximo)
        return 250;

    return static_cast<quint8>( 250*((valor - valorMinimo)/(valorMaximo-valorMinimo)) +.5);
}


quint16 Common::iPontuacaoReceptor(const QVector2D posRobo,
                                   const QVector2D posBola,
                                   const QVector2D vetorVelocidadeBola){
    double angulo {Common::dAngleBetweenTwoVectors((posRobo - posBola), vetorVelocidadeBola)};

    if( angulo > 30)
        return 0;

    return static_cast<quint16>( (30 - angulo)*(30 - angulo));
}

///
/// \brief Retorna o delta X entre a posição do receptor e a posição do passador
/// \details Função utilizada para determinar se o passe é progressivo (em direção ao ataque) ou
/// regressivo (passe de recuo). Retorna um valor negativo caso o passe seja regressivo
/// e um valor positivo caso o passe seja progressivo.
/// \param posicaoReceptor
/// \param posicaoPassador
/// \param iLadoCampoDefesa
/// \return
///
double Common::dDeltaXis(const QVector2D posicaoReceptor,
                         const QVector2D posicaoPassador,
                         const qint8 iLadoCampoDefesa)
{
    double _dDeltaXis = posicaoReceptor.x() - posicaoPassador.x();

    if(iLadoCampoDefesa == XMINUS)
        return _dDeltaXis;

    return -_dDeltaXis;

}
