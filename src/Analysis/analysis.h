﻿/*
 * LogAnalyserRoboFEI-SSL
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ANALYSIS_H
#define ANALYSIS_H

#include "Environment/environment.h"
#include "Constants/constants.h"
#include "Common/common.h"
#include "analysiscommon.h"
#include "GameStatistics/teamstatistics.h"

#include <QElapsedTimer>
#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QDebug>
#include <QTimer>
#include <QFile>
#include <QDir>
#include <QDateTime>
#include <QPointer>


class Analysis : public QObject
{

    Q_OBJECT

public:
    Analysis(QString caminho,
            QString nome);
    ~Analysis();

private:

    void vAnalisaDados();

    QVector<qint8> vtiIdReceptorRecebendoPasse();

    TIPO_DE_JOGADA tdjJogadaAtual();

    void vRanqueiaPontuacoes();

    Jogada jogCriaNovaJogada(Team timeAtacante) const;

    void vComputaNotaJogadaFinal(const Jogada& jogada);

    void vGenerateStatisticsFrame();

    Environment *acAmbienteAnalise;

    Jogada jogadaEmAndamento;

    Jogada parallelMove;

    bool bParallelMoveExistis;

    ESTADOS_DE_JOGO estadoDeJogoAtual;

    QScopedPointer<QTimer> tmrExecutaAnalise;

    QString caminhoParaArquivo;
    QString nomeDoArquivo;

    QScopedPointer<QFile> fileArquivoAnalisesAmareloChute;
    QScopedPointer<QFile> fileArquivoAnalisesAzulChute;
    QScopedPointer<QFile> fileArquivoAnalisesAmareloPasse;
    QScopedPointer<QFile> fileArquivoAnalisesAzulPasse;

    QScopedPointer<TeamStatistics> statisticsYellow;
    QScopedPointer<TeamStatistics> statisticsBlue;


public slots:

    void SLOT_ativaDesativaAnalise(const bool bAtivar);

    void SLOT_dadosVisao(const Environment& acAmbiente,
                         const bool bMudouGeometria);

    void SLOT_dadosReferee(const int comando,
                           const quint8 idGoleiroAmarelo,
                           const quint8 idGoleiroAzul,
                           const qint8 iLadoCampoTimeAzul);

    void SLOT_jump(qint64 sleepTime);

private slots:

    void SLOT_executaAnalise();

    void SLOT_habilitarDesabilitarJogadaParalela(bool habilitar);

signals:

    void SIGNAL_jogadaAtualAnalise(const Jogada&);

    void SIGNAL_jogadaSecundariaAnalise(const Jogada&);

    void SIGNAL_habilitarDesabilitarJogadaSecundaria(bool habilitar);

    void SIGNAL_estadoDejogadaAtualAnalise(ESTADOS_DE_JOGO);

    void SIGNAL_statisticsData(const TeamStatistics& yellow, const TeamStatistics& blue);
};

#endif // ANALYSIS_H
