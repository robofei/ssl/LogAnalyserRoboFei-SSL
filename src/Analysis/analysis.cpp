/*
 * LogAnalyserRoboFEI-SSL
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "analysis.h"
#include "analysiscommon.h"

// Uncomment the line beside to allow the debug messages coming from this class
//#define ANALYSIS_IGNORE_DEBUG_MESSAGES


Analysis::Analysis(QString caminho,
                   QString nome) :
    acAmbienteAnalise(new Environment()),
    jogadaEmAndamento(Jogada()),
    parallelMove(Jogada()),
    bParallelMoveExistis(false),
    estadoDeJogoAtual(BOLA_EM_DISPUTA),
    tmrExecutaAnalise(nullptr),
    caminhoParaArquivo(caminho),
    nomeDoArquivo(nome),
    fileArquivoAnalisesAmareloChute(new QFile(this)),
    fileArquivoAnalisesAzulChute(new QFile(this)),
    fileArquivoAnalisesAmareloPasse(new QFile(this)),
    fileArquivoAnalisesAzulPasse(new QFile(this))
{


    connect(this, &Analysis::SIGNAL_habilitarDesabilitarJogadaSecundaria,
            this, &Analysis::SLOT_habilitarDesabilitarJogadaParalela);

    nomeDoArquivo.append(QDateTime::currentDateTime().toString(Qt::ISODateWithMs));

    nomeDoArquivo.replace(QChar(':'), QChar('_'), Qt::CaseInsensitive);
    nomeDoArquivo.replace(QChar('.'), QChar('_'), Qt::CaseInsensitive);
    nomeDoArquivo.replace(QChar(' '), QChar('_'), Qt::CaseInsensitive);

    QDir::setCurrent(caminhoParaArquivo);

    fileArquivoAnalisesAmareloChute->setFileName( nomeDoArquivo + "YellowShoot.csv" );
    fileArquivoAnalisesAzulChute->setFileName( nomeDoArquivo + "BlueShoot.csv" );

    fileArquivoAnalisesAmareloPasse->setFileName( nomeDoArquivo + "YellowPass.csv" );
    fileArquivoAnalisesAzulPasse->setFileName( nomeDoArquivo + "BluePass.csv" );


    if(!fileArquivoAnalisesAmareloChute->open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qCritical()<<"Não foi possível criar o arquivo: "<<QDir::currentPath()<<'/'<<fileArquivoAnalisesAmareloChute->fileName();

        qCritical()<<"Criando arquivo em: \"$HOME\". Novo arquivo \"testeAmareloChute.csv\"";

        QDir::setCurrent("$HOME");

        fileArquivoAnalisesAmareloChute->setFileName("testeAmareloChute.csv");
        if(!fileArquivoAnalisesAmareloChute->open(QIODevice::WriteOnly | QIODevice::Text))
            qFatal("Não foi possível abrir o arquivo para as análises");
    }

    {
        QTextStream stream(fileArquivoAnalisesAmareloChute.get());

        stream << QString("Free Angle of the path") << ';'
               << QString("Ball to Goal distance") << ';'
               << QString("Freedom of the Kicker") << ';'

               << QString("Score of the goal shooting move") << ';'

               << '\n';
    }

    if(!fileArquivoAnalisesAzulChute->open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qCritical()<<"Não foi possível criar o arquivo: "<<QDir::currentPath()<<'/'<<fileArquivoAnalisesAzulChute->fileName();

        qCritical()<<"Criando arquivo em: \"$HOME\". Novo arquivo \"testeAzulChute.csv\"";

        QDir::setCurrent("$HOME");

        fileArquivoAnalisesAzulChute->setFileName("testeAzulChute.csv");
        if(!fileArquivoAnalisesAzulChute->open(QIODevice::WriteOnly | QIODevice::Text))
            qFatal("Não foi possível abrir o arquivo para as análises");
    }

    {
        QTextStream stream(fileArquivoAnalisesAzulChute.get());

        stream << QString("Free Angle of the path") << ';'
               << QString("Ball to Goal distance") << ';'
               << QString("Freedom of the Kicker") << ';'

               << QString("Score of the goal shooting move") << ';'

               << '\n';

    }

    if(!fileArquivoAnalisesAmareloPasse->open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qCritical()<<"Não foi possível criar o arquivo: "<<QDir::currentPath()<<'/'<<fileArquivoAnalisesAmareloPasse->fileName();

        qCritical()<<"Criando arquivo em: \"$HOME\". Novo arquivo \"testeAmareloPasse.csv\"";

        QDir::setCurrent("$HOME");

        fileArquivoAnalisesAmareloPasse->setFileName("testeAmareloPasse.csv");
        if(!fileArquivoAnalisesAmareloPasse->open(QIODevice::WriteOnly | QIODevice::Text))
            qFatal("Não foi possível abrir o arquivo para as análises");
    }

    {
        QTextStream stream(fileArquivoAnalisesAmareloPasse.get());

        stream << QString("Freedom of the Path")<<';'
               << QString("Distance of the Pass")<<';'
               << QString("Freedom of Receiver")<<';'
               << QString("Redirect Angle")<<';'
               << QString("Receiver Free Angle for Goal Shooting")<<';'
               << QString("Receiver to Goal Distance")<<';'
               << QString("Freedom of the Passer")<<';'
               << QString("Delta Xis")<<';'

               << QString("Score of the Passing Move")

               << '\n';
    }

    if(!fileArquivoAnalisesAzulPasse->open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qCritical()<<"Não foi possível criar o arquivo: "<<QDir::currentPath()<<'/'<<fileArquivoAnalisesAzulPasse->fileName();

        qCritical()<<"Criando arquivo em: \"$HOME\". Novo arquivo \"testeAzulPasse.csv\"";

        QDir::setCurrent("$HOME");

        fileArquivoAnalisesAzulPasse->setFileName("testeAzulloPasse.csv");
        if(!fileArquivoAnalisesAzulPasse->open(QIODevice::WriteOnly | QIODevice::Text))
            qFatal("Não foi possível abrir o arquivo para as análises");
    }

    {
        QTextStream stream(fileArquivoAnalisesAzulPasse.get());

        stream << QString("Freedom of the Path")<<';'
               << QString("Distance of the Pass")<<';'
               << QString("Freedom of Receiver")<<';'
               << QString("Redirect Angle")<<';'
               << QString("Receiver Free Angle for Goal Shooting")<<';'
               << QString("Receiver to Goal Distance")<<';'
               << QString("Freedom of the Passer")<<';'
               << QString("Delta Xis")<<';'

               << QString("Score of the Passing Move")

               << '\n';
    }

    // Checks if the option of generate statistics is enabled
    // if(generateStatistics)
    if(true)
    {
        statisticsYellow.reset(new TeamStatistics);
        statisticsBlue.reset(new TeamStatistics);
    }

}

Analysis::~Analysis()
{
    delete acAmbienteAnalise;
}



void Analysis::vAnalisaDados()
{
    // todo: verificar com clareza quem é o chutador. Quando a bola está em disputa fica difícil
    //        de ter certeza e ocorrem muitos erros de análise nessas jogadas.
    //
    // todo: durante o estado `JOGADA_EM_ANDAMENTO`, muitas vezes é considerado que o robô perdeu a bola
    //        quando isso na verdade não aconteceu. Ajustar a sensibilidade desse processo.

    if(estadoDeJogoAtual == JOGADA_NENHUMA)
        return;

    if(estadoDeJogoAtual == BOLA_EM_DISPUTA)
    {
        if(acAmbienteAnalise->teamWithBall == teamYelllow)
        {
#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
            qInfo("O time amarelo tem a bola");
#endif
            jogadaEmAndamento = jogCriaNovaJogada(teamYelllow);

            estadoDeJogoAtual = JOGADA_EM_ANDAMENTO;
        }
        else if(acAmbienteAnalise->teamWithBall == teamBlue)
        {

#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
            qInfo("O time azul tem a bola");
#endif
            jogadaEmAndamento = jogCriaNovaJogada(teamBlue);

            estadoDeJogoAtual = JOGADA_EM_ANDAMENTO;
        }
        return;
    }

    const Team timeAtacante = jogadaEmAndamento.timeDaJogada;

    const Team timeDefensor =
            (timeAtacante == teamYelllow) ? teamBlue :
                                            (timeAtacante == teamBlue) ? teamYelllow :
                                                                         teamNone;


    const QString stringTime =
            (timeAtacante == teamYelllow) ? "Time amarelo: " :
                                            (timeAtacante == teamBlue) ? "Time azul: " :
                                                                         "Time nenhum: ";

    // Pass or Goal Shooting Move? -> old
    // Pass Move:
    //    Did the receiver get the ball?
    //    Yes:
    //       Go to Parallel move
    //    No: 000
    //
    // Goal Shooting Move:
    //    Did the ball get on the defense area?
    //    Yes:
    //       Have a goal ocorred?
    //       Yes: 250
    //       No, but was the ball going towards the goal when the goalkeeper caught it?
    //          Yes: 200
    //          No: 100
    //     No: 000
    //

    // Parallel move -> old
    //
    // Pass Move, Goal Shooting Move or Dribbling Move?
    // Pass Move:
    //    Did the receiver get the ball?
    //    Yes:
    //       200
    //    No:
    //       150
    // Goal Shooting Move:
    //    Did the ball get on the defense area?
    //    Yes:
    //       Have a goal ocorred?
    //       Yes: 250
    //       No, but was ball the going towards the goal when the goalkeeper caught it?
    //          Yes: 250
    //          No: 200
    //     No: 150
    // Dribbling Move:
    //    How long did he hold the ball for?
    //       4 secs or more: 200
    //       2 secs or more: 150
    //       less than 2 secs: 100




    // Pass or Goal Shooting Move?
    // Pass Move:
    //    Did the receiver get the ball?
    //    Yes: Go to Parallel move
    //    No: 000
    //
    // Goal Shooting Move:
    //    Did the ball get on the defense area?
    //    Yes:
    //       Have a goal ocorred?
    //       Yes: 250
    //       No, but was the ball going towards the goal when the goalkeeper caught it?
    //          Yes: 150
    //          No: 50
    //       No, and the goalkeeper did not get the ball: 100
    //     No: 000
    //

    // Parallel move
    //
    // Pass Move, Goal Shooting Move or Dribbling Move?
    // Pass Move:
    //    Did the receiver get the ball?
    //    Yes:
    //       200
    //    No:
    //       100
    // Goal Shooting Move:
    //    Did the ball get on the defense area?
    //    Yes:
    //       Have a goal ocorred?
    //       Yes: 250
    //       No, but was ball the going towards the goal when the goalkeeper caught it?
    //          Yes: 200
    //          No: 150
    //       No, and the goalkeeper did not get the ball: 150
    //     No: 100
    // Dribbling Move:
    //    How long did he/she hold the ball for?
    //       4 secs or more: 200
    //       more than 3 secs but less than 4 secs: 150
    //       more than 2 secs but less than 3 secs: 100
    //       less than 2 secs: 50

    switch(estadoDeJogoAtual)
    {
        case JOGADA_EM_ANDAMENTO:
        {
            // todo: Verificar se a bola está se movendo na angulação do robô.
            //       Isso pode ser importante.

            // Se a bola começou a se mover então inicia-se uma nova jogada.
            if(acAmbienteAnalise->bBallIsMoving())
            {
#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                qInfo() << qPrintable(stringTime + "Passe ou chute ao gol em andamento");
#endif
                estadoDeJogoAtual = PASSE_OU_CHUTE_EM_ANDAMENTO;

                jogadaEmAndamento.contadorJogada = 0;

                jogadaEmAndamento.vt2dPosicaoBolaInicioChute = acAmbienteAnalise->vt2dBallCurrentPosition();

                jogadaEmAndamento.vAtribuiParametrosDaJogadaInicio(acAmbienteAnalise);
            }
            // Se o time atacante ainda está com a bola ou se a bola não estiver em disputa
            else if(acAmbienteAnalise->teamWithBall != timeDefensor &&
                    acAmbienteAnalise->rbt(timeAtacante, jogadaEmAndamento.iIdRoboComABola).vt2dGetCurrentPosition().
                    distanceToPoint(acAmbienteAnalise->vt2dBallCurrentPosition()) < BALL_POSSESSION_MAXIMUM_DISTANCE)
            {
                // se o robô com a bola não for o robô da jogada
                if(acAmbienteAnalise->iIdBallOwner(timeAtacante) != jogadaEmAndamento.iIdRoboComABola)
                {
                    if(bParallelMoveExistis)
                    {
                        // Considera a jogada como passe bem-sucedido
                        parallelMove.notaAvaliazaoFinalDaJogada = NOTA_ALTA;
                        vComputaNotaJogadaFinal(parallelMove);

                        emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
                        parallelMove.vReinicia();
                    }

                    jogadaEmAndamento.vReinicia();
                    jogadaEmAndamento = jogCriaNovaJogada(timeAtacante);

                }
            }
            // Se o time atacante perdeu a bola
            else
            {
#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                qInfo("Bola em disputa");
#endif
                jogadaEmAndamento.vReinicia();

                estadoDeJogoAtual = BOLA_EM_DISPUTA;

                if(bParallelMoveExistis)
                {
                    // Avalia a jogada de acordo com o tempo que o robô ficou com a bola
                    if(jogadaEmAndamento.timerTempoDaJogada.nsecsElapsed()*1e-9 >  4)
                    {
                        parallelMove.notaAvaliazaoFinalDaJogada = NOTA_ALTA;
                    }
                    else if(jogadaEmAndamento.timerTempoDaJogada.nsecsElapsed()*1e-9 > 3)
                    {
                        parallelMove.notaAvaliazaoFinalDaJogada = NOTA_MEDIA;
                    }
                    else if(jogadaEmAndamento.timerTempoDaJogada.nsecsElapsed()*1e-9 > 2)
                    {
                        parallelMove.notaAvaliazaoFinalDaJogada = NOTA_BAIXA;
                    }
                    else
                    {
                        parallelMove.notaAvaliazaoFinalDaJogada = NOTA_RUIM;
                    }

                    vComputaNotaJogadaFinal(parallelMove);

                    emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
                    parallelMove.vReinicia();
                }
            }
        }return;

        case PASSE_EM_ANDAMENTO:
        {
            // Se a bola estiver próxima do receptor
            // Nota: a bola pode estar em movimento e próxima ao receptor, mesmo assim será considerado que o
            //  receptor recebeu o passe
            if(acAmbienteAnalise->bReceiverHasReceivedThePass(timeAtacante,
                                                              jogadaEmAndamento.jogadaPasse.iIdReceptor,
                                                              jogadaEmAndamento.vt2dPosicaoBolaInicioChute))
            {
#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                qDebug() << qPrintable(stringTime + "Receptor recebeu passe");
#endif

                if(bParallelMoveExistis)
                {
                    parallelMove.notaAvaliazaoFinalDaJogada = NOTA_ALTA;
                    vComputaNotaJogadaFinal(parallelMove);

                    parallelMove.vReinicia();
                }
                else
                {
                    emit SIGNAL_habilitarDesabilitarJogadaSecundaria(true);
                }

                parallelMove = jogadaEmAndamento;

                jogadaEmAndamento = jogCriaNovaJogada(timeAtacante);

                estadoDeJogoAtual = JOGADA_EM_ANDAMENTO;
                return;
            }

            if(acAmbienteAnalise->bRobotReceivingThePass(jogadaEmAndamento.jogadaPasse.iIdReceptor, timeAtacante) &&
               !acAmbienteAnalise->bBallDirectionHasChanged(jogadaEmAndamento.vt2dPosicaoBolaInicioChute))
            {
                jogadaEmAndamento.contadorJogada = 0;
            }
            else
            {
                jogadaEmAndamento.contadorJogada++;
            }

            // The receiver is not in the ball line direction
            // Let's see what happened
            if(jogadaEmAndamento.contadorJogada > iAnaliseFramesSemAcharReceptor)
            {
                // The ball is still moving and its direction is the same,
                //  maybe the pass is for another robot
                if(acAmbienteAnalise->bBallIsMoving() &&
                   !acAmbienteAnalise->bBallDirectionHasChanged(jogadaEmAndamento.vt2dPosicaoBolaInicioChute))
                {
                    for (quint8 i=0; i<PLAYERS_PER_SIDE; i++)
                    {
                        if(acAmbienteAnalise->bRobotReceivingThePass(i, timeAtacante))
                        {
                            jogadaEmAndamento.jogadaPasse.iIdReceptor = -2;

                            for (quint8 i=0; i<PLAYERS_PER_SIDE; i++)
                                jogadaEmAndamento.aiPontuacoesPasse[i] = 0;

                            jogadaEmAndamento.iPontuacaoChute = 0;

                            jogadaEmAndamento.iPontuacaoMaxima = 0;

                            jogadaEmAndamento.contadorFramesPasseOuChuteEmAndamento = 0;

                            estadoDeJogoAtual = PASSE_OU_CHUTE_EM_ANDAMENTO;
                            return;
                        }
                    }

                    // Or maybe the ball is going towards the goal
                    if(acAmbienteAnalise->bGoalShootInProgress(timeAtacante))
                    {
                        jogadaEmAndamento.jogadaPasse.iIdReceptor = -2;

                        for (quint8 i=0; i<PLAYERS_PER_SIDE; i++)
                            jogadaEmAndamento.aiPontuacoesPasse[i] = 0;

                        jogadaEmAndamento.iPontuacaoChute = 0;

                        jogadaEmAndamento.iPontuacaoMaxima = 0;

                        jogadaEmAndamento.contadorFramesPasseOuChuteEmAndamento = 0;

                        estadoDeJogoAtual = PASSE_OU_CHUTE_EM_ANDAMENTO;
                        return;
                    }
                }
                // The passer still has the ball
                else if(acAmbienteAnalise->teamWithBall != timeDefensor &&
                        acAmbienteAnalise->rbt(timeAtacante, jogadaEmAndamento.iIdRoboComABola).
                                                   vt2dGetCurrentPosition().distanceToPoint(acAmbienteAnalise->vt2dBallCurrentPosition())
                                                                                            < BALL_POSSESSION_DISTANCE)
                {
                    jogadaEmAndamento.jogadaPasse.iIdReceptor = -2;

                    for (quint8 i=0; i<PLAYERS_PER_SIDE; i++)
                        jogadaEmAndamento.aiPontuacoesPasse[i] = 0;

                    jogadaEmAndamento.iPontuacaoChute = 0;
                    jogadaEmAndamento.iPontuacaoMaxima = 0;
                    jogadaEmAndamento.contadorFramesPasseOuChuteEmAndamento = 0;
                    estadoDeJogoAtual = JOGADA_EM_ANDAMENTO;

                    return;
                }
                // Checks if the ball has been intercepted
                else if(acAmbienteAnalise->teamWithBall == timeDefensor)
                {
#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                    qDebug() << stringTime << "Passe malsucedido, bola interceptada";
#endif
                    jogadaEmAndamento.notaAvaliazaoFinalDaJogada = NOTA_NULA;

                    vComputaNotaJogadaFinal(jogadaEmAndamento);

                    estadoDeJogoAtual = BOLA_EM_DISPUTA;
                    jogadaEmAndamento.vReinicia();

                    if(bParallelMoveExistis)
                    {
                        parallelMove.notaAvaliazaoFinalDaJogada = NOTA_BAIXA;
                        vComputaNotaJogadaFinal(parallelMove);

                        emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
                        parallelMove.vReinicia();
                    }
                    return;
                }
                // If we came this far, probably another allied robot received
                //  the pass already
                else
                {
                    for (quint8 i=0; i<PLAYERS_PER_SIDE; i++)
                    {
                        if(jogadaEmAndamento.iIdRoboComABola == i)
                            continue;

                        if(acAmbienteAnalise->iIdBallOwner(timeAtacante) == i &&
                           acAmbienteAnalise->rbt(timeAtacante, i).
                           vt2dGetCurrentPosition().distanceToPoint(acAmbienteAnalise->vt2dBallCurrentPosition())
                           < BALL_POSSESSION_DISTANCE)
                        {
#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                            qWarning() << qPrintable(stringTime + "Jogada interpretada de maneira incorreta");
#endif
                            jogadaEmAndamento.tdjTipoDeJogada = PASSE;
                            jogadaEmAndamento.jogadaPasse.iIdReceptor = i;

#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                            qDebug() << qPrintable(stringTime + "Receptor recebeu passe");
#endif

                            if(bParallelMoveExistis)
                            {
                                parallelMove.notaAvaliazaoFinalDaJogada = NOTA_ALTA;
                                vComputaNotaJogadaFinal(parallelMove);

                                parallelMove.vReinicia();
                            }
                            else
                            {
                                emit SIGNAL_habilitarDesabilitarJogadaSecundaria(true);
                            }

                            parallelMove = jogadaEmAndamento;

                            jogadaEmAndamento = jogCriaNovaJogada(timeAtacante);
                            estadoDeJogoAtual = JOGADA_EM_ANDAMENTO;

                            return;
                        }
                    }

                    // If we came this far then we discard the current move
                    //  but consider the parallel move
                    if(bParallelMoveExistis)
                    {
                        parallelMove.notaAvaliazaoFinalDaJogada = NOTA_BAIXA;
                        vComputaNotaJogadaFinal(parallelMove);
                        emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
                        parallelMove.vReinicia();
                    }

                    estadoDeJogoAtual = BOLA_EM_DISPUTA;
                    jogadaEmAndamento.vReinicia();
                    return;

                }

                jogadaEmAndamento.notaAvaliazaoFinalDaJogada = NOTA_NULA;
                vComputaNotaJogadaFinal(jogadaEmAndamento);

                estadoDeJogoAtual = BOLA_EM_DISPUTA;
                jogadaEmAndamento.vReinicia();

                if(bParallelMoveExistis)
                {
                    parallelMove.notaAvaliazaoFinalDaJogada = NOTA_BAIXA;
                    vComputaNotaJogadaFinal(parallelMove);

                    emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
                    parallelMove.vReinicia();
                }
            }
        }return;

        case CHUTE_AO_GOL_EM_ANDAMENT0:
        {
            // Is the ball going towards the goal
            if(acAmbienteAnalise->iGoolShootScore(timeAtacante) > 800)
            {
                jogadaEmAndamento.jogadaGol.iBolaEmDirecaoAoGolContador++;
            }
            else
            {
                jogadaEmAndamento.jogadaGol.iBolaNaoEmDirecaoAoGol++;
            }

            // The ball is in the area already
            if(jogadaEmAndamento.jogadaGol.bBolaDentroDaArea)
            {
                // todo: Sometimes the goal ocorred but it claims that the goalkeeper has
                // caught the ball.
                if(acAmbienteAnalise->bIsGool(timeAtacante))
                {
#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                    qInfo() << qPrintable(stringTime + "GOL");
                    qDebug() << qPrintable(stringTime + "GOL");
#endif

                    jogadaEmAndamento.notaAvaliazaoFinalDaJogada = NOTA_MAXIMA;

                    vComputaNotaJogadaFinal(jogadaEmAndamento);

                    estadoDeJogoAtual = BOLA_EM_DISPUTA;
                    jogadaEmAndamento.vReinicia();

                    if(bParallelMoveExistis)
                    {
                        parallelMove.notaAvaliazaoFinalDaJogada = NOTA_MAXIMA;
                        vComputaNotaJogadaFinal(parallelMove);

                        emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
                        parallelMove.vReinicia();
                    }
                    return;
                }
                else if(acAmbienteAnalise->bBallOutOfField())
                {
#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                    qDebug() << stringTime << "Para fora";
#endif

                    jogadaEmAndamento.notaAvaliazaoFinalDaJogada = NOTA_BAIXA;

                    vComputaNotaJogadaFinal(jogadaEmAndamento);

                    estadoDeJogoAtual = BOLA_EM_DISPUTA;
                    jogadaEmAndamento.vReinicia();

                    if(bParallelMoveExistis)
                    {
                        parallelMove.notaAvaliazaoFinalDaJogada = NOTA_MEDIA;
                        vComputaNotaJogadaFinal(parallelMove);

                        emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
                        parallelMove.vReinicia();
                    }
                    return;
                }

                if(!acAmbienteAnalise->bBallIsMoving() ||
                   acAmbienteAnalise->bBallDirectionHasChanged(jogadaEmAndamento.vt2dPosicaoBolaInicioChute, 10))
                {
                    if(acAmbienteAnalise->bGoolkepperHasDefended(timeAtacante) ||
                       acAmbienteAnalise->bBallInsideTheDefenseArea(timeAtacante))
                    {
#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                        qDebug() << stringTime << "O goleiro defendeu";
#endif

                        // The ball was going towards the goal
                        if(jogadaEmAndamento.jogadaGol.iBolaEmDirecaoAoGolContador >
                           jogadaEmAndamento.jogadaGol.iBolaNaoEmDirecaoAoGol)
                        {
                            jogadaEmAndamento.notaAvaliazaoFinalDaJogada = NOTA_MEDIA;
                        }
                        else
                        {
                            jogadaEmAndamento.notaAvaliazaoFinalDaJogada = NOTA_RUIM;
                        }

                        vComputaNotaJogadaFinal(jogadaEmAndamento);

                        if(bParallelMoveExistis)
                        {
                            // The ball was going towards the goal
                            if(jogadaEmAndamento.jogadaGol.iBolaEmDirecaoAoGolContador >
                               jogadaEmAndamento.jogadaGol.iBolaNaoEmDirecaoAoGol)
                            {
                                parallelMove.notaAvaliazaoFinalDaJogada = NOTA_ALTA;
                            }
                            else
                            {
                                parallelMove.notaAvaliazaoFinalDaJogada = NOTA_MEDIA;
                            }

                            vComputaNotaJogadaFinal(parallelMove);

                            emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
                            parallelMove.vReinicia();
                        }

                        estadoDeJogoAtual = BOLA_EM_DISPUTA;
                        jogadaEmAndamento.vReinicia();

                    }
                    else if(!acAmbienteAnalise->bBallInsideTheDefenseArea(timeAtacante))
                    {
                        if(bParallelMoveExistis)
                        {
                            parallelMove.notaAvaliazaoFinalDaJogada = NOTA_BAIXA;
                            vComputaNotaJogadaFinal(parallelMove);

                            emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
                            parallelMove.vReinicia();
                        }
                        estadoDeJogoAtual = BOLA_EM_DISPUTA;
                        jogadaEmAndamento.vReinicia();
                    }

                }


                return;
            }

            if(acAmbienteAnalise->bBallInsideTheDefenseArea(timeAtacante))
            {
#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                qDebug() << stringTime << "Bola chegou na área adversária";
#endif

                jogadaEmAndamento.jogadaGol.bBolaDentroDaArea = true;
                return;
            }

            if(acAmbienteAnalise->bGoalShootInProgress(timeAtacante) &&
               !acAmbienteAnalise->bBallDirectionHasChanged(jogadaEmAndamento.vt2dPosicaoBolaInicioChute))
            {
                jogadaEmAndamento.contadorJogada = 0;
            }
            else
            {
                jogadaEmAndamento.contadorJogada++;
            }

            // The ball is not going towards the goal anymore, let's parse it
            if(jogadaEmAndamento.contadorJogada > iAnaliseFramesSemAcharReceptor)
            {
                if(acAmbienteAnalise->bBallIsMoving() &&
                   !acAmbienteAnalise->bBallDirectionHasChanged(jogadaEmAndamento.vt2dPosicaoBolaInicioChute))
                {
                    jogadaEmAndamento.jogadaPasse.iIdReceptor = -2;

                    for(quint8 i=0; i<PLAYERS_PER_SIDE; i++)
                        jogadaEmAndamento.aiPontuacoesPasse[i] = 0;

                    jogadaEmAndamento.iPontuacaoChute = 0;

                    jogadaEmAndamento.iPontuacaoMaxima = 0;

                    jogadaEmAndamento.contadorFramesPasseOuChuteEmAndamento = 0;
                    estadoDeJogoAtual = PASSE_OU_CHUTE_EM_ANDAMENTO;
                    return;
                }
                // The passer still has the ball
                else if(acAmbienteAnalise->teamWithBall != timeDefensor &&
                        acAmbienteAnalise->rbt(timeAtacante, jogadaEmAndamento.iIdRoboComABola).
                                                   vt2dGetCurrentPosition().distanceToPoint(acAmbienteAnalise->vt2dBallCurrentPosition())
                                                                                            < BALL_POSSESSION_DISTANCE)
                {
                    jogadaEmAndamento.jogadaPasse.iIdReceptor = -2;

                    for (quint8 i=0; i<PLAYERS_PER_SIDE; i++)
                        jogadaEmAndamento.aiPontuacoesPasse[i] = 0;

                    jogadaEmAndamento.iPontuacaoChute = 0;
                    jogadaEmAndamento.iPontuacaoMaxima = 0;
                    jogadaEmAndamento.contadorFramesPasseOuChuteEmAndamento = 0;
                    estadoDeJogoAtual = JOGADA_EM_ANDAMENTO;

                    return;
                }
                else
                {
                    for (quint8 i=0; i<PLAYERS_PER_SIDE; i++)
                    {
                        if(jogadaEmAndamento.iIdRoboComABola == i)
                            continue;

                        if(acAmbienteAnalise->iIdBallOwner(timeAtacante) == i &&
                           acAmbienteAnalise->rbt(timeAtacante, i).
                           vt2dGetCurrentPosition().distanceToPoint(acAmbienteAnalise->vt2dBallCurrentPosition())
                           < BALL_POSSESSION_DISTANCE)
                        {

#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                            qWarning() << stringTime << "Jogada interpretada de maneira incorreta";
#endif
                            jogadaEmAndamento.tdjTipoDeJogada = PASSE;
                            jogadaEmAndamento.jogadaPasse.iIdReceptor = i;

#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                            qDebug() << stringTime << "Receptor recebeu passe";
#endif
                            if(bParallelMoveExistis)
                            {
                                parallelMove.notaAvaliazaoFinalDaJogada = NOTA_ALTA;
                                vComputaNotaJogadaFinal(parallelMove);

                                parallelMove.vReinicia();
                            }
                            else
                            {
                                emit SIGNAL_habilitarDesabilitarJogadaSecundaria(true);
                            }

                            parallelMove = jogadaEmAndamento;

                            jogadaEmAndamento = jogCriaNovaJogada(timeAtacante);

                            estadoDeJogoAtual = JOGADA_EM_ANDAMENTO;

                            return;
                        }
                    }

                    // If we got this far then the goal shoot failed
#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                    qDebug() << stringTime << "Chute ao gol malsucedido";
#endif
                    jogadaEmAndamento.notaAvaliazaoFinalDaJogada = NOTA_NULA;

                    vComputaNotaJogadaFinal(jogadaEmAndamento);

                    estadoDeJogoAtual = BOLA_EM_DISPUTA;
                    jogadaEmAndamento.vReinicia();

                    if(bParallelMoveExistis)
                    {
                        parallelMove.notaAvaliazaoFinalDaJogada = NOTA_BAIXA;
                        vComputaNotaJogadaFinal(parallelMove);

                        emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
                        parallelMove.vReinicia();
                    }
                }
            }
        }return;

        case PASSE_OU_CHUTE_EM_ANDAMENTO:
        {
            if(acAmbienteAnalise->bBallIsMoving() &&
               !acAmbienteAnalise->bBallDirectionHasChanged(jogadaEmAndamento.vt2dPosicaoBolaInicioChute))
            {
                jogadaEmAndamento.contadorJogada = 0;
            }
            else
            {
                jogadaEmAndamento.contadorJogada++;
            }

            if(jogadaEmAndamento.contadorJogada > 2*iAnaliseFramesSemAcharReceptor)
            {
                if(acAmbienteAnalise->teamWithBall != timeDefensor &&
                   acAmbienteAnalise->rbt(timeAtacante, jogadaEmAndamento.iIdRoboComABola).
                                              vt2dGetCurrentPosition().distanceToPoint(acAmbienteAnalise->vt2dBallCurrentPosition())
                                                                                       < BALL_POSSESSION_DISTANCE)
                {
                    // O atacante ainda está com a bola, volta ao estado anterior
                    estadoDeJogoAtual = JOGADA_EM_ANDAMENTO;
                }
                else if(acAmbienteAnalise->teamWithBall == timeAtacante)
                {
                    // O time atacante permanece com a bola, mas a bola está com outro robô.
                    if(bParallelMoveExistis)
                    {
                        emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
                        parallelMove.vReinicia();
                    }

                    jogadaEmAndamento.vReinicia();
                    jogadaEmAndamento = jogCriaNovaJogada(timeAtacante);

                    estadoDeJogoAtual = JOGADA_EM_ANDAMENTO;
                }
                else
                {
                    // Bola perdida
#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                    qWarning("Bola em disputa");
#endif
                    estadoDeJogoAtual = BOLA_EM_DISPUTA;
                    jogadaEmAndamento.vReinicia();

                    if(bParallelMoveExistis)
                    {
                        parallelMove.notaAvaliazaoFinalDaJogada = NOTA_MEDIA;
                        vComputaNotaJogadaFinal(parallelMove);

                        emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
                        parallelMove.vReinicia();
                    }
                }
                return;
            }

            jogadaEmAndamento.vtiReceptores = vtiIdReceptorRecebendoPasse();
            jogadaEmAndamento.tdjTipoDeJogada = tdjJogadaAtual();

            jogadaEmAndamento.contadorFramesPasseOuChuteEmAndamento++;

            if(jogadaEmAndamento.tdjTipoDeJogada == PASSE)
            {

#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                qDebug() << stringTime << "Jogada de passe detectada";
#endif

                estadoDeJogoAtual = PASSE_EM_ANDAMENTO;
            }

            else if(jogadaEmAndamento.tdjTipoDeJogada == CHUTE)
            {

#ifdef ANALYSIS_IGNORE_DEBUG_MESSAGES
                qDebug() << stringTime << "Jogada de chute detectada";
#endif
                estadoDeJogoAtual = CHUTE_AO_GOL_EM_ANDAMENT0;
            }

            else if(jogadaEmAndamento.contadorFramesPasseOuChuteEmAndamento > 20 )
            {
                jogadaEmAndamento.jogadaPasse.iIdReceptor = -2;

                for (quint8 i=0; i<PLAYERS_PER_SIDE; i++)
                    jogadaEmAndamento.aiPontuacoesPasse[i] = 0;

                jogadaEmAndamento.iPontuacaoChute = 0;

                jogadaEmAndamento.iPontuacaoMaxima = 0;

                jogadaEmAndamento.contadorFramesPasseOuChuteEmAndamento = 0;
            }
        }return;

        default:
            break;

    }

}


QVector<qint8> Analysis::vtiIdReceptorRecebendoPasse(){

    vRanqueiaPontuacoes();

    QVector<qint8> vtiR{};

    for (quint8 id=0; id<PLAYERS_PER_SIDE; id++)
    {
        if(jogadaEmAndamento.aiPontuacoesPasse[id] > jogadaEmAndamento.iPontuacaoMaxima/2)
            vtiR.append(id);
    }

    if(jogadaEmAndamento.iPontuacaoChute > jogadaEmAndamento.iPontuacaoMaxima/2)
        vtiR.append(CHUTE_AO_GOL);

    if(vtiR.isEmpty())
        vtiR.append(NENHUM);

    return vtiR;

}


TIPO_DE_JOGADA Analysis::tdjJogadaAtual(){

    if(jogadaEmAndamento.iPontuacaoMaxima > 5000 )
    {
        jogadaEmAndamento.contadorJogada = 0;

        for (quint8 id=0; id<PLAYERS_PER_SIDE; id++)
        {
            if(jogadaEmAndamento.aiPontuacoesPasse[id] >= jogadaEmAndamento.iPontuacaoMaxima)
            {
                jogadaEmAndamento.jogadaPasse.iIdReceptor = id;
                return PASSE;
            }
        }

        return CHUTE;
    }

    return PASSE_OU_CHUTE;
}


void Analysis::vRanqueiaPontuacoes(){

    if(jogadaEmAndamento.timeDaJogada == teamYelllow)
    {
        for (qint8 id=0; id<PLAYERS_PER_SIDE; id++)
        {
            if(id != acAmbienteAnalise->iIdYellowGoalKepper &&
               acAmbienteAnalise->rbtYellow[id].getAttributes().bOnField &&
               id != jogadaEmAndamento.iIdRoboComABola)
            {
                jogadaEmAndamento.aiPontuacoesPasse[id] += acAmbienteAnalise->iRobotScoreForReceiving(teamYelllow, id);
            }
        }

        jogadaEmAndamento.iPontuacaoChute += acAmbienteAnalise->iGoolShootScore(teamYelllow);
    }

    if(jogadaEmAndamento.timeDaJogada == teamBlue)
    {
        for (qint8 id=0; id<PLAYERS_PER_SIDE; id++)
        {
            if(id != acAmbienteAnalise->iIdBlueGoalKepper &&
               acAmbienteAnalise->rbtBlue[id].getAttributes().bOnField &&
               id != jogadaEmAndamento.iIdRoboComABola)
            {
                jogadaEmAndamento.aiPontuacoesPasse[id] +=
                        acAmbienteAnalise->iRobotScoreForReceiving(teamBlue, id);
            }
            else
            {
                jogadaEmAndamento.aiPontuacoesPasse[id] = 0;
            }

        }

        jogadaEmAndamento.iPontuacaoChute +=
                acAmbienteAnalise->iGoolShootScore(teamBlue);
    }

    for (const auto& n : jogadaEmAndamento.aiPontuacoesPasse)
    {
        if(n > jogadaEmAndamento.iPontuacaoMaxima)
        {
            jogadaEmAndamento.iPontuacaoMaxima = n;

        }
    }

    if(jogadaEmAndamento.iPontuacaoChute > jogadaEmAndamento.iPontuacaoMaxima)
        jogadaEmAndamento.iPontuacaoMaxima = jogadaEmAndamento.iPontuacaoChute;

}

Jogada Analysis::jogCriaNovaJogada(Team timeAtacante) const
{

    if(timeAtacante == teamYelllow)
    {
        Jogada novaJogada = Jogada();

        novaJogada.timeDaJogada = teamYelllow;
        novaJogada.iIdRoboComABola = acAmbienteAnalise->iIdBallOwner(teamYelllow);
        novaJogada.timerTempoDaJogada.start();

        return novaJogada;

    }

    if(timeAtacante == teamBlue)
    {
        Jogada novaJogada = Jogada();

        novaJogada.timeDaJogada = teamBlue;
        novaJogada.iIdRoboComABola = acAmbienteAnalise->iIdBallOwner(teamBlue);
        novaJogada.timerTempoDaJogada.start();

        return novaJogada;

    }

    return Jogada();

}

void Analysis::vComputaNotaJogadaFinal(const Jogada& jogada)
{
    if(jogada.timeDaJogada == teamNone)
        return;

    const QString teamMove = (jogada.timeDaJogada == teamYelllow) ? "Yellow" : "Blue";

    if(jogada.tdjTipoDeJogada == CHUTE)
    {   
        QFile* file = (jogada.timeDaJogada == teamYelllow) ?
                          fileArquivoAnalisesAmareloChute.get() : fileArquivoAnalisesAzulChute.get();

        const NotaGol& notaChuteInicio = jogada.jogadaGol.notaGolInicioJogada;

        QTextStream stream(file);

        stream << QString::number(notaChuteInicio.iAnguloLivreCaminho)<<';'
               << QString::number(notaChuteInicio.iDistanciaBolaGolAdversario)<<';'
               << QString::number(notaChuteInicio.iLiberdadeDoChutador)<<';'

               << QString::number(jogada.notaAvaliazaoFinalDaJogada)
               << '\n' ;



        QString debugMessage = '\n' + teamMove + " team: Shoot move finished\n";
        debugMessage.append("Shooter ID: " + QString::number(jogada.iIdRoboComABola) + '\n');
        debugMessage.append("Score: " + QString::number(jogada.notaAvaliazaoFinalDaJogada));

        qInfo() << qPrintable(debugMessage);

//        switch (jogada.notaAvaliazaoFinalDaJogada)
//        {
//            case NOTA_NULA:
//                qInfo()<<"Jogada de Chute finalizada: NOTA NULA";
//                break;

//            case NOTA_RUIM:
//                qInfo()<<"Jogada de Chute finalizada: NOTA RUIM";
//                break;

//            case NOTA_BAIXA:
//                qInfo()<<"Jogada de Chute finalizada: NOTA BAIXA";
//                break;

//            case NOTA_MEDIA:
//                qInfo()<<"Jogada de Chute finalizada: NOTA MÉDIA";
//                break;

//            case NOTA_ALTA:
//                qInfo()<<"Jogada de Chute finalizada: NOTA ALTA";
//                break;

//            case NOTA_MAXIMA:
//                qInfo()<<"Jogada de Chute finalizada: NOTA MÁXIMA";
//                break;

//        }


    }

    else if(jogada.tdjTipoDeJogada == PASSE)
    {
        QString debugMessage = '\n' + teamMove + " team: Pass move finished\n";
        debugMessage.append("Passer ID: " + QString::number(jogada.iIdRoboComABola) + '\n');
        debugMessage.append("Receiver ID: " + QString::number(jogada.jogadaPasse.iIdReceptor) + '\n');
        debugMessage.append("Score: " + QString::number(jogada.notaAvaliazaoFinalDaJogada));

        qInfo() << qPrintable(debugMessage);


        QFile* file = (jogada.timeDaJogada == teamYelllow) ?
                          fileArquivoAnalisesAmareloPasse.get() : fileArquivoAnalisesAzulPasse.get();

        const NotaPasse& notaInicio = jogada.jogadaPasse.notaPasseInicioJogada[jogada.jogadaPasse.iIdReceptor];

        if(notaInicio.iAnguloRedirect == 0 &&
           notaInicio.iDeltaXisDoPasse == 0 &&
           notaInicio.iDistanciaDoPasse == 0 &&
           notaInicio.iAnguloLivreCaminho == 0 &&
           notaInicio.iLiberdadeDeMarcacao == 0 &&
           notaInicio.iLiberdadeDoPassador == 0 &&
           notaInicio.iDistanciaGolReceptor == 0 &&
           notaInicio.iAnguloLivreParaChuteReceptor == 0)
        {
            // something is wrong, I can feel it
            qCritical("Passe com nota nula descartado");
            return;
        }


        QTextStream stream(file);

        stream << QString::number(notaInicio.iAnguloLivreCaminho)<<';'
               << QString::number(notaInicio.iDistanciaDoPasse)<<';'
               << QString::number(notaInicio.iLiberdadeDeMarcacao)<<';'
               << QString::number(notaInicio.iAnguloRedirect)<<';'
               << QString::number(notaInicio.iAnguloLivreParaChuteReceptor)<<';'
               << QString::number(notaInicio.iDistanciaGolReceptor)<<';'
               << QString::number(notaInicio.iLiberdadeDoPassador)<<';'
               << QString::number(notaInicio.iDeltaXisDoPasse)<<';'

               << QString::number(jogada.notaAvaliazaoFinalDaJogada)

               << '\n' ;

//        switch (jogada.notaAvaliazaoFinalDaJogada)
//        {
//            case NOTA_NULA:
//                qInfo()<<"Jogada de Passe finalizada: NOTA NULA";
//                break;

//            case NOTA_RUIM:
//                qInfo()<<"Jogada de Passe finalizada: NOTA RUIM";
//                break;

//            case NOTA_BAIXA:
//                qInfo()<<"Jogada de Passe finalizada: NOTA BAIXA";
//                break;

//            case NOTA_MEDIA:
//                qInfo()<<"Jogada de Passe finalizada: NOTA MÉDIA";
//                break;

//            case NOTA_ALTA:
//                qInfo()<<"Jogada de Passe finalizada: NOTA ALTA";
//                break;

//            case NOTA_MAXIMA:
//                qInfo()<<"Jogada de Passe finalizada: NOTA MÁXIMA";
//                break;
//        }
    }

    if(statisticsYellow)
    {
        TeamStatistics& statistics = jogada.timeDaJogada == teamYelllow ?
                                         *statisticsYellow : *statisticsBlue;

        if(jogada.tdjTipoDeJogada == CHUTE)
        {
            if(jogada.jogadaGol.iBolaEmDirecaoAoGolContador > jogada.jogadaGol.iBolaNaoEmDirecaoAoGol &&
               jogada.jogadaGol.bBolaDentroDaArea)
            {
                ++statistics.shotsOnGoal;
            }
            else
            {
                ++statistics.shotsOutOfTheGoal;
            }
        }
        else if(jogada.tdjTipoDeJogada == PASSE)
        {
            if(jogada.notaAvaliazaoFinalDaJogada > 0)
            {
                ++statistics.correctPasses;
            }
            else
            {
                ++statistics.wrongPasses;
            }
        }
    }
}

void Analysis::vGenerateStatisticsFrame()
{
    static SSL_Referee_Command currentCommand = SSL_Referee_Command_HALT;

    // Checks if the command has changed
    if(currentCommand != acAmbienteAnalise->currentRefereeCommand)
    {
        const SSL_Referee_Command& c = acAmbienteAnalise->currentRefereeCommand;

        switch(c)
        {
            case SSL_Referee_Command_INDIRECT_FREE_BLUE:
            case SSL_Referee_Command_DIRECT_FREE_BLUE:
                ++statisticsBlue->freeKicks;
                break;

            case SSL_Referee_Command_INDIRECT_FREE_YELLOW:
            case SSL_Referee_Command_DIRECT_FREE_YELLOW:
                ++statisticsYellow->freeKicks;
                break;

            default:
                break;
        }

        currentCommand = acAmbienteAnalise->currentRefereeCommand;
    }

    // Checks the ball possession
    if(jogadaEmAndamento.timeDaJogada == teamYelllow)
    {
        ++statisticsYellow->framesWithBal;
    }
    else if(jogadaEmAndamento.timeDaJogada == teamBlue)
    {
        ++statisticsBlue->framesWithBal;
    }

    {
        static quint16 counter = 0;

        if(counter > 100)
        {
            emit SIGNAL_statisticsData(*statisticsYellow, *statisticsBlue);
            counter = 0;
        }
        else
        {
            ++counter;
        }
    }
}


void Analysis::SLOT_ativaDesativaAnalise(const bool bAtivar)
{
    if(bAtivar)
    {
        tmrExecutaAnalise.reset( new QTimer(this) );
        tmrExecutaAnalise->setTimerType(Qt::PreciseTimer);

        connect(tmrExecutaAnalise.get(),
                &QTimer::timeout,
                this,
                &Analysis::SLOT_executaAnalise);

        tmrExecutaAnalise->start(TIME_LOOP_ANALYSIS);
    }
    else
    {
        tmrExecutaAnalise->stop();
    }
}

void Analysis::SLOT_dadosVisao(const Environment& acAmbiente, const bool bMudouGeometria)
{
    acAmbienteAnalise->vVisionUpdate(acAmbiente);

    acAmbienteAnalise->vUpdateTeamWithTheBall();

    if(bMudouGeometria)
    {
        //Atualiza informacoes sobre as geometrias do campo
        acAmbienteAnalise->vSetDefenseAreaWidth(acAmbiente.iGetDefenseAreaWidth());
        acAmbienteAnalise->vSetDefenseAreaHeight(acAmbiente.iGetDefenseAreaHeigth());
        acAmbienteAnalise->vSetGoalWidth(acAmbiente.iGetGoalWidth());
        acAmbienteAnalise->vSetGoalDepth(acAmbiente.iGetGoalDepth());
        acAmbienteAnalise->vSetCenterRadius(acAmbiente.iGetCenterRadius());
        acAmbienteAnalise->setFieldSize(acAmbiente.getFieldSize());
    }

}

void Analysis::SLOT_dadosReferee(const int comando,
                                 const quint8 idGoleiroAmarelo,
                                 const quint8 idGoleiroAzul,
                                 const qint8 iLadoCampoTimeAzul)
{
    acAmbienteAnalise->currentRefereeCommand = static_cast<SSL_Referee_Command>(comando);

    acAmbienteAnalise->iIdBlueGoalKepper = idGoleiroAzul;

    acAmbienteAnalise->iIdYellowGoalKepper = idGoleiroAmarelo;

    acAmbienteAnalise->iBlueSide = iLadoCampoTimeAzul;
    acAmbienteAnalise->iYellowSide = -iLadoCampoTimeAzul;
}

void Analysis::SLOT_jump(qint64 sleepTime)
{
    jogadaEmAndamento.vReinicia();
    parallelMove.vReinicia();
    estadoDeJogoAtual = BOLA_EM_DISPUTA;

    if(bParallelMoveExistis)
    {
        emit SIGNAL_habilitarDesabilitarJogadaSecundaria(false);
    }

    emit SIGNAL_jogadaAtualAnalise(Jogada());

    emit SIGNAL_estadoDejogadaAtualAnalise(JOGADA_NENHUMA);
}

void Analysis::SLOT_executaAnalise()
{
    QElapsedTimer elapsedTempoDeExecucao;
    elapsedTempoDeExecucao.start();

    if(acAmbienteAnalise->currentRefereeCommand == SSL_Referee_Command_NORMAL_START       ||
       acAmbienteAnalise->currentRefereeCommand == SSL_Referee_Command_FORCE_START        ||
       acAmbienteAnalise->currentRefereeCommand == SSL_Referee_Command_DIRECT_FREE_BLUE   ||
       acAmbienteAnalise->currentRefereeCommand == SSL_Referee_Command_DIRECT_FREE_YELLOW ||
       acAmbienteAnalise->currentRefereeCommand == SSL_Referee_Command_INDIRECT_FREE_BLUE ||
       acAmbienteAnalise->currentRefereeCommand == SSL_Referee_Command_INDIRECT_FREE_YELLOW)
    {
        vAnalisaDados();

        emit SIGNAL_jogadaAtualAnalise(jogadaEmAndamento);

        emit SIGNAL_estadoDejogadaAtualAnalise(estadoDeJogoAtual);

        if(bParallelMoveExistis)
        {
            emit SIGNAL_jogadaSecundariaAnalise(parallelMove);
        }
    }
    else
    {
        emit SIGNAL_jogadaAtualAnalise(Jogada());
        emit SIGNAL_estadoDejogadaAtualAnalise(JOGADA_NENHUMA);
    }

    if(statisticsBlue)
    {
        vGenerateStatisticsFrame();
    }

    double tempoTotal = elapsedTempoDeExecucao.nsecsElapsed()*1e-6;

    if(tempoTotal < TIME_LOOP_ANALYSIS)
        return;

    if(tempoTotal < 1.2*TIME_LOOP_ANALYSIS)
        qWarning() << "Tempo Análise " << QString::number(tempoTotal, 'f', 2) << " ms";
    else
        qCritical() << "Tempo Análise " << QString::number(tempoTotal, 'f', 2) << " ms";

}

void Analysis::SLOT_habilitarDesabilitarJogadaParalela(bool habilitar)
{
    bParallelMoveExistis = habilitar;
}

