/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ANALYSISCOMMON_H
#define ANALYSISCOMMON_H


#include "Constants/constants.h"
#include "Common/common.h"
#include "Environment/environment.h"

#include <QElapsedTimer>
#include <QVector>
#include <QVector2D>
#include <QVector3D>

#define CHUTE_AO_GOL -1
#define NENHUM -2

enum ESTADOS_DE_JOGO : quint8 {
    BOLA_EM_DISPUTA = 0,

    JOGADA_EM_ANDAMENTO = 1,

    PASSE_EM_ANDAMENTO = 3,

    CHUTE_AO_GOL_EM_ANDAMENT0 = 5,

    PASSE_OU_CHUTE_EM_ANDAMENTO = 7,

    JOGADA_NENHUMA = 9,

};
Q_DECLARE_METATYPE(ESTADOS_DE_JOGO);

const quint8 NOTA_NULA{0};

const quint8 NOTA_RUIM{50};

const quint8 NOTA_BAIXA{100};

const quint8 NOTA_MEDIA {150};

const quint8 NOTA_ALTA {200};

const quint8 NOTA_MAXIMA{250};

enum TIPO_DE_JOGADA : quint8 {
    PASSE_OU_CHUTE = 0,
    PASSE = 1,
    CHUTE = 2
};



struct NotaPasse {

    quint8 iAnguloLivreCaminho {0};

    quint8 iDistanciaDoPasse {0};

    quint8 iLiberdadeDeMarcacao {0};

    quint8 iAnguloRedirect {0};

    quint8 iAnguloLivreParaChuteReceptor {0};

    quint8 iDistanciaGolReceptor {0};

    quint8 iLiberdadeDoPassador{0};

    quint8 iDeltaXisDoPasse {0};

    NotaPasse(const QVector2D posicaoPassador,
              const QVector2D pontoMiraPassador,
              const QVector2D posicaoReceptor,
              const QVector2D posicaoBola,
              const QVector<QVector3D>& vt_vt3dPosicaoDeTodosadversarios,
              const QVector2D vt2dTamanhoCampo,
              const qint8 iLadoCampo,
              const qint16 iLarguraGol){


        iAnguloLivreCaminho =
                Common::iEscalaVariavel(Common::dAnguloEmGrausLivreParaPasse(posicaoBola,
                                                                             posicaoReceptor,
                                                                             vt_vt3dPosicaoDeTodosadversarios),
                                        0,
                                        45);

        iDistanciaDoPasse =
                Common::iEscalaVariavel(posicaoBola.distanceToPoint(posicaoReceptor),
                                        0,
                                        vt2dTamanhoCampo.x()/2);

        iLiberdadeDeMarcacao =
                Common::iEscalaVariavel(Common::dLiberdadeDeMarcacao(posicaoReceptor,
                                                                     vt_vt3dPosicaoDeTodosadversarios),
                                        -vt2dTamanhoCampo.x()/2,
                                        0);

        iAnguloRedirect =
                Common::iEscalaVariavel(Common::dAnguloDeRotacao(posicaoReceptor,
                                                                 posicaoBola,
                                                                 QVector2D(-iLadoCampo*vt2dTamanhoCampo.x()/2, 0)),
                                        0,
                                        180);

        iAnguloLivreParaChuteReceptor =
                Common::iEscalaVariavel(Common::dAnguloEmGrausLivreParaChute(posicaoReceptor,
                                                                             iLadoCampo,
                                                                             iLarguraGol,
                                                                             vt2dTamanhoCampo,
                                                                             vt_vt3dPosicaoDeTodosadversarios),
                                        0,
                                        45);

        iDistanciaGolReceptor =
                Common::iEscalaVariavel(posicaoReceptor.distanceToPoint(QVector2D(-iLadoCampo*vt2dTamanhoCampo.x()/2, 0)),
                                        0,
                                        vt2dTamanhoCampo.x()/2);

        iLiberdadeDoPassador =
                Common::iEscalaVariavel(Common::dLiberdadeDeMarcacao(posicaoBola,
                                                                     vt_vt3dPosicaoDeTodosadversarios),
                                        -vt2dTamanhoCampo.x()/2,
                                        0);

        iDeltaXisDoPasse =
                Common::iEscalaVariavel( Common::dDeltaXis(posicaoReceptor,
                                                           posicaoBola,
                                                           iLadoCampo),
                                         -vt2dTamanhoCampo.x()/2,
                                         vt2dTamanhoCampo.x()/2);

    };

    NotaPasse(){

    };

};

struct NotaGol {

    quint8 iAnguloLivreCaminho {0};

    quint8 iDistanciaBolaGolAdversario {0};

    quint8 iLiberdadeDoChutador{0};

    NotaGol(const QVector2D posicaoRobo,
            const QVector2D pontoMiraRobo,
            const QVector2D posicaoBola,
            const QVector<QVector3D>& vt_vt3dPosicaoDeTodosadversarios,
            const QVector2D vt2dTamanhoCampo,
            const qint8 iLadoCampo,
            const qint16 iLarguraGol){

        iAnguloLivreCaminho =
                Common::iEscalaVariavel(Common::dAnguloEmGrausLivreParaChute(posicaoBola,
                                                                             iLadoCampo,
                                                                             iLarguraGol,
                                                                             vt2dTamanhoCampo,
                                                                             vt_vt3dPosicaoDeTodosadversarios),
                                        0,
                                        45);

        iDistanciaBolaGolAdversario =
                Common::iEscalaVariavel(posicaoBola.distanceToPoint(QVector2D((-iLadoCampo)*vt2dTamanhoCampo.x()/2, 0)),
                                        0,
                                        vt2dTamanhoCampo.x()/2);

        iLiberdadeDoChutador =
                Common::iEscalaVariavel(Common::dLiberdadeDeMarcacao(posicaoBola,
                                                                     vt_vt3dPosicaoDeTodosadversarios),
                                        -vt2dTamanhoCampo.x()/2,
                                        0);



    };

    NotaGol(){

    };

};


struct JogadaGol {
    NotaGol notaGolInicioJogada;   /// quando o robô aproxima-se da bola

    bool bBolaDentroDaArea;
    quint16 iBolaEmDirecaoAoGolContador;
    quint16 iBolaNaoEmDirecaoAoGol;

    JogadaGol():
        notaGolInicioJogada( NotaGol() ),
        bBolaDentroDaArea( false ),
        iBolaEmDirecaoAoGolContador( 0 ),
        iBolaNaoEmDirecaoAoGol( 0 )
    {}

};

struct JogadaPasse {
    NotaPasse notaPasseInicioJogada[PLAYERS_PER_SIDE];  /// logo que o robô chuta a bola

    qint8 iIdReceptor{-2};

    JogadaPasse(){

        for (NotaPasse& n : notaPasseInicioJogada)
            n = NotaPasse();

    };
};



struct Jogada {
    QElapsedTimer timerTempoDaJogada;
    Team timeDaJogada{teamNone};
    qint8 iIdRoboComABola{-1};

    JogadaGol jogadaGol;
    JogadaPasse jogadaPasse;
    TIPO_DE_JOGADA tdjTipoDeJogada{PASSE_OU_CHUTE};

    quint16 aiPontuacoesPasse[PLAYERS_PER_SIDE]{};
    quint16 iPontuacaoChute{0};
    quint16 iPontuacaoMaxima{0};

    quint16 contadorJogada{0};
    quint16 contadorFramesPasseOuChuteEmAndamento{0};

    QVector<qint8> vtiReceptores{};

    quint8 notaAvaliazaoFinalDaJogada{NOTA_NULA};

    QVector2D vt2dPosicaoBolaInicioChute{QVector2D(0,0)};

    Jogada(){

        jogadaGol = JogadaGol();

        jogadaPasse = JogadaPasse();
    };


    void vReinicia(){

        vtiReceptores.clear();

        jogadaPasse = JogadaPasse();

        jogadaGol = JogadaGol();

        timeDaJogada = teamNone;

        iIdRoboComABola = -1;

        tdjTipoDeJogada = PASSE_OU_CHUTE;

        iPontuacaoChute = 0;
        iPontuacaoMaxima = 0;

        contadorJogada = 0;

        contadorFramesPasseOuChuteEmAndamento = 0;

        notaAvaliazaoFinalDaJogada = NOTA_NULA;

        vt2dPosicaoBolaInicioChute = QVector2D(0 , 0);

    };

    void vAtribuiParametrosDaJogadaInicio(const Environment *acAmbiente)
    {
        if(timeDaJogada == teamYelllow)
        {
            for (qint8 id = 0; id<PLAYERS_PER_SIDE; id++)
            {
                if(id != iIdRoboComABola)
                {
                    jogadaPasse.notaPasseInicioJogada[id] =
                            NotaPasse(
                                acAmbiente->rbtYellow[iIdRoboComABola].vt2dGetCurrentPosition(),
                                acAmbiente->rbtYellow[iIdRoboComABola].vt2dPontoMiraRobo(),
                                acAmbiente->rbtYellow[id].vt2dGetCurrentPosition(),
                                acAmbiente->vt2dBallCurrentPosition(),
                                acAmbiente->vt3dGetAllPositions(otAzul, true),
                                acAmbiente->getFieldSize(),
                                acAmbiente->iYellowSide,
                                acAmbiente->iGetGoalWidth()
                                );
                }
                else
                {
                    jogadaPasse.notaPasseInicioJogada[id] =
                            NotaPasse();
                }
            }

            jogadaGol.notaGolInicioJogada =
                    NotaGol(
                        acAmbiente->rbtYellow[iIdRoboComABola].vt2dGetCurrentPosition(),
                        acAmbiente->rbtYellow[iIdRoboComABola].vt2dPontoMiraRobo(),
                        acAmbiente->vt2dBallCurrentPosition(),
                        acAmbiente->vt3dGetAllPositions(otAzul, true),
                        acAmbiente->getFieldSize(),
                        acAmbiente->iYellowSide,
                        acAmbiente->iGetGoalWidth()
                        );

            return;
        }

        if(timeDaJogada == teamBlue)
        {
            for (qint8 id = 0; id<PLAYERS_PER_SIDE; id++)
            {
                if(id != iIdRoboComABola)
                {
                    jogadaPasse.notaPasseInicioJogada[id] =
                            NotaPasse(
                                acAmbiente->rbtBlue[iIdRoboComABola].vt2dGetCurrentPosition(),
                                acAmbiente->rbtBlue[iIdRoboComABola].vt2dPontoMiraRobo(),
                                acAmbiente->rbtBlue[id].vt2dGetCurrentPosition(),
                                acAmbiente->vt2dBallCurrentPosition(),
                                acAmbiente->vt3dGetAllPositions(otAmarelo, true),
                                acAmbiente->getFieldSize(),
                                acAmbiente->iYellowSide,
                                acAmbiente->iGetGoalWidth()
                                );
                }
                else
                {
                    jogadaPasse.notaPasseInicioJogada[id] =
                            NotaPasse();
                }
            }

            jogadaGol.notaGolInicioJogada =
                    NotaGol(
                        acAmbiente->rbtBlue[iIdRoboComABola].vt2dGetCurrentPosition(),
                        acAmbiente->rbtBlue[iIdRoboComABola].vt2dPontoMiraRobo(),
                        acAmbiente->vt2dBallCurrentPosition(),
                        acAmbiente->vt3dGetAllPositions(otAmarelo, true),
                        acAmbiente->getFieldSize(),
                        acAmbiente->iYellowSide,
                        acAmbiente->iGetGoalWidth()
                        );

            return;
        }
    };

};
Q_DECLARE_METATYPE(Jogada);

#endif /*ANALYSISCOMMON_H*/
