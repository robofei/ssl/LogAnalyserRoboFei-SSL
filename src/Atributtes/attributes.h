/*
 * LogAnalyserRoboFEI-SSL
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ATTRIBUTES_H
#define ATTRIBUTES_H

#include "Constants/constants.h"
#include <QElapsedTimer>
#include <QVector>
#include <QVector2D>
#include <QVector3D>

/**
 * @brief
 *
 */
enum TipoObjeto : quint8
{
    otAmarelo,      //0
    otAzul,        //1
    otBola,          //2
    otLinhaPenalty,   //3
    otVazio
};



//estrutura com a posição da bola
/**
 * @brief
 *
 */
class Ball
{
public:

    /**
     * @brief
     *
     */
    Ball()
    {
        vt2dCurrentPosition      = QVector2D(0,0);
        vt2dLastPosition   = QVector2D(0,0);

        dVelocity = 0;
        dBallTime  = 0;

        vt2dVelocity = QVector2D(0,0);

        iCameraBola = 0;
        for(int i=0; i < NUMBER_OF_CAMERAS; ++i)
        {
            iCameraFrame[i] = 0;
        }

        bEmCampo = false;
    }

    QVector2D vt2dCurrentPosition, vt2dLastPosition; /**< TODO: describe */

    double dVelocity; /**< TODO: describe */
    QVector2D vt2dVelocity; /**< TODO: describe */
    double dBallTime; /**< TODO: describe */

    uint iCameraBola;//Indica o ID da última câmera em que a bola foi vista /**< TODO: describe */
    uint iCameraFrame[NUMBER_OF_CAMERAS];//Guarda o número do frame em que a bola foi visto /**< TODO: describe */

    bool bEmCampo;//Mostra se a bola está sendo detectado pela visão, definido como padrão em false /**< TODO: describe */
};


//estrutura com o valores relativos aos robôs (vetor posição, ângulo e velocidade)
/**
 * @brief
 *
 */
typedef struct Attributes
{
    quint8 id; /**< ID do robô */

    QVector2D vt2dCurrentPosition;/**< Posicao atual do robo  */
    QVector2D vt2dPreviousPosition;/**< Posicao anterior do robo */
    float rotation; /**< Angulo do robo em radianos */

    QVector3D vt3dVeloxity;
    double dVelocity; /**< TODO: describe */
    double dTempoRobo; /**< TODO: describe */

    bool bOnField;/**< Mostra se o robô está sendo detectado pela visão, definido como padrão em false */

    uint iCameraRobo; /**< Indica o ID da última câmera em que o robô foi visto  */
    uint iCameraFrame[NUMBER_OF_CAMERAS]; /**< Guarda o número do frame em que o robô foi visto */

    /**
     * @brief
     *
     */
    Attributes()
    {
        id = -1;
        rotation = 0;

        dVelocity=0;
        dTempoRobo=0;

        bOnField=false;

        iCameraRobo=0;
    }

/**
 * @brief
 *
 */
} Attributes;


#endif // ATTRIBUTES_H
