/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef MIRRORLOGFILES_H
#define MIRRORLOGFILES_H

#include <QDialog>
#include <QCheckBox>
#include <QList>
#include <QString>
#include <QtAlgorithms>
#include <QVBoxLayout>
#include <QScrollArea>

struct Properties{

    QString sMatchMirror{};
    QString sMatchInterface{};

    Properties(const QString mirrorPAth,
               const QString mirror,
               const QString matchInterface);
};

enum MirrorOrigin : quint8{
    TIGERS,
    JACKETS
};

enum Cup : quint8{
    L2019A,
    L2019B,
    L2018A,
    L2018B,
    L2017,
    L2016
};

namespace Ui {
class MirrorLogFiles;
}

class MirrorLogFiles : public QDialog
{
    Q_OBJECT

public:
    MirrorLogFiles(QWidget *parent = nullptr);
    ~MirrorLogFiles();

    void vSetMirror(MirrorOrigin origin);

protected:
    void closeEvent(QCloseEvent *event = nullptr);

    void reject();

private:

    QScrollArea* scrollAreaItens;
    QWidget* widgetScrollArea;
    QVBoxLayout* layoutScrollArea;

    Ui::MirrorLogFiles *ui;

    QString sPathMirror{};

    Cup whichCup{};

    const QString sTigersMirror{"https://tigers-mannheim.de/download/gamelogs/"};
    const QString sJacketsMirror{"https://mirror.robojackets.org/robocup-small-size-league/gamelogs/"};

    const QString s2019A{"2019/div-a/"};
    const QString s2019B{"2019/div-b/"};

    const QString s2018A{"2018/div-a/"};
    const QString s2018B{"2018/div-b/"};

    const QString s2017{"2017/"};

    const QString s2016{"2016/"};

    const QList<Properties> list2019A{
        Properties(s2019A, "2019-07-03_14-09_ER-Force-vs-TIGERs_Mannheim.log.gz", "ER-Force vs TIGERs-Mannheim (2x0)"),
                Properties(s2019A, "2019-07-04_03-27_OP-AmP-vs-MRL.log.gz", "OP-AmP vs MRL (0x5)"),
                Properties(s2019A, "2019-07-04_05-37_ER-Force-vs-RoboTeam_Twente.log.gz", "ER-Force vs RoboTeam Twente (0x1)"),
                Properties(s2019A, "2019-07-04_06-55_KIKS-vs-TIGERs_Mannheim.log.gz", "KIKS vs TIGERs-Mannheim (1x7)"),
                Properties(s2019A, "2019-07-04_08-31_RoboDragons-vs-ZJUNlict.log.gz", "RoboDragons vs ZJUNlict (0x4)"),
                Properties(s2019A, "2019-07-04_10-12_TIGERs_Mannheim-vs-OP-AmP.log.gz", "TIGERs-Mannheim vs OP Amp (6x0)"),
                Properties(s2019A, "2019-07-04_12-15_ZJUNlict-vs-RoboTeam_Twente.log.gz", "ZJUNlict vs RoboTeam Twente (6x0)"),
                Properties(s2019A, "2019-07-04_14-10_KIKS-vs-MRL.log.gz", "KIKS vs MRL (0x6)"),
                Properties(s2019A, "2019-07-05_01-51_ER-Force-vs-RoboDragons.log.gz", "ER-Force vs RoboDragons (1x0)"),
                Properties(s2019A, "2019-07-05_04-18_KIKS-vs-OP-AmP.log.gz", "KIKS vs OP-AmP (1x3)"),
                Properties(s2019A, "2019-07-05_06-15_RoboTeam_Twente-vs-RoboDragons.log.gz", "RoboTeam Twente vs RoboDragons (0x3)"),
                Properties(s2019A, "2019-07-05_08-05_TIGERs_Mannheim-vs-MRL.log.gz", "TIGERs-Mannheim vs MRL (3x1)"),
                Properties(s2019A, "2019-07-05_09-11_ER-Force-vs-ZJUNlict.log.gz", "ER-Force vs ZJUNlict (0x0)"),
                Properties(s2019A, "2019-07-05_10-38_RoboTeam_Twente-vs-TIGERs_Mannheim.log.gz", "RoboTeam-Twente vs TIGERs-Mannheim (0x6)"),
                Properties(s2019A, "2019-07-05_11-51_ER-Force-vs-OP-AmP.log.gz", "ER-Force vs OP-AmP (8x0)"),
                Properties(s2019A, "2019-07-05_13-27_MRL-vs-RoboDragons.log.gz", "MRL vs RoboDragons (1x0)"),
                Properties(s2019A, "2019-07-06_03-11_KIKS-vs-ZJUNlict.log.gz", "KIKS vs ZJUNlict (1x4)"),
                Properties(s2019A, "2019-07-06_04-54_ER-Force-vs-TIGERs_Mannheim.log.gz", "ER-Force vs TIGERs-Mannheim (0x2)"),
                Properties(s2019A, "2019-07-06_06-28_ZJUNlict-vs-MRL.log.gz", "ZJUNlict vs MRL (2x0)"),
                Properties(s2019A, "2019-07-06_08-04_RoboDragons-vs-KIKS.log.gz", "RoboDragons vs KIKS (1x3)"),
                Properties(s2019A, "2019-07-06_10-26_OP-AmP-vs-RoboTeam_Twente.log.gz", "OP-AmP vs RoboTeam Twente (1x2)"),
                Properties(s2019A, "2019-07-06_11-32_ER-Force-vs-ZJUNlict.log.gz", "ER-Force vs ZJUNlict (0x2)"),
                Properties(s2019A, "2019-07-06_12-25_RoboDragons-vs-TIGERs_Mannheim.log.gz", "RoboDragons vs TIGERs-Mannheim (0x4)"),
                Properties(s2019A, "2019-07-07_01-49_RoboTeam_Twente-vs-MRL.log.gz", "RoboTeam-Twente vs MRL (1x6)"),
                Properties(s2019A, "2019-07-07_03-26_TIGERs_Mannheim-vs-MRL.log.gz", "TIGERs-Mannheim vs MRL (0x2)"),
                Properties(s2019A, "2019-07-07_05-26_ER-Force-vs-MRL.log.gz", "ER-Force vs MRL (1x0)"),
                Properties(s2019A, "2019-07-07_06-34_ER-Force-vs-ZJUNlict.log.gz", "ER-Force vs ZJUNlict (0x1)"),


    };

    const QList<Properties> list2019B{
        Properties(s2019B, "2019-07-04_06-20_RoboFEI-vs-RoboIME.log.gz", "RoboFEI vs RoboIME (1x0)"),
                Properties(s2019B, "2019-07-04_07-52_Rob%C3%B4CIn-vs-nAMeC.log.gz", "RobôCin vs nAMeC (4x0)"),
                Properties(s2019B, "2019-07-04_09-51_RoboJackets-vs-NEUIslanders.log.gz", "RoboJackets vs NEUIslanders (0x3)"),
                Properties(s2019B, "2019-07-05_21-18_RoboJackets-vs-nAMeC.log.gz", "RoboJackets vs nAMeC (0x1)"),
                Properties(s2019B, "2019-07-05_22-40_Rob%C3%B4CIn-vs-NEUIslanders.log.gz", "RobôCin vs NEUIslanders (1x0)"),
                Properties(s2019B, "2019-07-06_00-16_RoboIME-vs-MCT_Susano_Logics.log.gz", "RoboIME vs MCT Susano Logics (1x0)"),
                Properties(s2019B, "2019-07-06_01-49_RoboFEI-vs-UBC_Thunderbots.log.gz", "RoboFEI vs UBC Thunderbots (0x5)"),
                Properties(s2019B, "2019-07-06_03-19_ULtron-vs-AMC.log.gz", "ULtron vs nAMeC (2x0)"),
                Properties(s2019B, "2019-07-06_04-32_ULtron-vs-RoboIME.log.gz", "ULtron vs RoboIME (0x0)"),
                Properties(s2019B, "2019-07-06_09-16_RoboIME-vs-MCT_Susano_Logics.log.gz", "RoboIME vs MCT Susano Logics (?x?)"),
                Properties(s2019B, "2019-07-07_04-13_RoboIME-vs-Rob%C3%B4CIn.log.gz", "RoboIME vs RobôCIn (2x1)"),
                Properties(s2019B, "2019-07-07_05-40_UBC_Thunderbots-vs-ULtron.log.gz", "UBC Thunderbots vs ULtron (3x1)"),
                Properties(s2019B, "2019-07-07_06-58_RoboIME-vs-UBC_Thunderbots.log.gz", "RoboIME vs UBC-Thunderbots (0x1)"),
    };

    const QList<Properties> list2018A{
        Properties(s2018A, "2018-06-18_15-06_ZJUNlict-vs-UMass_Minutebots.log.gz", "ZJUNlict vs UMass Minutebots (0x0)"),
                Properties(s2018A, "2018-06-18_17-09_TIGERs_Mannheim-vs-RoboTeam_Twente.log.gz", "TIGERs-Mannheim vs RoboTeam Twente (10x0)"),
                Properties(s2018A, "2018-06-18_18-41_KIKS-vs-Immortals.log.gz", "KIKS vs Immortals (1x4)"),
                Properties(s2018A, "2018-06-18_20-04_ER-Force-vs-UMass_Minutebots.log.gz", "ER-Force vs UMass Minutebots (9x0)"),
                Properties(s2018A, "2018-06-18_21-55_CM%CE%BCs-vs-RoboTeam_Twente.log.gz", "CMµs vs RoboTeam Twente (10x0)"),
                Properties(s2018A, "2018-06-18_23-22_ZJUNlict-vs-KIKS.log.gz", "ZJUNlict vs KIKS (7x0)"),
                Properties(s2018A, "2018-06-19_01-15_TIGERs_Mannheim-vs-RoboDragons.log.gz", "TIGERs-Mannheim vs RoboDragons (10x0)"),
                Properties(s2018A, "2018-06-19_03-13_Immortals-vs-ER-Force.log.gz", "Immortals vs ER-Force (0x1)"),
                Properties(s2018A, "2018-06-19_15-50_KIKS-vs-ER-Force.log.gz", "KIKS vs ER-Force (0x3)"),
                Properties(s2018A, "2018-06-19_17-36_RoboDragons-vs-CM%CE%BCs.log.gz", "RoboDragons vs CMµs (1x4)"),
                Properties(s2018A, "2018-06-19_19-23_UMass_Minutebots-vs-Immortals.log.gz", "UMass-Minutebots vs Immortals (0x1)"),
                Properties(s2018A, "2018-06-19_21-34_ER-Force-vs-ZJUNlict.log.gz", "ER-Force vs ZJUNlict (1x2)"),
                Properties(s2018A, "2018-06-19_22-35_RoboDragons-vs-RoboTeam_Twente.log.gz", "RoboDragons vs RoboTeam-Twente (1x3)"),
                Properties(s2018A, "2018-06-20_01-24_CM%CE%BCs-vs-TIGERs_Mannheim.log.gz", "CMµs vs TIGERs-Mannheim (1x1)"),
                Properties(s2018A, "2018-06-20_02-30_ZJUNlict-vs-Immortals.log.gz", "ZJUNlict vs Immortals (4x2)"),
                Properties(s2018A, "2018-06-20_15-13_KIKS-vs-RoboDragons.log.gz", "KIKS vs RoboDragons (1x5)"),
                Properties(s2018A, "2018-06-20_17-18_TIGERs_Mannheim-vs-ER-Force.log.gz", "TIGERs-Mannheim vs ER Force (1x0)"),
                Properties(s2018A, "2018-06-20_18-37_CM%CE%BCs-vs-ZJUNlict.log.gz", "CMµs vs ZJUNlict (2x1)"),
                Properties(s2018A, "2018-06-20_20-11_UMass_Minutebots-vs-RoboTeam_Twente.log.gz", "UMass Minutebots vs RoboTeam Twente (2x0)"),
                Properties(s2018A, "2018-06-20_21-57_RoboDragons-vs-Immortals.log.gz", "RoboDragons vs Immortals (2x5)"),
                Properties(s2018A, "2018-06-21_00-08_UMass_Minutebots-vs-ER-Force.log.gz", "UMass Minutebots vs ER-Force (0x5)"),
                Properties(s2018A, "2018-06-21_01-27_Immortals-vs-ZJUNlict.log.gz", "Immortals vs ZJUNlict (3x5)"),
                Properties(s2018A, "2018-06-21_01-39_Immortals-vs-ZJUNlict.log.gz", "Immortals vs ZJUNlict (3x5)"),
                Properties(s2018A, "2018-06-21_03-21_TIGERs_Mannheim-vs-CM%CE%BCs.log.gz", "TIGERs-Mannheim vs CMµs (0x1)"),
                Properties(s2018A, "2018-06-21_15-12_ER-Force-vs-ZJUNlict.log.gz", "ER-Force vs ZJUNlict (0x2)"),
                Properties(s2018A, "2018-06-21_17-36_TIGERs_Mannheim-vs-ZJUNlict.log.gz", "TIGERs-Mannheim vs ZJUNlict (3x5)"),
                Properties(s2018A, "2018-06-21_20-09_ZJUNlict-vs-CM%CE%BCs.log.gz", "ZJUNlict vs CMµs (4x0)"),
    };

    const QList<Properties> list2018B{
        Properties(s2018B, "2018-06-18_17-48_RoboIME-vs-ULtron.log.gz", "RoboIME vs ULtron (1x0)"),
                Properties(s2018B, "2018-06-18_19-05_AMC-vs-RoboFEI.log.gz", "AMC vs RoboFEI (0x2)"),
                Properties(s2018B, "2018-06-18_22-42_RoboIME-vs-NEUIslanders.log.gz", "RoboIME vs NEUIslanders (0x0)"),
                Properties(s2018B, "2018-06-19_00-13_RoboJackets-vs-ULtron.log.gz", "RoboJackets vs ULtron (0x0)"),
                Properties(s2018B, "2018-06-19_01-31_UBC_Thunderbots-vs-RoboFEI.log.gz", "UBC-Thunderbots vs RoboFEI (3x0)"),
                Properties(s2018B, "2018-06-19_18-07_UBC_Thunderbots-vs-NEUIslanders.log.gz", "UBC-Thunderbots vs NEUIslanders (0x2)"),
                Properties(s2018B, "2018-06-20_00-03_RoboIME-vs-RoboJackets.log.gz", "RoboIME vs RoboJackets (0x0)"),
                Properties(s2018B, "2018-06-20_01-33_ULtron-vs-NEUIslanders.log.gz", "ULtron vs NEUIslanders (0x2)"),
                Properties(s2018B, "2018-06-20_04-05_UBC_Thunderbots-vs-RoboJackets.log.gz", "UBC-Thunderbots vs RoboJackets (4x0)"),
                Properties(s2018B, "2018-06-20_05-23_UBC_Thunderbots-vs-RoboJackets.log.gz", "UBC-Thunderbots vs RoboJackets (4x0)"),
                Properties(s2018B, "2018-06-20_19-05_ACES-vs-RoboJackets.log.gz", "ACES vs RoboJackets (?x?)"),
                Properties(s2018B, "2018-06-21_00-00_RoboIME-vs-UBC_Thunderbots.log.gz", "RoboIME vs UBC-Thunderbots (1x0)"),
                Properties(s2019A, "2018-06-21_06-04_AIS-vs-AMC.log.gz", "AIS vs AMC (0x0)"),
                Properties(s2018B, "2018-06-21_17-05_RoboFEI-vs-ULtron.log.gz", "RoboFEI vs ULtron (0x2)"),
                Properties(s2018B, "2018-06-21_18-29_RoboIME-vs-AIS.log.gz", "RoboIME vs AIS (5x0)"),
                Properties(s2018B, "2018-06-21_19-39_UBC_Thunderbots-vs-ULtron.log.gz", "UBC-Thunderbots vs ULtron (?x?)")
    };

    // Need to fill the result of the matches
    const QList<Properties> list2017{
        Properties(s2017, "2017-07-27_03-25_NEUIslanders-vs-Parsian.log.gz", "NEUIslanders vs Parsian (?x?)"),
                Properties(s2017, "2017-07-27_03-46_Tigers_Mannheim-vs-Op-AmP.log.gz", "Tigers-Mannheim vs Op-AmP (?x?)"),
                Properties(s2017, "2017-07-27_04-03_ER-Force-vs-UMass_MinuteBots.log.gz", "ER-Force vs UMass MinuteBots (?x?)"),
                Properties(s2017, "2017-07-27_04-34_RoboJackets-vs-STOx%E2%80%99s.log.gz", "RoboJackets vs STOx’s (?x?)"),
                Properties(s2017, "2017-07-27_05-20_MCT_Susano_Logics-vs-ZJUNlict.log.gz", "MCT Susano Logics vs ZJUNlict (?x?)"),
                Properties(s2017, "2017-07-27_06-04_KIKS-vs-RoboFEI.log.gz", "KIKS vs RoboFEI (?x?)"),
                Properties(s2017, "2017-07-27_06-11_RoboIME-vs-MRL.log.gz", "RoboIME vs MRL (?x?)"),
                Properties(s2017, "2017-07-27_06-41_RoboTeam_Twente-vs-RoboDragons.log.gz", "RoboTeam Twente vs RoboDragons (?x?)"),
                Properties(s2017, "2017-07-27_07-11_NEUIslanders-vs-ULtron.log.gz", "NEUIslanders vs ULtron (?x?)"),
                Properties(s2017, "2017-07-27_07-48_KgpKubs-vs-Op-AmP.log.gz", "KgpKubs vs Op-AmP (?x?)"),
                Properties(s2017, "2017-07-27_08-39_STOx%E2%80%99s-vs-UBC_Thunderbots.log.gz", "STOx’s vs UBC Thunderbots (?x?)"),
                Properties(s2017, "2017-07-27_09-34_MCT_Susano_Logics-vs-Parsian.log.gz", "MCT_Susano_Logics vs Parsian (?x?)"),
                Properties(s2017, "2017-07-27_09-48_Tigers_Mannheim-vs-RoboIME.log.gz", "Tigers_Mannheim vs RoboIME (?x?)"),
                Properties(s2017, "2017-07-27_10-13_UMass_MinuteBots-vs-RoboFEI.log.gz", "UMass_MinuteBots vs RoboFEI (?x?)"),
                Properties(s2017, "2017-07-27_10-36_RoboJackets-vs-RoboTeam_Twente.log.gz", "RoboJackets vs RoboTeam_Twente (?x?)"),
                Properties(s2017, "2017-07-27_11-07_ZJUNlict-vs-ULtron.log.gz", "ZJUNlict vs ULtron (?x?)"),
                Properties(s2017, "2017-07-27_11-49_KgpKubs-vs-MRL.log.gz", "KgpKubs vs MRL (?x?)"),
                Properties(s2017, "2017-07-27_12-26_KIKS-vs-SRC.log.gz", "KIKS vs SRC (?x?)"),
                Properties(s2017, "2017-07-27_12-50_UBC_Thunderbots-vs-RoboDragons.log.gz", "UBC_Thunderbots vs RoboDragons (?x?)"),
                Properties(s2017, "2017-07-28_03-09_UBC_Thunderbots-vs-RoboTeam_Twente.log.gz", "UBC_Thunderbots vs RoboTeam_Twente (?x?)"),
                Properties(s2017, "2017-07-28_03-32_SRC-vs-UMass_MinuteBots.log.gz", "SRC vs UMass_MinuteBots (?x?)"),
                Properties(s2017, "2017-07-28_04-17_KgpKubs-vs-RoboIME.log.gz", "KgpKubs vs RoboIME (?x?)"),
                Properties(s2017, "2017-07-28_04-38_RoboJackets-vs-RoboDragons.log.gz", "RoboJackets vs RoboDragons (?x?)"),
                Properties(s2017, "2017-07-28_04-40_MCT_Susano_Logics-vs-ULtron.log.gz", "MCT_Susano_Logics vs ULtron (?x?)"),
                Properties(s2017, "2017-07-28_05-36_ER-Force-vs-RoboFEI.log.gz", "ER-Force vs RoboFEI (?x?)"),
                Properties(s2017, "2017-07-28_06-20_Op-AmP-vs-MRL.log.gz", "Op-AmP vs MRL (?x?)"),
                Properties(s2017, "2017-07-28_06-39_NEUIslanders-vs-ZJUNlict.log.gz", "NEUIslanders vs ZJUNlict (?x?)"),
                Properties(s2017, "2017-07-28_07-06_RoboTeam_Twente-vs-STOx%E2%80%99s.log.gz", "RoboTeam_Twente vs STOx’s (?x?)"),
                Properties(s2017, "2017-07-28_07-33_KIKS-vs-UMass_MinuteBots.log.gz", "KIKS vs UMass_MinuteBots (?x?)"),
                Properties(s2017, "2017-07-28_08-43_Parsian-vs-ULtron.log.gz", "Parsian vs ULtron (?x?)"),
                Properties(s2017, "2017-07-28_09-08_RoboJackets-vs-UBC_Thunderbots.log.gz", "RoboJackets vs UBC_Thunderbots (?x?)"),
                Properties(s2017, "2017-07-28_09-32_SRC-vs-RoboFEI.log.gz", "SRC vs RoboFEI (?x?)"),
                Properties(s2017, "2017-07-28_10-10_Op-AmP-vs-RoboIME.log.gz", "Op-AmP vs RoboIME (?x?)"),
                Properties(s2017, "2017-07-28_10-46_NEUIslanders-vs-MCT_Susano_Logics.log.gz", "NEUIslanders vs MCT_Susano_Logics (?x?)"),
                Properties(s2017, "2017-07-28_11-02_RoboDragons-vs-STOx%E2%80%99s.log.gz", "RoboDragons vs STOx’s (?x?)"),
                Properties(s2017, "2017-07-28_11-31_KIKS-vs-ER-Force.log.gz", "KIKS vs ER-Force (?x?)"),
                Properties(s2017, "2017-07-28_12-08_Tigers_Mannheim-vs-MRL.log.gz", "Tigers_Mannheim vs MRL (?x?)"),
                Properties(s2017, "2017-07-28_12-22_Parsian-vs-ZJUNlict.log.gz", "Parsian vs ZJUNlict (?x?)"),
                Properties(s2017, "2017-07-29_03-32_Tigers_Mannheim-vs-KIKS.log.gz", "Tigers_Mannheim vs KIKS (?x?)"),
                Properties(s2017, "2017-07-29_04-17_Parsian-vs-UBC_Thunderbots.log.gz", "Parsian vs UBC_Thunderbots (?x?)"),
                Properties(s2017, "2017-07-29_04-39_NEUIslanders-vs-STOx%E2%80%99s.log.gz", "NEUIslanders vs STOx’s (?x?)"),
                Properties(s2017, "2017-07-29_05-02_ER-Force-vs-OP-AmP.log.gz", "ER-Force vs OP-AmP (?x?)"),
                Properties(s2017, "2017-07-29_05-45_ULtron-vs-RoboIME.log.gz", "ULtron vs RoboIME (?x?)"),
                Properties(s2017, "2017-07-29_07-02_RoboJackets-vs-UMass_MinuteBots.log.gz", "RoboJackets vs UMass_MinuteBots (?x?)"),
                Properties(s2017, "2017-07-29_07-15_RoboTeam_Twente-vs-KgpKubs.log.gz", "RoboTeam_Twente vs KgpKubs (?x?)"),
                Properties(s2017, "2017-07-29_08-07_Parsian-vs-MRL.log.gz", "Parsian vs MRL (?x?)"),
                Properties(s2017, "2017-07-29_08-11_Tigers_Mannheim-vs-ZJUNlict.log.gz", "Tigers_Mannheim vs ZJUNlict (?x?)"),
                Properties(s2017, "2017-07-29_08-43_RoboDragons-vs-ER-Force.log.gz", "RoboDragons vs ER-Force (?x?)"),
                Properties(s2017, "2017-07-29_09-05_STOx%E2%80%99s-vs-SRC.log.gz", "STOx’s vs SRC (?x?)"),
                Properties(s2017, "2017-07-29_10-02_UMass_MinuteBots-vs-RoboIME.log.gz", "UMass_MinuteBots vs RoboIME (?x?)"),
                Properties(s2017, "2017-07-29_10-03_RoboTeam_Twente-vs-RoboFEI.log.gz", "RoboTeam_Twente vs RoboFEI (?x?)"),
                Properties(s2017, "2017-07-29_10-56_RoboTeam_Twente-vs-RoboFEI.log.gz", "RoboTeam_Twente vs RoboFEI (?x?)"),
                Properties(s2017, "2017-07-29_11-08_Tigers_Mannheim-vs-OP-AmP.log.gz", "Tigers_Mannheim vs OP-AmP (?x?)"),
                Properties(s2017, "2017-07-29_11-47_RoboDragons-vs-KIKS.log.gz", "RoboDragons vs KIKS (?x?)"),
                Properties(s2017, "2017-07-29_12-07_STOx%E2%80%99s-vs-UBC_Thunderbots.log.gz", "STOx’s vs UBC_Thunderbots (?x?)"),
                Properties(s2017, "2017-07-30_04-11_SRC-vs-Parsian.log.gz", "SRC vs Parsian (?x?)"),
                Properties(s2017, "2017-07-30_04-37_Tigers_Mannheim-vs-MRL.log.gz", "Tigers_Mannheim vs MRL (?x?)"),
                Properties(s2017, "2017-07-30_05-09_STOx%27s-vs-RoboDragons.log.gz", "STOx’s vs RoboDragons (?x?)"),
                Properties(s2017, "2017-07-30_05-22_ZJUNlict-vs-ER-Force.log.gz", "ZJUNlict vs ER-Force (?x?)"),
                Properties(s2017, "2017-07-30_05-38_RoboTeam_Twente-vs-UMass_MinuteBots.log.gz", "RoboTeam_Twente vs UMass_MinuteBots (?x?)"),
                Properties(s2017, "2017-07-30_06-14_RoboDragons-vs-MRL.log.gz", "RoboDragons vs MRL (?x?)"),
                Properties(s2017, "2017-07-30_06-54_Tigers_Mannheim-vs-STOx%E2%80%99s.log.gz", "Tigers_Mannheim vs STOx’s (?x?)"),
                Properties(s2017, "2017-07-30_08-08_ZJUNlict-vs-Parsian.log.gz", "ZJUNlict vs Parsian (?x?)"),
                Properties(s2017, "2017-07-30_08-55_ER-Force-vs-SRC.log.gz", "ER-Force vs SRC (?x?)"),
    };

    // Need to fill the matches
    const QList<Properties> list2016{
        Properties(s2016, "2016-06-30_cmdragons_neuislanders.log.gz", "CMDragons vs NEUIslanders")
    };




    QList< QSharedPointer<QCheckBox> > list_cbMatches;

private slots:

    void SLOT_generateList();
    void SLOT_download();

    void SLOT_activateRadioButtonDivisions();
    void SLOT_desactivateRadioButtonDivisions();


signals:

    void SIGNAL_downloadLinks(const QList<QString>& listLinks);

};

#endif // MIRRORLOGFILES_H
