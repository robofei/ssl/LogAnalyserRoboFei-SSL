/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "mirrorlogfiles.h"
#include "ui_mirrorlogfiles.h"

Properties::Properties(const QString mirrorPAth,
                       const QString mirror,
                       const QString matchInterface){

    sMatchMirror = mirrorPAth+mirror;
    sMatchInterface = matchInterface;

}



MirrorLogFiles::MirrorLogFiles(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MirrorLogFiles)
{
    ui->setupUi(this);

    scrollAreaItens = new QScrollArea(this);

    scrollAreaItens->setWidgetResizable(true);
    scrollAreaItens->setGeometry(239, 9,
                                 451, 881);

    widgetScrollArea = new QWidget(this);
    scrollAreaItens->setWidget(widgetScrollArea);

    layoutScrollArea = new QVBoxLayout();
    widgetScrollArea->setLayout(layoutScrollArea);

    this->setWindowTitle("Mirror Log Files");

    ui->rb_2019->setChecked(true);

    ui->rb_DivA->setChecked(true);

    connect(ui->rb_2019,
            &QRadioButton::clicked,
            this,
            &MirrorLogFiles::SLOT_activateRadioButtonDivisions);

    connect(ui->rb_2018,
            &QRadioButton::clicked,
            this,
            &MirrorLogFiles::SLOT_activateRadioButtonDivisions);

    connect(ui->rb_2017,
            &QRadioButton::clicked,
            this,
            &MirrorLogFiles::SLOT_desactivateRadioButtonDivisions);

    connect(ui->rb_2016,
            &QRadioButton::clicked,
            this,
            &MirrorLogFiles::SLOT_desactivateRadioButtonDivisions);

    connect(ui->pb_GenerateList,
            &QPushButton::clicked,
            this,
            &MirrorLogFiles::SLOT_generateList);

    connect(ui->pb_Download,
            &QPushButton::clicked,
            this,
            &MirrorLogFiles::SLOT_download);

}

MirrorLogFiles::~MirrorLogFiles()
{
    delete ui;
}

void MirrorLogFiles::vSetMirror(MirrorOrigin origin){

    if(origin == TIGERS)
        sPathMirror = sTigersMirror;
    else if(origin == JACKETS)
        sPathMirror = sJacketsMirror;

}

void MirrorLogFiles::closeEvent(QCloseEvent *event)
{
    emit SIGNAL_downloadLinks(QList<QString>());
}

void MirrorLogFiles::reject()
{
    emit SIGNAL_downloadLinks(QList<QString>());
}



void MirrorLogFiles::SLOT_generateList(){

    const uint x = ui->rb_DivA->x()+ui->rb_DivA->width()+150;

    const uint y = ui->rb_DivA->y();

    list_cbMatches.clear();

    if(ui->rb_2019->isChecked() &&
       ui->rb_DivA->isChecked()){

        for(quint8 i=0; i<list2019A.size(); i++){

            QSharedPointer<QCheckBox> cb ( new QCheckBox(this) );

            cb->setText(list2019A.at(i).sMatchInterface);
            cb->show();

            list_cbMatches.append(cb);


        }

        whichCup = L2019A;
    }
    else if(ui->rb_2019->isChecked() &&
            ui->rb_DivB->isChecked()){

        for(quint8 i=0; i<list2019B.size(); i++){

            QSharedPointer<QCheckBox> cb ( new QCheckBox(this) );

            cb->setText(list2019B.at(i).sMatchInterface);
            cb->show();

            list_cbMatches.append(cb);
        }

        whichCup = L2019B;

    }
    else if(ui->rb_2018->isChecked() &&
            ui->rb_DivA->isChecked()){

        for(quint8 i=0; i<list2018A.size(); i++){

            QSharedPointer<QCheckBox> cb ( new QCheckBox(this) );

            cb->setText(list2018A.at(i).sMatchInterface);
            cb->show();

            list_cbMatches.append(cb);


        }

        whichCup = L2018A;

    }

    else if(ui->rb_2018->isChecked() &&
            ui->rb_DivB->isChecked()){

        for(quint8 i=0; i<list2018B.size(); i++){

            QSharedPointer<QCheckBox> cb ( new QCheckBox(this) );

            cb->setText(list2018B.at(i).sMatchInterface);
            cb->show();

            list_cbMatches.append(cb);
        }

        whichCup = L2018B;

    }
    else if(ui->rb_2017->isChecked()){

        for(quint8 i=0; i<list2017.size(); i++){

            QSharedPointer<QCheckBox> cb ( new QCheckBox(this) );

            cb->setText(list2017.at(i).sMatchInterface);
            cb->show();

            list_cbMatches.append(cb);
        }

        whichCup = L2017;

    }
    else if(ui->rb_2016->isChecked()){

        for(quint8 i=0; i<list2016.size(); i++){

            QSharedPointer<QCheckBox> cb ( new QCheckBox(this) );

            cb->setText(list2016.at(i).sMatchInterface);
            cb->show();

            list_cbMatches.append(cb);


        }

        whichCup = L2016;
    }

    for(auto& n: list_cbMatches)
    {
        layoutScrollArea->addWidget(n.get());
    }
}

void MirrorLogFiles::SLOT_download(){

    QList<QString> listLinksForDownload{};

    QList<Properties> listProperties{};

    QString path{};

    if(sPathMirror == TIGERS)
        sPathMirror = sTigersMirror;
    else if(sPathMirror == JACKETS)
        sPathMirror = sJacketsMirror;

    switch (whichCup){
        case L2019A:
            listProperties = list2019A;
            break;

        case L2019B:
            listProperties = list2019B;
            break;

        case L2018A:
            listProperties = list2018A;
            break;

        case L2018B:
            listProperties = list2018B;
            break;

        case L2017:
            listProperties = list2017;
            break;

        case L2016:
            listProperties = list2016;
            break;

    }


    for (quint8 i=0; i<list_cbMatches.size(); i++){

        if(list_cbMatches.at(i)->isChecked()){

            listLinksForDownload.append(
                        sPathMirror + listProperties.at(i).sMatchMirror);
        }

    }
    emit SIGNAL_downloadLinks(listLinksForDownload);

}

void MirrorLogFiles::SLOT_activateRadioButtonDivisions(){

    ui->rb_DivA->show();
    ui->rb_DivB->show();

}

void MirrorLogFiles::SLOT_desactivateRadioButtonDivisions(){

    ui->rb_DivA->hide();
    ui->rb_DivB->hide();

}


