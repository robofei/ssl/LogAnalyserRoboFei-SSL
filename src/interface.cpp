/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "interface.h"
#include "ui_interface.h"

Interface::Interface(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::Interface)
{
    this->setWindowFlags(Qt::Window);
    this->setWindowIcon(QIcon(":/icon/icon.svg"));

    ui->setupUi(this);

    configureDarkStyle();

    qRegisterMetaType<Environment>();
    qRegisterMetaType<Jogada>();
    qRegisterMetaType<ESTADOS_DE_JOGO>();
    qRegisterMetaType<TeamStatistics>();

    environmentInterface = new Environment();

    tmrUpdateDrawing.reset(new QTimer(this));
    tmrUpdateInterfaceInformation.reset(new QTimer(this));

    QList<QNetworkInterface> ifInterfaceLocal = QNetworkInterface::allInterfaces();
    for(auto & i : ifInterfaceLocal)
    {
        ui->cb_Rede_InterfaceRede->addItem(i.humanReadableName());
    }

    connect(ui->pb_Rede_RecebePacotes, &QPushButton::clicked,
            this, &Interface::SLOT_connectNetwork);
    connect(tmrUpdateDrawing.get(),       &QTimer::timeout,
            this, &Interface::SLOT_AtualizaDesenho);           // Conecxão do timer que atualiza os desenhos

    QStringList strListItems;

    strListItems << "Show Ball Sight"
                 << "Show Kalman Predict"
                 << "Show Ball Placement Designed Position"
                 << "Highlight Analysis Receiver"
                 << "Highlight Ball Path"
                 << "Highlight Analysis Ball Owner";

    ui->lwid_Mapa_Mostrar->addItems(strListItems);

    for(int i = 0; i < ui->lwid_Mapa_Mostrar->count(); ++i){

        QListWidgetItem* item = ui->lwid_Mapa_Mostrar->item(i);
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Unchecked);
    }

    ui->lwid_Mapa_Mostrar->item(2)->setCheckState(Qt::Checked);
    ui->lwid_Mapa_Mostrar->item(3)->setCheckState(Qt::Checked);
    ui->lwid_Mapa_Mostrar->item(5)->setCheckState(Qt::Checked);

    logPlayer = new LogPlayer();

    connect(ui->pb_Carregar_Log_File,
            &QPushButton::clicked,
            this,
            &Interface::SLOT_openLogFile);

    connect(ui->pb_Reproduzir_Log_File,
            &QPushButton::clicked,
            this,
            &Interface::SLOT_toggleLogPlayerStopped);

    connect(ui->pb_ConfigurarNetworkLogFile,
            &QPushButton::clicked,
            this,
            &Interface::SLOT_setNetworkConfiguration);

    connect(logPlayer,
            &LogPlayer::SIGNAL_positionChanged,
            this,
            &Interface::SLOT_updateLogPlayerCurrentFrame);

    connect(logPlayer,
            &LogPlayer::SIGNAL_refreshEventsInterface,
            this,
            &Interface::SLOT_refreshStatusLoadLogFile);

    connect(logPlayer,
            &LogPlayer::SIGNAL_finished,
            this,
            &Interface::SLOT_notifyLogFileEnded);


    ui->pb_Carregar_Log_File->setEnabled(false);
    ui->pb_Reproduzir_Log_File->setEnabled(false);

    process = new QProcess();

    process->setProcessChannelMode(QProcess::MergedChannels);

    sPathToOutputDirectory = QDir::currentPath();
    sPathToAnalysisFile = QDir::currentPath();

    ui->le_OutputDirectory->setText(sPathToOutputDirectory);

    ui->te_terminal->setReadOnly(true);

    jogadaAtualAnalise = Jogada();

    connect(ui->pb_DownloadTigers,
            &QPushButton::clicked,
            this,
            &Interface::SLOT_downloadMirrorTigers);

    connect(ui->pb_DownloadJackets,
            &QPushButton::clicked,
            this,
            &Interface::SLOT_downloadMirrorJackets);

    connect(ui->cb_OutputDirectory,
            &QCheckBox::clicked,
            this,
            &Interface::SLOT_setDownloadOutputDirectoryReadOnly);


    connect(ui->pb_OutputDirectory,
            &QPushButton::clicked,
            this,
            &Interface::SLOT_setDownloadOutputDirectory);

    connect(this,
            &Interface::SIGNAL_deleteMirror,
            this,
            &Interface::SLOT_deleteMirror);

    connect(this,
            &Interface::SIGNAL_deleteProcess,
            this,
            &Interface::SLOT_deleteProcess);

    ui->te_terminal->setAlignment(Qt::AlignCenter);

    ui->le_OutputDirectory->setReadOnly(true);

    ui->cb_OutputDirectory->setChecked(true);

    threadVision = new QThread(this);
    threadAnalysis= new QThread(this);

    this->setWindowTitle("LogAnalyserRoboFEI-SSL");

    ui->cb_NormalStart->setChecked(true);
    ui->cb_ForceStart->setChecked(true);
    ui->cb_DirectYellow->setChecked(true);
    ui->cb_DirectBlue->setChecked(true);
    ui->cb_IndirectYellow->setChecked(true);
    ui->cb_IndirectBlue->setChecked(true);


    connect(ui->hs_ProgressLogFile,
            &QSlider::sliderPressed,
            this,
            &Interface::SLOT_stopLogPlayer);

    connect(ui->hs_ProgressLogFile,
            &QSlider::sliderReleased,
            this,
            &Interface::SLOT_restartLogPlayer);

    connect(new QShortcut(QKeySequence(Qt::ALT + Qt::Key_1), this),
            &QShortcut::activated,
            this,
            &Interface::SLOT_switchToTab1);

    connect(new QShortcut(QKeySequence(Qt::ALT + Qt::Key_2), this),
            &QShortcut::activated,
            this,
            &Interface::SLOT_switchToTab2);

    connect(new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_D), this),
            &QShortcut::activated,
            this,
            &Interface::SLOT_logPlayerForward);

    connect(new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_A), this),
            &QShortcut::activated,
            this,
            &Interface::SLOT_logPlayerBackwards);

    connect(new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_S), this),
            &QShortcut::activated,
            this,
            &Interface::SLOT_toggleLogPlayerStopped);

    connect(new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q), this),
            &QShortcut::activated,
            this,
            &QWidget::close);

    connect(ui->pb_forwardsLogPlayer,
            &QPushButton::clicked,
            this,
            &Interface::SLOT_logPlayerForward);

    connect(ui->pb_backWardsLogPlayer,
            &QPushButton::clicked,
            this,
            &Interface::SLOT_logPlayerBackwards);



    connect(ui->cb_OutputDirectoryAnalise,
            &QCheckBox::clicked,
            this,
            &Interface::SLOT_setOutputDirectoryAnalisesReadOnly);


    connect(ui->pb_OutputDirectoryAnalise,
            &QPushButton::clicked,
            this,
            &Interface::SLOT_setOutputDirectoryAnalises);

    ui->le_OutputDirectoryAnalise->setText(sPathToAnalysisFile);
    ui->le_NomeDoArquivoAnalises->setText(sAnalysisFileName);

    ui->pb_DetailedStatistics->setEnabled(false);
    connect(ui->pb_DetailedStatistics, &QPushButton::released,
            [this](){  statisticsDialog->setVisible(!statisticsDialog->isVisible());  });

#ifdef Q_OS_WINDOWS
    ui->pb_DownloadTigers->setEnabled(false);
    ui->pb_DownloadJackets->setEnabled(false);
#endif

    tray = new QSystemTrayIcon(this);
    tray->setIcon( this->windowIcon() );

    this->showMaximized();
}

Interface::~Interface()
{
    threadVision->quit();
    threadVision->wait();

    threadAnalysis->quit();
    threadAnalysis->wait();

    vSetLogPlayerStopped(true);

    if(process->isOpen())
        process->kill();

    qApp->quit();
}

void Interface::vMessageOutput(const QtMsgType type,
                               const QMessageLogContext& context,
                               const QString& msg)
{
    QScopedPointer<QListWidgetItem> item( new QListWidgetItem() );

    switch (type)
    {
        case QtDebugMsg:
            item->setForeground(Qt::cyan);
            qDebug()<<"Debug: "<<msg.toLatin1().data();
            break;

        case QtInfoMsg:
            item->setForeground(Qt::green);
            qInfo()<<"Info: "<<msg.toLatin1().data();
            break;

        case QtWarningMsg:
            item->setForeground(Qt::yellow);
            qWarning()<<"Warning: "<<msg.toLatin1().data();
            break;

        case QtCriticalMsg:
            item->setForeground(Qt::red);
            qCritical()<<"Critical: "<<msg.toLatin1().data();
            break;

        case QtFatalMsg:
            qFatal("%s", QString("Fatal: " + msg).toLatin1().data());
            return;
    }
    item->setText(msg);

    if(ui->listWidget_debug->count() > 50)
        delete ui->listWidget_debug->takeItem(ui->listWidget_debug->count() - 1);

    ui->listWidget_debug->insertItem(0, item.take());


}

void Interface::closeEvent(QCloseEvent *event)
{
    this->close();
}

void Interface::reject(){}


void Interface::SLOT_connectNetwork(){

    ui->pb_Rede_RecebePacotes->setText("Stop Receiving");

    QList<QNetworkInterface> ifInterfaceLocal = QNetworkInterface::allInterfaces();

    QHostAddress haEnderecoGrupoRefbox(ui->le_Rede_IPReferee_Log->text());
    udpRefbox.reset( new QUdpSocket(this) );
    udpRefbox->bind(QHostAddress::AnyIPv4, ui->le_Rede_PortaReferee_Log->text().toInt(), QUdpSocket::ShareAddress);
    udpRefbox->setMulticastInterface(ifInterfaceLocal.at(ui->cb_Rede_InterfaceRede->currentIndex()));
    udpRefbox->joinMulticastGroup(haEnderecoGrupoRefbox);

    vision.reset( new Vision() );


    connect(this,
            &Interface::SIGNAL_networkConfigurationVision,
            vision.get(),
            &Vision::SLOT_connectNetwork);

    connect(this,
            &Interface::SIGNAL_disconnectVision,
            vision.get(),
            &Vision::SLOT_disconnectNetwork);

    connect(vision.get(),
            &Vision::SIGNAL_sendVisionData,
            this,
            &Interface::SLOT_RecebeDadosVisao);

    connect(vision.get(),
            &Vision::SIGNAL_fieldSize,
            this,
            &Interface::SLOT_ChegouGeometria);

    if(analysisEnvironment){

        connect(vision.get(),
                &Vision::SIGNAL_sendVisionData,
                analysisEnvironment.get(),
                &Analysis::SLOT_dadosVisao);

    }

    vision->moveToThread(threadVision);

    threadVision->start();

    emit SIGNAL_networkConfigurationVision(ui->le_Rede_IPVisao_Log->text(),
                                            ui->le_Rede_PortaVisao_Log->text().toInt(),
                                            ui->cb_Rede_InterfaceRede->currentIndex());

    connect(udpRefbox.get(),
            &QIODevice::readyRead,
            this,
            &Interface::SLOT_ProcessaReferee);

    ui->pb_Rede_RecebePacotes->disconnect(SIGNAL(clicked()));

    connect(ui->pb_Rede_RecebePacotes,
            &QAbstractButton::clicked,
            this,
            &Interface::SLOT_disconnectNetwork);
}

void Interface::vDrawField()
{
    if(fieldImage)
    {
        if(fieldImage->size() != ui->lb_Campo_ImagemCampo->size())
        {
            vInicializaDesenhos();
        }
    }
    vDesenharCampos();
    vDesenharMiraBola();
    vDesenharPredicoesKalman();
    vDesenhaPosicaoDesignada();
    vDesenhaDestacaReceptor();
    vDesenhaVetorDaBola();
    vDesenhaRoboComABola();

    drawMapField->vDesenhaPontos(environmentInterface->vtBolaKALMAN, *fieldImage, Qt::red, 3); // Posicao da bola com filtro

    //Desenha o traço do robô monitorado

    //Deixa cinza a regiao nao utilizada do campo caso nao esteja usando o campo inteiro
}

void Interface::vDesenharCampos()
{
    drawMapField->vDesenhaAmbiente(environmentInterface,
                                     *fieldImage);
}



void Interface::vDesenharMiraBola()
{

    if(ui->lwid_Mapa_Mostrar->findItems("Show Ball Sight", Qt::MatchExactly).constFirst()->checkState() == Qt::Checked)//Mostra mira da bola para o gol inimigo
    {
        QVector2D posBola = environmentInterface->vt2dBallCurrentPosition();
        QVector<QVector3D> vt3dPosicoesTimeAzul;
        vt3dPosicoesTimeAzul.clear();
        vt3dPosicoesTimeAzul.append( environmentInterface->vt3dGetAllPositions(otAzul));
        QVector<QVector2D> mira;
        mira.clear();
        mira.append( Common::vt2dIntervaloLivreGol(posBola,
                                                              environmentInterface->iYellowSide,
                                                              environmentInterface->iGetGoalWidth(),
                                                              environmentInterface->iGetGoalDepth(),
                                                              environmentInterface->getFieldSize(),
                                                              vt3dPosicoesTimeAzul) );


        if(!mira.isEmpty())//Desenha o intervalo livre do gol azul
        {
            drawMapField->vDesenhaMiraBola(mira, posBola, *fieldImage, Qt::blue);
        }

        QVector<QVector3D> vt3dPosicoesTimeAmarelo = {};
        vt3dPosicoesTimeAmarelo.append( environmentInterface->vt3dGetAllPositions(otAmarelo));
        mira.clear();
        mira.append( Common::vt2dIntervaloLivreGol(posBola,
                                                              environmentInterface->iBlueSide,
                                                              environmentInterface->iGetGoalWidth(),
                                                              environmentInterface->iGetGoalDepth(),
                                                              environmentInterface->getFieldSize(),
                                                              vt3dPosicoesTimeAmarelo) );

        if(!mira.isEmpty())//Desenha o intervalo livre do gol amarelo
        {
            drawMapField->vDesenhaMiraBola(mira, posBola, *fieldImage, Qt::yellow);
        }
    }
}


void Interface::vDesenharPredicoesKalman()
{
    if(ui->lwid_Mapa_Mostrar->findItems("Show Kalman Predict", Qt::MatchExactly).constFirst()->checkState() == Qt::Checked)
    {
        // desenha predicao da bola kalman
        if(!environmentInterface->vtPredicaoBola.isEmpty())
        {
            QVector4D aux = Common::vt4dCalculaRetaMedia(environmentInterface->vtPredicaoBola);
            QVector<QVector2D> aux2; aux2.clear();
            aux2.append(aux.toVector2D());
            aux2.append(environmentInterface->vt2dBallCurrentPosition());
            aux2.append(QVector2D(aux.z(), aux.w()));
            drawMapField->vDesenhaLinhas(aux2,*fieldImage,Qt::black); // Pontos da bola depois do Kalman
        }
        drawMapField->vDesenhaPontos(environmentInterface->vtPredicaoBola, *fieldImage, Qt::white, 3); // Predição da bola

        // desenha predicao dos robos kalman
        for(int n=0; n < PLAYERS_PER_SIDE ;n++)
        {
            if((!environmentInterface->vtPredicaoAmarelo[n].isEmpty()) && environmentInterface->rbtYellow[n].getAttributes().bOnField )
            {
                QVector4D aux = Common::vt4dCalculaRetaMedia(environmentInterface->vtPredicaoAmarelo[n]);
                QVector<QVector2D> aux2; aux2.clear();
                aux2.append(aux.toVector2D());
                aux2.append(environmentInterface->rbtYellow[n].vt2dGetCurrentPosition());
                aux2.append(QVector2D(aux.z(), aux.w()));
                drawMapField->vDesenhaLinhas(aux2,*fieldImage,Qt::red); // Pontos da bola depois do Kalman
            }
            if((!environmentInterface->vtPredicaoAzul[n].isEmpty()) && environmentInterface->rbtBlue[n].getAttributes().bOnField )
            {
                QVector4D aux = Common::vt4dCalculaRetaMedia(environmentInterface->vtPredicaoAzul[n]);
                QVector<QVector2D> aux2; aux2.clear();
                aux2.append(aux.toVector2D());
                aux2.append(environmentInterface->rbtBlue[n].vt2dGetCurrentPosition());
                aux2.append(QVector2D(aux.z(), aux.w()));
                drawMapField->vDesenhaLinhas(aux2,*fieldImage,Qt::cyan); // Pontos da bola depois do Kalman
            }
        }
    }
}



void Interface::vDesenhaPosicaoDesignada(){
    if(ui->lwid_Mapa_Mostrar->findItems("Show Ball Placement Designed Position", Qt::MatchExactly).constFirst()->checkState() == Qt::Checked &&
       (environmentInterface->currentRefereeCommand == SSL_Referee_Command_BALL_PLACEMENT_BLUE ||
        environmentInterface->currentRefereeCommand == SSL_Referee_Command_BALL_PLACEMENT_YELLOW))
    {
        const QVector2D posDesignada = environmentInterface->vt2dBallPlacementPosition,
                posBola = environmentInterface->vt2dBallCurrentPosition();

        drawMapField->vDesenhaBallPlacement(posDesignada, posBola, *fieldImage);
    }
}


void Interface::vDesenhaDestacaReceptor(){

    if(ui->lwid_Mapa_Mostrar->findItems("Highlight Analysis Receiver", Qt::MatchExactly).
       constFirst()->checkState() == Qt::Checked){

        switch (estadoDeJogoAnalise) {

            case PASSE_EM_ANDAMENTO:

                if(jogadaAtualAnalise.timeDaJogada == teamYelllow){

                    drawMapField->vDestacaReceptor(
                                environmentInterface->rbtYellow[jogadaAtualAnalise.jogadaPasse.iIdReceptor].vt2dGetCurrentPosition(),
                            environmentInterface->vt2dBallCurrentPosition(),
                            "#00ff00",
                            *fieldImage);
                }
                else if(jogadaAtualAnalise.timeDaJogada == teamBlue){

                    drawMapField->vDestacaReceptor(
                                environmentInterface->rbtBlue[jogadaAtualAnalise.jogadaPasse.iIdReceptor].vt2dGetCurrentPosition(),
                            environmentInterface->vt2dBallCurrentPosition(),
                            "#00ff00",
                            *fieldImage);
                }
                break;

            case CHUTE_AO_GOL_EM_ANDAMENT0:

                if(jogadaAtualAnalise.timeDaJogada == teamYelllow){

                    drawMapField->vDestacaReceptor(environmentInterface->vt2dCenterBlueGoalPosition(),
                                                     environmentInterface->vt2dBallCurrentPosition(),
                                                     "#00ff00",
                                                     *fieldImage);
                }

                else if(jogadaAtualAnalise.timeDaJogada == teamBlue){

                    drawMapField->vDestacaReceptor(environmentInterface->vt2dCenterYellowGoalPosition(),
                                                     environmentInterface->vt2dBallCurrentPosition(),
                                                     "#00ff00",
                                                     *fieldImage);
                }
                break;

            case PASSE_OU_CHUTE_EM_ANDAMENTO:

                if(jogadaAtualAnalise.timeDaJogada == teamYelllow){

                    for(auto n : jogadaAtualAnalise.vtiReceptores){

                        if(n == CHUTE_AO_GOL)
                            drawMapField->vDestacaReceptor(environmentInterface->vt2dCenterBlueGoalPosition(),
                                                             environmentInterface->vt2dBallCurrentPosition(),
                                                             "#ff0000",
                                                             *fieldImage);

                        else if(n == NENHUM)
                            drawMapField->vDestacaPontoComUmXis(environmentInterface->vt2dBallCurrentPosition(),
                                                                  "#ff0000",
                                                                  ROBOT_DIAMETER,
                                                                  *fieldImage);

                        else
                            drawMapField->vDestacaReceptor(environmentInterface->rbtYellow[n].vt2dGetCurrentPosition(),
                                                             environmentInterface->vt2dBallCurrentPosition(),
                                                             "#ff0000",
                                                             *fieldImage);

                    }
                }
                else if(jogadaAtualAnalise.timeDaJogada == teamBlue){
                    for(auto n : jogadaAtualAnalise.vtiReceptores){

                        if(n == CHUTE_AO_GOL)
                            drawMapField->vDestacaReceptor(environmentInterface->vt2dCenterYellowGoalPosition(),
                                                             environmentInterface->vt2dBallCurrentPosition(),
                                                             "#ff0000",
                                                             *fieldImage);

                        else if(n == NENHUM)
                            drawMapField->vDestacaPontoComUmXis(environmentInterface->vt2dBallCurrentPosition(),
                                                                  "#ff0000",
                                                                  ROBOT_DIAMETER,
                                                                  *fieldImage);

                        else
                            drawMapField->vDestacaReceptor(environmentInterface->rbtBlue[n].vt2dGetCurrentPosition(),
                                                             environmentInterface->vt2dBallCurrentPosition(),
                                                             "#ff0000",
                                                             *fieldImage);

                    }
                }
                break;

            default:
                break;
        }

        if(bConsiderarJogadaSecundariaAnalise){

            if(jogadaParalelaAnalise.timeDaJogada == teamYelllow){

                drawMapField->vDestacaReceptor(
                            environmentInterface->rbtYellow[jogadaParalelaAnalise.iIdRoboComABola].vt2dGetCurrentPosition(),
                        environmentInterface->rbtYellow[jogadaParalelaAnalise.jogadaPasse.iIdReceptor].vt2dGetCurrentPosition(),
                        "#555555",
                        *fieldImage);
            }

            else if(jogadaParalelaAnalise.timeDaJogada == teamBlue){

                drawMapField->vDestacaReceptor(
                            environmentInterface->rbtBlue[jogadaParalelaAnalise.iIdRoboComABola].vt2dGetCurrentPosition(),
                        environmentInterface->rbtBlue[jogadaParalelaAnalise.jogadaPasse.iIdReceptor].vt2dGetCurrentPosition(),
                        "#555555",
                        *fieldImage);
            }
        }
    }
}

void Interface::vDesenhaVetorDaBola(){

    if(ui->lwid_Mapa_Mostrar->findItems("Highlight Ball Path", Qt::MatchExactly).
       constFirst()->checkState() == Qt::Checked){

        drawMapField->vDesenhaVetorDaBola(environmentInterface->vt2dBallCurrentPosition(),
                                            environmentInterface->vt2dBallVelocity(),
                                            *fieldImage);
    }


}


void Interface::vDesenhaRoboComABola()
{
    if(ui->lwid_Mapa_Mostrar->findItems("Highlight Analysis Ball Owner", Qt::MatchExactly).
       constFirst()->checkState() == Qt::Checked)
    {

        if(jogadaAtualAnalise.timeDaJogada == teamYelllow)
        {
            drawMapField->vDestacaPontoComUmXis(environmentInterface->rbtYellow[jogadaAtualAnalise.iIdRoboComABola].
                    vt2dGetCurrentPosition(),
                    "#000000",
                    ROBOT_DIAMETER,
                    *fieldImage);
        }
        else if(jogadaAtualAnalise.timeDaJogada == teamBlue)
        {
            drawMapField->vDestacaPontoComUmXis(environmentInterface->rbtBlue[jogadaAtualAnalise.iIdRoboComABola].
                    vt2dGetCurrentPosition(),
                    "#000000",
                    ROBOT_DIAMETER,
                    *fieldImage);
        }
    }
}

void Interface::vInicializaDesenhos()
{
    bool tamanhoCampoMudou= false;

    qDebug() << "vInicializaDesenhos:" << "labelJogo = "  << ui->lb_Campo_ImagemCampo->size();

    if(fieldImage)
    {
        if(ui->lb_Campo_ImagemCampo->size() != fieldImage->size() )
        {
            tamanhoCampoMudou = true;
        }

    }
    if(!fieldImage)
        tamanhoCampoMudou = true;
    //Atualiza o tamanho do campo

    if(tamanhoCampoMudou == true)
    {
        QVector2D tamanhoCampo = environmentInterface->getFieldSize();

        //        if(tmrAtualizacaoDesenhos->isActive() == false)
        //        {
        //            etmFPSInterface.restart();
        //        }

        //Mapa
        //        QVector2D tamanhoLabel = QVector2D(ui->lb_Testes_ImagemCampo->width(), ui->lb_Testes_ImagemCampo->height());
        //        float fEscalaDesenhoX = (tamanhoCampo.x() + iOffsetCampo*2) / tamanhoLabel.y(),
        //              fEscalaDesenhoY = (tamanhoCampo.y() + iOffsetCampo*2) / tamanhoLabel.x();
        QSize sizeLabelJogo  = ui->lb_Campo_ImagemCampo->size()  ,
                sizeGol        = QSize(environmentInterface->iGetGoalWidth(), environmentInterface->iGetGoalDepth()),
                sizePenalti    = QSize(environmentInterface->iGetDefenseAreaWidth(), environmentInterface->iGetDefenseAreaHeigth());



        drawMapField.reset( new DrawMap(tamanhoCampo, sizeLabelJogo, sizeGol, sizePenalti) );
        fieldImage.reset( new QImage(sizeLabelJogo, QImage::Format_ARGB32));

    }
}




void Interface::SLOT_disconnectNetwork(){

    emit SIGNAL_disconnectVision();
    QThread::currentThread()->msleep(20);

    bInicializaInterface = false;

    udpRefbox.reset();

    if(analysisEnvironment)
        vDisableAnalysis();

    ui->pb_Rede_RecebePacotes->setText("Start Receiving");
    ui->pb_Rede_RecebePacotes->disconnect(SIGNAL(clicked()));
    connect(ui->pb_Rede_RecebePacotes, &QPushButton::clicked,
            this, &Interface::SLOT_connectNetwork);

    threadVision->quit();
    threadVision->wait();


    vision.reset();
}


void Interface::SLOT_ChegouGeometria()
{
    if(!bInicializaInterface )
    {
        bInicializaInterface = true;

        vInicializaDesenhos();
    }
}

void Interface::SLOT_RecebeDadosVisao(const Environment& ambiente, bool mudouGeometria)
{
    environmentInterface->vVisionUpdate(ambiente);

    if(bInicializaInterface == true)
    {
        if(!tmrUpdateDrawing->isActive())
        {
            tmrUpdateDrawing->setTimerType(Qt::PreciseTimer);
            tmrUpdateDrawing->start(TIME_LOOP_INTERFACE);
        }
        if(!tmrUpdateInterfaceInformation->isActive())
        {
            tmrUpdateInterfaceInformation->start(4*TIME_LOOP_INTERFACE);
        }
    }

    if(mudouGeometria == true)
    {
        vInicializaDesenhos();
    }
}

void Interface::SLOT_ProcessaReferee(){

    while(udpRefbox->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udpRefbox->pendingDatagramSize());
        datagram.fill(0,udpRefbox->pendingDatagramSize());
        udpRefbox->readDatagram(datagram.data(), datagram.size());

        SSL_Referee sslreferee;
        sslreferee.ParseFromArray(datagram, datagram.size());

        roboController.addReferee(sslreferee);

        QTime tempoRestante(0,0,0);
        tempoRestante = tempoRestante.addMSecs(sslreferee.stage_time_left()/1e3);
        ui->time_TempoRestante->setTime(tempoRestante);


        ui->l_ComandosRecebidosReferee->setText( roboController.getCommand(sslreferee));
        environmentInterface->currentRefereeCommand = sslreferee.command();
        environmentInterface->currentRefereeStage = sslreferee.stage();

        QString strEstagio;
        std::string strEstagioAux = sslreferee.Stage_Name(sslreferee.stage()) ;
        for(auto n : strEstagioAux)
            strEstagio.append(n);

        ui->le_EstadoJogoAtual->setText(strEstagio);

        //============================================TIME AMARELO=================================
        QTime tempoAmarelo(0,0,0), tempoTimeout(0,0,0);

        ui->gb_TimeAmarelo->setTitle( QString(sslreferee.yellow().name().c_str()) ); //Nome
        ui->lcd_TimeAmarelo_Gols->display( QString::number(sslreferee.yellow().score()) );//Placar
        ui->lcd_TimeAmarelo_Cartoes_Vermelho->display( QString::number(sslreferee.yellow().red_cards()) );//Cartoes vermelhos
        ui->lcd_TimeAmarelo_Cartoes_Amarelo->display( QString::number(sslreferee.yellow().yellow_cards()) );//Cartoes amarelos

        if(sslreferee.yellow().yellow_card_times_size() > 0)
        {
            tempoAmarelo = tempoAmarelo.addMSecs(sslreferee.yellow().yellow_card_times(0)/1e3);

            ui->time_TimeAmarelo_TempoCartaoAmarelo->setTime(tempoAmarelo);//Tempo de cartao amarelo
        }
        else
        {
            ui->time_TimeAmarelo_TempoCartaoAmarelo->setTime(tempoAmarelo);//Tempo de cartao amarelo
        }

        ui->lcd_TimeAmarelo_TimeoutsPedidos->display(QString::number(sslreferee.yellow().timeouts()));// Timeouts restantes

        tempoTimeout = tempoTimeout.addMSecs(sslreferee.yellow().timeout_time()/1e3);
        ui->time_TimeAmarelo_TempoTimeout->setTime(tempoTimeout);//Tempo de timeout

        ui->lcd_TimeAmarelo_IDGoleiro->display(QString::number(sslreferee.yellow().goalie()));// ID do goleiro



        //=========================================================================================

        //============================================TIME AZUL=================================

        tempoAmarelo.setHMS(0,0,0);
        tempoTimeout.setHMS(0,0,0);

        ui->gb_TimeAzul->setTitle( QString(sslreferee.blue().name().c_str()) ); //Nome
        ui->lcd_TimeAzul_Gols->display( QString::number(sslreferee.blue().score()) );//Placar
        ui->lcd_TimeAzul_Cartoes_Vermelho->display( QString::number(sslreferee.blue().red_cards()) );//Cartoes vermelhos
        ui->lcd_TimeAzul_Cartoes_Amarelo->display( QString::number(sslreferee.blue().yellow_cards()) );//Cartoes amarelos

        if(sslreferee.blue().yellow_card_times_size() > 0)
        {
            tempoAmarelo = tempoAmarelo.addMSecs(sslreferee.blue().yellow_card_times(0)/1e3);

            ui->time_TimeAzul_TempoCartaoAmarelo->setTime(tempoAmarelo);//Tempo de cartao amarelo
        }
        else
        {
            ui->time_TimeAzul_TempoCartaoAmarelo->setTime(tempoAmarelo);//Tempo de cartao amarelo
        }

        ui->lcd_TimeAzul_TimeoutsPedidos->display(QString::number(sslreferee.blue().timeouts()));// Timeouts restantes

        tempoTimeout = tempoTimeout.addMSecs(sslreferee.blue().timeout_time()/1e3);
        ui->time_TimeAzul_TempoTimeout->setTime(tempoTimeout);//Tempo de timeout

        ui->lcd_TimeAzul_IDGoleiro->display(QString::number(sslreferee.blue().goalie()));// ID do goleiro
        ui->lcd_TimeAmarelo_IDGoleiro->display(QString::number(sslreferee.yellow().goalie()));

        //=========================================================================================

        SSL_Referee_Point positionOfBallPlacement = sslreferee.designated_position();

        environmentInterface->vUpdateBallBlacementPosition(QVector2D(positionOfBallPlacement.x(),
                                                         positionOfBallPlacement.y()));




        environmentInterface->currentRefereeCommand = sslreferee.command();
        environmentInterface->currentRefereeStage = sslreferee.stage();

        environmentInterface->vUpdateSides(sslreferee.yellow().goalie(),
                                          sslreferee.blue().goalie());


        emit SIGNAL_refereeInformation(static_cast<int>(sslreferee.command()),
                                       sslreferee.yellow().goalie(),
                                       sslreferee.blue().goalie(),
                                       environmentInterface->iBlueSide);
    }

    emit SIGNAL_statisticsData(ui->gb_TimeAmarelo->title(),
                               ui->lcd_TimeAmarelo_Gols->intValue(),
                               ui->lcd_TimeAmarelo_Cartoes_Amarelo->intValue(),
                               ui->lcd_TimeAmarelo_Cartoes_Vermelho->intValue(),
                               QTime(0, 0, 0).secsTo(ui->time_TimeAmarelo_TempoTimeout->time()),
                               ui->gb_TimeAzul->title(),
                               ui->lcd_TimeAzul_Gols->intValue(),
                               ui->lcd_TimeAzul_Cartoes_Amarelo->intValue(),
                               ui->lcd_TimeAzul_Cartoes_Vermelho->intValue(),
                               QTime(0, 0, 0).secsTo(ui->time_TimeAzul_TempoTimeout->time()));
}



void Interface::SLOT_AtualizaDesenho(){


    //Desenha os campos
    if(fieldImage){

        vDrawField();
        ui->lb_Campo_ImagemCampo->setPixmap(QPixmap::fromImage(*fieldImage, Qt::AutoColor));
    }


}

void Interface::SLOT_openLogFile()
{
    vSetLogPlayerStopped(true);

    QString filename = QFileDialog::getOpenFileName(this, "Select log file", "", "Log files (*.log *.log.gz)");

    if (!filename.isEmpty())
    {
        bLogFileLoaded = false;
        if(analysisEnvironment)
        {
            vDisableAnalysis();
        }
        SLOT_disconnectNetwork();

        vLoadFile(filename);

        if((ui->rb_TransmitirRemotamente->isChecked() &&
            bNetworkConfiguration))
        {
            ui->pb_Reproduzir_Log_File->setEnabled(true);
            ui->hs_ProgressLogFile->setEnabled(true);
        }
        else if(ui->rb_ReproduzirEmTempoReal->isChecked())
        {
            ui->pb_Reproduzir_Log_File->setEnabled(true);
            ui->hs_ProgressLogFile->setEnabled(true);

            vStartLocalPlay();
        }
        else
        {
            ui->pb_Reproduzir_Log_File->setEnabled(false);
            ui->hs_ProgressLogFile->setEnabled(false);
        }

        bLogFileLoaded = true;
    }
}

void Interface::SLOT_toggleLogPlayerStopped()
{
    if(bLogFileLoaded)vSetLogPlayerStopped(!logPlayerStopped);
}

void Interface::SLOT_stopLogPlayer()
{
    logPlayer->stop();
}

void Interface::SLOT_restartLogPlayer()
{
    logPlayer->start(ui->hs_ProgressLogFile->sliderPosition());
}

void Interface::SLOT_updateLogPlayerCurrentFrame(quint32 frame, double time)
{
    iCurrentFrameLogFile = frame;

    ui->hs_ProgressLogFile->setValue(frame);

    QString progresso{};
    progresso.append(QString::number(frame));
    progresso.append('/');
    progresso.append(QString::number(iMaximumFrameLogFile));
    progresso.append('\t');

    progresso.append(QString("%1:%2.%3")
                     .arg((int) (time / 1E9) / 60)
                     .arg((int) (time / 1E9) % 60, 2, 10, QChar('0'))
                     .arg((int) (time / 1E6) % 1000, 3, 10, QChar('0')));

    ui->l_statusLogFile->setText(progresso);
}

void Interface::SLOT_setNetworkConfiguration(){

    if(bNetworkConfiguration)
    {
        vSetLogPlayerStopped(true);

        logPlayer->clearAllNetworkConfiguration();

        ui->pb_ConfigurarNetworkLogFile->setText("Start Sending");

        ui->le_Rede_IPVisao_Log->setEnabled(true);
        ui->le_Rede_PortaVisao_Log->setEnabled(true);

        ui->le_Rede_IPReferee_Log->setEnabled(true);
        ui->le_Rede_PortaReferee_Log->setEnabled(true);

        ui->le_Rede_IPVisionLegacy_Log->setEnabled(true);
        ui->le_Rede_IPVisionLegacy_Log->setEnabled(true);

        ui->pb_Carregar_Log_File->setEnabled(false);
        ui->pb_Reproduzir_Log_File->setEnabled(false);
    }
    else
    {
        logPlayer->setNetworkConfiguration(
                    ui->le_Rede_IPReferee_Log->text(),
                    ui->le_Rede_PortaReferee_Log->text().toUInt(),

                    ui->le_Rede_IPVisao_Log->text(),
                    ui->le_Rede_PortaVisao_Log->text().toUInt(),

                    ui->le_Rede_IPVisionLegacy_Log->text(),
                    ui->le_Rede_PortaVisionLegacy_Log->text().toUInt()
                    );

        if(bLogFileLoaded)
            ui->pb_Reproduzir_Log_File->setEnabled(true);

        ui->pb_ConfigurarNetworkLogFile->setText("Stop Sending");

        ui->le_Rede_IPVisao_Log->setEnabled(false);
        ui->le_Rede_PortaVisao_Log->setEnabled(false);

        ui->le_Rede_IPReferee_Log->setEnabled(false);
        ui->le_Rede_PortaReferee_Log->setEnabled(false);

        ui->le_Rede_IPVisionLegacy_Log->setEnabled(false);
        ui->le_Rede_IPVisionLegacy_Log->setEnabled(false);

        ui->pb_Carregar_Log_File->setEnabled(true);

    }

    bNetworkConfiguration = !bNetworkConfiguration;

}

void Interface::SLOT_getTerminalOutput(){


    if(process->isOpen())
    {
        QString currentText = ui->te_terminal->toPlainText();

        currentText = currentText.left(currentText.lastIndexOf('\n'));
        currentText.append(process->readLine());

        ui->te_terminal->setPlainText(currentText);
    }

    qApp->processEvents();

}

void Interface::SLOT_downloadMirrorTigers(){

    mirror.reset(new MirrorLogFiles());

    connect(mirror.get(),
            &MirrorLogFiles::SIGNAL_downloadLinks,
            this,
            &Interface::SLOT_downloadLinks);

    mirror->vSetMirror(TIGERS);

    mirror->show();

    this->setEnabled(false);

}

void Interface::SLOT_downloadMirrorJackets(){

    mirror.reset( new MirrorLogFiles() );

    connect(mirror.get(),
            &MirrorLogFiles::SIGNAL_downloadLinks,
            this,
            &Interface::SLOT_downloadLinks);

    mirror->vSetMirror(JACKETS);

    mirror->show();

    this->setEnabled(false);
}

void Interface::SLOT_downloadLinks(const QList<QString>& listLinks)
{
    mirror.reset();
    this->setEnabled(true);

    if(listLinks.isEmpty())
    {
        return;
    }

    qApp->processEvents();

    QString terminalText;

    connect(process,
            &QProcess::readyRead,
            this,
            &Interface::SLOT_getTerminalOutput);

    connect(process,
            &QProcess::readChannelFinished,
            this,
            &Interface::SLOT_finishedDownloadMirror);

    connect(process,
            &QProcess::errorOccurred,
            this,
            &Interface::SLOT_finishedDownloadMirror);

    process->setProcessChannelMode(QProcess::MergedChannels);

    QDir::setCurrent(sPathToOutputDirectory);

#ifdef Q_OS_WINDOWS
    process->start("cmd", QStringList());
#else
    process->start("sh", QStringList());
#endif
    for (quint8 i=0; i<listLinks.size(); i++)
    {
        if(i == listLinks.size()-1)
        {
            process->write(QString(" wget  -q --show-progress --progress=bar:force 2>&1 " + listLinks.at(i) ).toLatin1());
            terminalText.append(QString(" wget  -q --show-progress --progress=bar:force 2>&1 " + listLinks.at(i) ));

        }
        else
        {
            process->write(QString(" wget  -q --show-progress --progress=bar:force 2>&1 " + listLinks.at(i) +  ';' ).toLatin1());
            terminalText.append(QString(" wget  -q --show-progress --progress=bar:force 2>&1 " + listLinks.at(i) +  ';' ));
        }

    }

    terminalText.append('\n');

    ui->te_terminal->document()->setPlainText(terminalText);

    qApp->processEvents();

    process->closeWriteChannel();


    if(!process->waitForStarted(2000))
    {
        SLOT_finishedDownloadMirror();
        return;
    }

    if(!process->waitForReadyRead(2000))
    {
        SLOT_finishedDownloadMirror();
        return;
    }

    ui->pb_DownloadTigers->setEnabled(false);

    ui->pb_DownloadJackets->setEnabled(false);

}



void Interface::SLOT_finishedDownloadMirror()
{
    QString currentText = ui->te_terminal->toPlainText();
    process->waitForReadyRead(200);

    currentText.append('\n' + process->readAll());

    process->kill();

    currentText.append("\nFinished");

    ui->te_terminal->document()->setPlainText(currentText);

    ui->pb_DownloadTigers->setEnabled(true);

    ui->pb_DownloadJackets->setEnabled(true);

    emit SIGNAL_deleteProcess();
}

void Interface::SLOT_setDownloadOutputDirectory()
{
    const QString path = QFileDialog::getExistingDirectory();

    if(!path.isEmpty())
    {
        sPathToOutputDirectory = path;
        ui->le_OutputDirectory->setText(sPathToOutputDirectory);
    }
}

void Interface::SLOT_setDownloadOutputDirectoryReadOnly()
{
    ui->le_OutputDirectory->setReadOnly(ui->cb_OutputDirectory->isChecked());
}

void Interface::SLOT_mirrorInterfaceClose()
{
}

void Interface::SLOT_deleteMirror()
{
    mirror.reset();
}

void Interface::SLOT_deleteProcess()
{
    process->disconnect();
}

void Interface::SLOT_jogadaAtualAnalise(Jogada jogadaAnalise)
{
    jogadaAtualAnalise = jogadaAnalise;

    ui->lJogadasEmAndamento->setText(sEstadoDeJogoAtual());
}

void Interface::SLOT_jogadaParalelaAnalise(Jogada jogadaAnalise)
{
    jogadaParalelaAnalise = jogadaAnalise;
}

void Interface::SLOT_setarJogadaEmAndamentoAnalise(bool habilitar)
{
    bConsiderarJogadaSecundariaAnalise = habilitar;
}

void Interface::SLOT_estadoDeJogoAtualAnalise(ESTADOS_DE_JOGO _estadoDeJogoAnalise)
{
    estadoDeJogoAnalise = _estadoDeJogoAnalise;
}


void Interface::vSetLogPlayerStopped(bool p)
{
    logPlayerStopped = p || !logPlayer->good();

    if (logPlayerStopped)
    {
        ui->pb_Reproduzir_Log_File->setText("(Ctrl + S) Play");
        logPlayer->stop();

        ui->rb_TransmitirRemotamente->setEnabled(true);
        ui->rb_ReproduzirEmTempoReal->setEnabled(true);
    }
    else
    {
        ui->pb_Reproduzir_Log_File->setText("(Ctrl + S) Stop");
        logPlayer->start(iCurrentFrameLogFile);

        ui->rb_TransmitirRemotamente->setEnabled(false);
        ui->rb_ReproduzirEmTempoReal->setEnabled(false);
    }
}

void Interface::vLoadFile(const QString& filename)
{
    iCurrentFrameLogFile = 0;

    quint32 maxFrame;
    double duration;

    QList<SSL_Referee_Command> list_comandos{};

    if(ui->cb_Halt->isChecked())
        list_comandos.append(SSL_Referee_Command_HALT);


    if(ui->cb_Stop->isChecked())
        list_comandos.append(SSL_Referee_Command_STOP);


    if(ui->cb_NormalStart->isChecked())
        list_comandos.append(SSL_Referee_Command_NORMAL_START);


    if(ui->cb_ForceStart->isChecked())
        list_comandos.append(SSL_Referee_Command_FORCE_START);


    if(ui->cb_PrepareKickOffYellow->isChecked())
        list_comandos.append(SSL_Referee_Command_PREPARE_KICKOFF_YELLOW);


    if(ui->cb_PrepareKickOffBlue->isChecked())
        list_comandos.append(SSL_Referee_Command_PREPARE_KICKOFF_BLUE);


    if(ui->cb_PreparePenaltyYellow->isChecked())
        list_comandos.append(SSL_Referee_Command_PREPARE_PENALTY_YELLOW);


    if(ui->cb_PreparePenaltyBlue->isChecked())
        list_comandos.append(SSL_Referee_Command_PREPARE_PENALTY_BLUE);


    if(ui->cb_DirectYellow->isChecked())
        list_comandos.append(SSL_Referee_Command_DIRECT_FREE_YELLOW);


    if(ui->cb_DirectBlue->isChecked())
        list_comandos.append(SSL_Referee_Command_DIRECT_FREE_BLUE);


    if(ui->cb_IndirectYellow->isChecked())
        list_comandos.append(SSL_Referee_Command_INDIRECT_FREE_YELLOW);


    if(ui->cb_IndirectBlue->isChecked())
        list_comandos.append(SSL_Referee_Command_INDIRECT_FREE_BLUE);


    if(ui->cb_TimeoutYellow->isChecked())
        list_comandos.append(SSL_Referee_Command_TIMEOUT_YELLOW);


    if(ui->cb_TimeoutBlue->isChecked())
        list_comandos.append(SSL_Referee_Command_TIMEOUT_BLUE);


    if(ui->cb_GoalYellow->isChecked())
        list_comandos.append(SSL_Referee_Command_GOAL_YELLOW);


    if(ui->cb_GoalBlue->isChecked())
        list_comandos.append(SSL_Referee_Command_GOAL_BLUE);


    if(ui->cb_BallPlacementYellow->isChecked())
        list_comandos.append(SSL_Referee_Command_BALL_PLACEMENT_YELLOW);


    if(ui->cb_BallPlacementBlue->isChecked())
        list_comandos.append(SSL_Referee_Command_BALL_PLACEMENT_BLUE);



    ui->pb_Reproduzir_Log_File->setEnabled(false);

    if ( logPlayer->load(filename, maxFrame, duration, list_comandos) ) {

        ui->hs_ProgressLogFile->setValue(0);
        ui->hs_ProgressLogFile->setMaximum(maxFrame);
        iMaximumFrameLogFile = maxFrame;

        QFileInfo fileInfo(filename);
        ui->l_statusLogFile->setText(fileInfo.fileName() + " loaded");


        ui->pb_Reproduzir_Log_File->setEnabled(true);
    }
}

void Interface::vEnableAnalysis()
{
    ui->pb_AtivarAnalises->setText("Disable Analysis");

    sPathToAnalysisFile = ui->le_OutputDirectoryAnalise->text();
    sAnalysisFileName = ui->le_NomeDoArquivoAnalises->text();

    analysisEnvironment.reset( new Analysis(sPathToAnalysisFile, sAnalysisFileName) );

    // Create Statistics Dialog object
    statisticsDialog.reset( new GameStatisticsDialog(this) );
    ui->pb_DetailedStatistics->setEnabled(true);

    connect(this,
            &Interface::SIGNAL_statisticsData,
            statisticsDialog.get(),
            &GameStatisticsDialog::SLOT_getStatisticsFromReferee);

    connect(this,
            &Interface::SIGNAL_setAnalysis,
            analysisEnvironment.get(),
            &Analysis::SLOT_ativaDesativaAnalise);

    connect(this,
            &Interface::SIGNAL_refereeInformation,
            analysisEnvironment.get(),
            &Analysis::SLOT_dadosReferee);

    if(vision)
    {
        connect(vision.get(),
                &Vision::SIGNAL_sendVisionData,
                analysisEnvironment.get(),
                &Analysis::SLOT_dadosVisao);
    }


    connect(analysisEnvironment.get(),
            &Analysis::SIGNAL_jogadaAtualAnalise,
            this,
            &Interface::SLOT_jogadaAtualAnalise);

    connect(analysisEnvironment.get(),
            &Analysis::SIGNAL_jogadaSecundariaAnalise,
            this,
            &Interface::SLOT_jogadaParalelaAnalise);

    connect(analysisEnvironment.get(),
            &Analysis::SIGNAL_habilitarDesabilitarJogadaSecundaria,
            this,
            &Interface::SLOT_setarJogadaEmAndamentoAnalise);

    connect(analysisEnvironment.get(),
            &Analysis::SIGNAL_estadoDejogadaAtualAnalise,
            this,
            &Interface::SLOT_estadoDeJogoAtualAnalise);

    if(bLogFileLoaded &&
       ui->rb_ReproduzirEmTempoReal->isChecked())
    {
        connect(logPlayer,
                &LogPlayer::SIGNAL_jump,
                analysisEnvironment.get(),
                &Analysis::SLOT_jump);
    }

    connect(analysisEnvironment.get(),
            &Analysis::SIGNAL_statisticsData,
            statisticsDialog.get(),
            &GameStatisticsDialog::SLOT_getStatisticsFromAnalysis);

    analysisEnvironment->moveToThread(threadAnalysis);

    threadAnalysis->start();

    emit SIGNAL_setAnalysis(true);
}

void Interface::vDisableAnalysis()
{
    ui->pb_AtivarAnalises->setText("Enable Analysis");
    emit SIGNAL_setAnalysis(false);

    QThread::currentThread()->msleep(20);

    threadAnalysis->quit();
    threadAnalysis->wait();

    analysisEnvironment.reset();

    statisticsDialog.reset();
    ui->pb_DetailedStatistics->setEnabled(false);

    jogadaAtualAnalise.vReinicia();

    estadoDeJogoAnalise = JOGADA_NENHUMA;
    ui->lJogadasEmAndamento->setText("JOGADA_NENHUMA");
}

void Interface::vStartLocalPlay()
{
    vision.reset( new Vision() );

    connect(this, &Interface::SIGNAL_setLocalVisionEnabled,
            vision.get(), &Vision::SLOT_toggleLocalPlayer);

    connect(vision.get(), &Vision::SIGNAL_sendVisionData,
            this, &Interface::SLOT_RecebeDadosVisao);

    connect(vision.get(), &Vision::SIGNAL_fieldSize,
            this, &Interface::SLOT_ChegouGeometria);

    if(analysisEnvironment){

        connect(vision.get(),
                &Vision::SIGNAL_sendVisionData,
                analysisEnvironment.get(),
                &Analysis::SLOT_dadosVisao);

        connect(logPlayer,
                &LogPlayer::SIGNAL_jump,
                analysisEnvironment.get(),
                &Analysis::SLOT_jump);

    }


    connect(logPlayer,
            &LogPlayer::SIGNAL_visionMessageLocal,
            vision.get(),
            &Vision::SLOT_getPackagesFromLocalVision);

    connect(logPlayer,
            &LogPlayer::SIGNAL_jump,
            vision.get(),
            &Vision::SLOT_jump);


    connect(logPlayer,
            &LogPlayer::SIGNAL_refereeMessageLocal,
            this,
            &Interface::SLOT_processLocalReferee);


    vision->moveToThread(threadVision);

    threadVision->start();

    emit SIGNAL_setLocalVisionEnabled(true);

    ui->pb_Reproduzir_Log_File->setEnabled(true);

}

void Interface::vFinishLocalPlay()
{
    emit SIGNAL_setLocalVisionEnabled(false);

    QThread::currentThread()->msleep(20);

    threadVision->quit();
    threadVision->wait();

    ui->pb_Rede_RecebePacotes->disconnect();
    connect(ui->pb_Rede_RecebePacotes, &QPushButton::clicked,
            this, &Interface::SLOT_connectNetwork);

    vDisableAnalysis();

    vision.reset();

    logPlayer->disconnect(SIGNAL(SIGNAL_visionMessageLocal()));
    logPlayer->disconnect(SIGNAL(SIGNAL_jump()));
    logPlayer->disconnect(SIGNAL(SIGNAL_refereeMessageLocal()));
}

QString Interface::sEstadoDeJogoAtual()const{

    switch (estadoDeJogoAnalise) {

        case BOLA_EM_DISPUTA:
            return QString("BOLA_EM_DISPUTA");

        case JOGADA_EM_ANDAMENTO:
            if(jogadaAtualAnalise.timeDaJogada == teamYelllow)
                return QString("JOGADA_\nEM_ANDAMENTO_\nTIME_AMARELO");
            else if(jogadaAtualAnalise.timeDaJogada == teamBlue)
                return QString("JOGADA_\nEM_ANDAMENTO_\nTIME_AZUL");

        case PASSE_EM_ANDAMENTO:
            if(jogadaAtualAnalise.timeDaJogada == teamYelllow)
                return QString("PASSE_\nEM_ANDAMENTO_\nTIME_AMARELO");
            else if(jogadaAtualAnalise.timeDaJogada == teamBlue)
                return QString("PASSE_\nEM_ANDAMENTO_\nTIME_AZUL");

        case CHUTE_AO_GOL_EM_ANDAMENT0:
            if(jogadaAtualAnalise.timeDaJogada == teamYelllow)
                return QString("CHUTE_AO_GOL_\nEM_ANDAMENT0_\nTIME_AMARELO");
            else if(jogadaAtualAnalise.timeDaJogada == teamBlue)
                return QString("CHUTE_AO_GOL_\nEM_ANDAMENT0_\nTIME_AZUL");

        case PASSE_OU_CHUTE_EM_ANDAMENTO:
            if(jogadaAtualAnalise.timeDaJogada == teamYelllow)
                return QString("PASSE_OU_CHUTE_\nEM_ANDAMENTO_\nTIME_AMARELO");
            else if(jogadaAtualAnalise.timeDaJogada == teamBlue)
                return QString("PASSE_OU_CHUTE_\nEM_ANDAMENTO_\nTIME_AZUL");

        case JOGADA_NENHUMA:
            return QString("JOGADA_\nEM_ANDAMENTO_\nTIME_AMARELO");

    }

    return QString("JOGADA_NENHUMA");

}

void Interface::configureDarkStyle()
{
    qApp->setStyle(QStyleFactory::create ("Fusion"));
    QPalette darkPalette;
    darkPalette.setColor (QPalette::BrightText,      Qt::red);
    darkPalette.setColor (QPalette::WindowText,      Qt::white);
    darkPalette.setColor (QPalette::ToolTipBase,     Qt::white);
    darkPalette.setColor (QPalette::ToolTipText,     Qt::white);
    darkPalette.setColor (QPalette::Text,            Qt::white);
    darkPalette.setColor (QPalette::ButtonText,      Qt::white);
    darkPalette.setColor (QPalette::HighlightedText, Qt::black);
    darkPalette.setColor (QPalette::Window,          QColor (53, 53, 53));
    darkPalette.setColor (QPalette::Base,            QColor (25, 25, 25));
    darkPalette.setColor (QPalette::AlternateBase,   QColor (53, 53, 53));
    darkPalette.setColor (QPalette::Button,          QColor (53, 53, 53));
    darkPalette.setColor (QPalette::Link,            QColor (42, 130, 218));
    darkPalette.setColor (QPalette::Highlight,       QColor (42, 130, 218));
    qApp->setPalette (darkPalette);
}



//    qDebug() << "Tempo interface = " << tempoInterface.nsecsElapsed()/1e6 << " ms";

void Interface::on_rb_TransmitirRemotamente_clicked()
{
    if(ui->w_DadosNetworkLogFile->isHidden())
        ui->w_DadosNetworkLogFile->show();

    if(bLogFileLoaded)
        vFinishLocalPlay();

    if(analysisEnvironment)
    {
        vDisableAnalysis();
    }

    ui->pb_Carregar_Log_File->setEnabled(bNetworkConfiguration);
    ui->pb_Reproduzir_Log_File->setEnabled(bLogFileLoaded && bNetworkConfiguration);
}

void Interface::on_rb_ReproduzirEmTempoReal_clicked()
{
    if(!ui->w_DadosNetworkLogFile->isHidden())
        ui->w_DadosNetworkLogFile->hide();

    SLOT_disconnectNetwork();

   if(analysisEnvironment)
   {
       vDisableAnalysis();
   }


   logPlayer->clearAllNetworkConfiguration();

   if(bLogFileLoaded)
       vStartLocalPlay();

   ui->pb_Carregar_Log_File->setEnabled(true);

}

void Interface::on_pb_AtivarAnalises_clicked()
{

    if(!analysisEnvironment)
    {
        vEnableAnalysis();
    }
    else
    {
        vDisableAnalysis();
    }
}

void Interface::SLOT_processLocalReferee(const QByteArray& data)
{
    SSL_Referee sslreferee;
    sslreferee.ParseFromArray(data, data.size());

    roboController.addReferee(sslreferee);

    QTime tempoRestante(0,0,0);
    tempoRestante = tempoRestante.addMSecs(sslreferee.stage_time_left()/1e3);
    ui->time_TempoRestante->setTime(tempoRestante);


    ui->l_ComandosRecebidosReferee->setText( roboController.getCommand(sslreferee));
    environmentInterface->currentRefereeCommand = sslreferee.command();
    environmentInterface->currentRefereeStage = sslreferee.stage();

    QString strEstagio;
    std::string strEstagioAux = sslreferee.Stage_Name(sslreferee.stage()) ;
    for(auto n : strEstagioAux)
        strEstagio.append(n);

    ui->le_EstadoJogoAtual->setText(strEstagio);

    //============================================TIME AMARELO=================================
    QTime tempoAmarelo(0,0,0), tempoTimeout(0,0,0);

    ui->gb_TimeAmarelo->setTitle( QString(sslreferee.yellow().name().c_str()) ); //Nome
    ui->lcd_TimeAmarelo_Gols->display( QString::number(sslreferee.yellow().score()) );//Placar
    ui->lcd_TimeAmarelo_Cartoes_Vermelho->display( QString::number(sslreferee.yellow().red_cards()) );//Cartoes vermelhos
    ui->lcd_TimeAmarelo_Cartoes_Amarelo->display( QString::number(sslreferee.yellow().yellow_cards()) );//Cartoes amarelos

    if(sslreferee.yellow().yellow_card_times_size() > 0)
    {
        tempoAmarelo = tempoAmarelo.addMSecs(sslreferee.yellow().yellow_card_times(0)/1e3);

        ui->time_TimeAmarelo_TempoCartaoAmarelo->setTime(tempoAmarelo);//Tempo de cartao amarelo
    }
    else
    {
        ui->time_TimeAmarelo_TempoCartaoAmarelo->setTime(tempoAmarelo);//Tempo de cartao amarelo
    }

    ui->lcd_TimeAmarelo_TimeoutsPedidos->display(QString::number(sslreferee.yellow().timeouts()));// Timeouts restantes

    tempoTimeout = tempoTimeout.addMSecs(sslreferee.yellow().timeout_time()/1e3);
    ui->time_TimeAmarelo_TempoTimeout->setTime(tempoTimeout);//Tempo de timeout

    ui->lcd_TimeAmarelo_IDGoleiro->display(QString::number(sslreferee.yellow().goalie()));// ID do goleiro



    //=========================================================================================

    //============================================TIME AZUL=================================

    tempoAmarelo.setHMS(0,0,0);
    tempoTimeout.setHMS(0,0,0);

    ui->gb_TimeAzul->setTitle( QString(sslreferee.blue().name().c_str()) ); //Nome
    ui->lcd_TimeAzul_Gols->display( QString::number(sslreferee.blue().score()) );//Placar
    ui->lcd_TimeAzul_Cartoes_Vermelho->display( QString::number(sslreferee.blue().red_cards()) );//Cartoes vermelhos
    ui->lcd_TimeAzul_Cartoes_Amarelo->display( QString::number(sslreferee.blue().yellow_cards()) );//Cartoes amarelos

    if(sslreferee.blue().yellow_card_times_size() > 0)
    {
        tempoAmarelo = tempoAmarelo.addMSecs(sslreferee.blue().yellow_card_times(0)/1e3);

        ui->time_TimeAzul_TempoCartaoAmarelo->setTime(tempoAmarelo);//Tempo de cartao amarelo
    }
    else
    {
        ui->time_TimeAzul_TempoCartaoAmarelo->setTime(tempoAmarelo);//Tempo de cartao amarelo
    }

    ui->lcd_TimeAzul_TimeoutsPedidos->display(QString::number(sslreferee.blue().timeouts()));// Timeouts restantes

    tempoTimeout = tempoTimeout.addMSecs(sslreferee.blue().timeout_time()/1e3);
    ui->time_TimeAzul_TempoTimeout->setTime(tempoTimeout);//Tempo de timeout

    ui->lcd_TimeAzul_IDGoleiro->display(QString::number(sslreferee.blue().goalie()));// ID do goleiro
    ui->lcd_TimeAmarelo_IDGoleiro->display(QString::number(sslreferee.yellow().goalie()));

    //=========================================================================================

    SSL_Referee_Point positionOfBallPlacement = sslreferee.designated_position();

    environmentInterface->vUpdateBallBlacementPosition(QVector2D(positionOfBallPlacement.x(),
                                                     positionOfBallPlacement.y()));




    environmentInterface->currentRefereeCommand = sslreferee.command();
    environmentInterface->currentRefereeStage = sslreferee.stage();

    environmentInterface->vUpdateSides(sslreferee.yellow().goalie(),
                                      sslreferee.blue().goalie());

    emit SIGNAL_refereeInformation(static_cast<int>(sslreferee.command()),
                                   sslreferee.yellow().goalie(),
                                   sslreferee.blue().goalie(),
                                   environmentInterface->iBlueSide);

    emit SIGNAL_statisticsData(ui->gb_TimeAmarelo->title(),
                               ui->lcd_TimeAmarelo_Gols->intValue(),
                               ui->lcd_TimeAmarelo_Cartoes_Amarelo->intValue(),
                               ui->lcd_TimeAmarelo_Cartoes_Vermelho->intValue(),
                               QTime(0, 0, 0).secsTo(ui->time_TimeAmarelo_TempoTimeout->time()),
                               ui->gb_TimeAzul->title(),
                               ui->lcd_TimeAzul_Gols->intValue(),
                               ui->lcd_TimeAzul_Cartoes_Amarelo->intValue(),
                               ui->lcd_TimeAzul_Cartoes_Vermelho->intValue(),
                               QTime(0, 0, 0).secsTo(ui->time_TimeAzul_TempoTimeout->time()));
}

void Interface::SLOT_switchToTab1()
{
    ui->tabWidget->setCurrentIndex(0);
}

void Interface::SLOT_switchToTab2()
{
    ui->tabWidget->setCurrentIndex(1);
}

void Interface::SLOT_logPlayerForward()
{
    if(bLogFileLoaded)
    {
        vSetLogPlayerStopped(true);

        const qint32 passoMaximo = iMaximumFrameLogFile/100;

        iCurrentFrameLogFile += passoMaximo;

        if(iCurrentFrameLogFile > iMaximumFrameLogFile)
        {
            iCurrentFrameLogFile = iMaximumFrameLogFile;
        }
        else
        {
            vSetLogPlayerStopped(false);
        }
    }
}

void Interface::SLOT_logPlayerBackwards()
{

    if(bLogFileLoaded)
    {
        vSetLogPlayerStopped(true);

        const qint32 passoMaximo = iMaximumFrameLogFile/100;

        iCurrentFrameLogFile -= passoMaximo;

        if(iCurrentFrameLogFile < 0)
        {
            iCurrentFrameLogFile = 0;
        }
        else
        {
            vSetLogPlayerStopped(false);
        }
    }

}

void Interface::SLOT_refreshStatusLoadLogFile(const QString& status)
{
    ui->l_statusLogFile->setText(status);
    qApp->processEvents();
}

void Interface::SLOT_setOutputDirectoryAnalises()
{
    QString path { QFileDialog::getExistingDirectory()};

    if(!path.isEmpty()){
        sPathToAnalysisFile = path;

        ui->le_OutputDirectoryAnalise->setText(sPathToAnalysisFile);
    }
}

void Interface::SLOT_setOutputDirectoryAnalisesReadOnly()
{
    if(ui->cb_OutputDirectoryAnalise->isChecked()){

        ui->le_OutputDirectoryAnalise->setReadOnly(true);

        sAnalysisFileName = ui->le_NomeDoArquivoAnalises->text();
    }

    else
        ui->le_OutputDirectoryAnalise->setReadOnly(false);
}

void Interface::SLOT_notifyLogFileEnded()
{

    tray->show();
    tray->showMessage("LogAnalyserRoboFEI-SSL", "Log File transmission is over");

    QTimer::singleShot(3000, this, [this](){ tray->hide(); });
}
