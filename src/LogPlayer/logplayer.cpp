/***************************************************************************
 *   Copyright (C) 2020-2021  RoboFEI / Small Size League                       *
 *   Copyright (c) 2013 Robotics Erlangen e.V.                             *
 *   http://www.robotics-erlangen.de/                                      *
 *   info@robotics-erlangen.de                                             *
 *                                                                         *
 *   This file may be licensed under the terms of the GNU General Public   *
 *   License Version 3, as published by the Free Software Foundation.      *
 *   You can find it here: http://www.gnu.org/licenses/gpl.html            *
 *                                                                         *
 ***************************************************************************/


#include "logplayer.h"

#include "protofiles/messages_robocup_ssl_wrapper_legacy.pb.h"
#include "protofiles/messages_robocup_ssl_wrapper.pb.h"


LogPlayer::LogPlayer() :
    referee(nullptr),
    vision(nullptr),
    legacyVision(nullptr)
{


}

void LogPlayer::setNetworkConfiguration(const QString& ipReferee,
                                        const quint16& portaReferee,
                                        const QString& ipVisao,
                                        const quint16& portaVisao,
                                        const QString& ipVisionLegacy,
                                        const quint16& portaVisionLegacy)
{
    // create referee socket
    referee.reset( new Network(QHostAddress(ipReferee), 0, portaReferee) );
    referee->connect();

    // create vision socket
    vision.reset( new Network(QHostAddress(ipVisao), 0, portaVisao) );
    vision->connect();

    // create legacy vision socket
    legacyVision.reset( new Network(QHostAddress(ipVisionLegacy), 0, portaVisionLegacy) );
    legacyVision->connect();

    network = true;
}

void LogPlayer::clearAllNetworkConfiguration()
{
    referee.reset();
    vision.reset();
    legacyVision.reset();

    network = false;
}

LogPlayer::~LogPlayer()
{
    stop();
}

bool LogPlayer::load(const QString& filename,
                     quint32& maxFrame,
                     double& duration,
                     QList<SSL_Referee_Command> commands)
{
    const bool compressed = QFileInfo(filename).suffix() == "gz";

    LogFile file(filename, compressed);

    if (!file.openRead())
    {
        return false;
    }

    packets.clear();

    bool record = false;

    quint16 count = 0; // Used to update status of loading on the interface


    forever
    { // and never, I don't want to close my eyes...

        QSharedPointer<Frame> packet ( new Frame() );

        if (!file.readMessage(packet->data, packet->time, packet->type))
        {
            // We have read everything already
            break;
        }

        if(packet->type == MESSAGE_BLANK)
        {
            // Aparently it always comes to the end of the file
            break;
        }

        // Parse the package
        if(packet->type == MESSAGE_SSL_REFBOX_2013)
        {
            SSL_Referee sslreferee;

            sslreferee.ParseFromArray(packet->data, packet->data.size());

            // Change this if you want to load only packages that are on some specific stage.
            // Similary to the commands
            if(commands.contains(sslreferee.command()) /*&&
               sslreferee.stage() == SSL_Referee_Stage_NORMAL_SECOND_HALF*/)
            {
                record = true;
                packets.append(packet);
            }
            else{

                record = false;
            }
        }

        else if(record)
        {
            packets.append(packet);
        }

        if (count >= 851)
        {
            // Update Status on Interface
            emit SIGNAL_refreshEventsInterface(QString(QString::number(packets.size()) + " frames loaded"));
            count = 0;
        }
        else
        {
            count++;
        }
    }

    if(packets.size() > 0)
    {
        maxFrame = packets.size() - 1;
        duration = packets.last()->time - packets.first()->time;
        return true;
    }

    return false;
}

bool LogPlayer::start(int position)
{  
    if (position > packets.size() - 1) {
        return false;
    }

    currentFrame = position;

    mayRun = true;
    QThread::start();

    return true;
}

void LogPlayer::stop()
{
    mayRun = false;
    elapsedTimer.reset();
    wait();
}

bool LogPlayer::good()
{
    return packets.size() > 0;
}

void LogPlayer::sendMessage(const Frame* const packet)
{

    if(packet->type == MESSAGE_BLANK)
    {
        // ignore
    }
    else if(packet->type == MESSAGE_UNKNOWN)
    {
        RoboCup2014Legacy::Wrapper::SSL_WrapperPacket legacyVisionPacket;
        SSL_WrapperPacket visionPacket;
        SSL_Referee refereePacket;

        // OK, let's try to figure this out by parsing the message
        if(refereePacket.ParseFromArray(packet->data.data(), packet->data.size()))
        {
            referee->writeData(packet->data);
        }
        else if(visionPacket.ParseFromArray(packet->data.data(), packet->data.size()))
        {
            vision->writeData(packet->data);
        }
        else if(legacyVisionPacket.ParseFromArray(packet->data.data(), packet->data.size()))
        {
            legacyVision->writeData(packet->data);
        }
        else
        {
            qCritical()<< "Error: unsupported or corrupt packet found in log file!";
        }

    }
    else if(packet->type == MESSAGE_SSL_VISION_2010)
    {
        legacyVision->writeData(packet->data);
    }
    else if(packet->type == MESSAGE_SSL_REFBOX_2013)
    {
        referee->writeData(packet->data);
    }
    else if(packet->type == MESSAGE_SSL_VISION_2014)
    {
        vision->writeData(packet->data);
    }
    else if(packet->type == MESSAGE_SSL_VISION_TRACKER)
    {
        // not implemented yet. Just ignore it for now
    }
    else
    {
        qCritical()<< "Error: unsupported or corrupt packet found in log file!";
    }
}

void LogPlayer::emitMessageLocal(const Frame* const packet)
{
    if(packet->type == MESSAGE_BLANK)
    {
        // ignore
    }
    else if(packet->type == MESSAGE_UNKNOWN)
    {
        RoboCup2014Legacy::Wrapper::SSL_WrapperPacket legacyVisionPacket;
        SSL_WrapperPacket visionPacket;
        SSL_Referee refereePacket;

        // OK, let's try to figure this out by parsing the message
        if(refereePacket.ParseFromArray(packet->data.data(), packet->data.size()))
        {
            emit SIGNAL_refereeMessageLocal(packet->data);
        }
        else if(visionPacket.ParseFromArray(packet->data.data(), packet->data.size()))
        {
            emit SIGNAL_visionMessageLocal(packet->data);
        }
        else if(legacyVisionPacket.ParseFromArray(packet->data.data(), packet->data.size()))
        {
//            emit SIGNAL_visionLegacyMessageLocal(packet->data);
        }
        else
        {
            qCritical() << "Error: unsupported or corrupt packet found in log file!";
        }
    }
    else if(packet->type == MESSAGE_SSL_VISION_2010)
    {
        //        emit SIGNAL_visionLegacyMessageLocal(packet->data);
    }
    else if(packet->type == MESSAGE_SSL_REFBOX_2013)
    {
         emit SIGNAL_refereeMessageLocal(packet->data);
    }
    else if(packet->type == MESSAGE_SSL_VISION_2014)
    {
        emit SIGNAL_visionMessageLocal(packet->data);
    }
    else if(packet->type == MESSAGE_SSL_VISION_TRACKER)
    {
        // not implemented yet. Just ignore it for now
    }
    else
    {
        qCritical() << "Error: unsupported or corrupt packet found in log file!";
    }

}

void LogPlayer::run()
{
    if(network)
        sendMessage(packets.at(currentFrame).get());

    else
        emitMessageLocal(packets.at(currentFrame).get());

    elapsedTimer.reset(new QElapsedTimer());
    elapsedTimer->restart();

    const qint64 startTime = elapsedTimer->nsecsElapsed();
    const qint64 referenceTime = packets.at(currentFrame)->time;

    qint64 jumpTime = 0;

    while (mayRun && ++currentFrame < packets.size() && this->isRunning())
    {
        const Frame*const packet = packets.at(currentFrame).get();

        qint64 sleepTime = ((packet->time - referenceTime) - (elapsedTimer->nsecsElapsed() - startTime) - jumpTime ) / 1000;

        if(sleepTime >1000000){ // if sleepTime > 1sec

            jumpTime  += sleepTime*1000; // jumpTime += sleepTime - 1sec

            qInfo()<<"Jump "<<sleepTime*1e-6<<" seconds";

            sleepTime = 1000000; // sleepTime time is reduced to 1sec

            emit SIGNAL_jump(sleepTime);

        }

        else if (sleepTime > 0) {
            QThread::currentThread()->usleep(sleepTime);
        }

        if(network)
            sendMessage(packet);

        else
            emitMessageLocal(packet);

        emit SIGNAL_positionChanged(currentFrame, packet->time - packets.first()->time); // update interface

    }

    if(currentFrame == packets.size())
        emit SIGNAL_finished();
}

