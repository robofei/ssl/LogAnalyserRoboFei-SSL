/***************************************************************************
 *   Copyright (C) 2020-2021  RoboFEI / Small Size League                       *
 *   Copyright (c) 2013 Robotics Erlangen e.V.                             *
 *   http://www.robotics-erlangen.de/                                      *
 *   info@robotics-erlangen.de                                             *
 *                                                                         *
 *   This file may be licensed under the terms of the GNU General Public   *
 *   License Version 3, as published by the Free Software Foundation.      *
 *   You can find it here: http://www.gnu.org/licenses/gpl.html            *
 *                                                                         *
 ***************************************************************************/


#ifndef LOGPLAYER_H
#define LOGPLAYER_H

#include "protofiles/referee.pb.h"

#include "LogFile/common/network.h"
#include "LogFile/common/logfile.h"


#include <iostream>

#include <QFileInfo>
#include <QThread>
#include <QElapsedTimer>

#include <QPointer>


struct Frame {
    qint64 time;
    MessageType type;
    QByteArray data;

    Frame(){
    }

    ~Frame(){
    }
};

class LogPlayer : public QThread
{
    Q_OBJECT

public:

public:
    LogPlayer();
    ~LogPlayer();

signals:
    void SIGNAL_positionChanged(quint32 frame, double time);
    void SIGNAL_finished();

    void SIGNAL_jump(qint64 sleepTime);

    void SIGNAL_refereeMessageLocal(const QByteArray&);
    void SIGNAL_visionMessageLocal(const QByteArray&);
    void SIGNAL_visionLegacyMessageLocal(const QByteArray&);
    // nothing would receive this signal

    void SIGNAL_refreshEventsInterface(const QString& status);

public:
    bool load(const QString& filename,
              quint32& maxFrame,
              double& duration,
              QList<SSL_Referee_Command> commands = QList<SSL_Referee_Command>());

    bool start(int position);
    void stop();
    bool good();

    void setNetworkConfiguration(const QString& ipReferee,
                                 const quint16& portaReferee,
                                 const QString& ipVisao,
                                 const quint16& portaVisao,
                                 const QString& ipVisionLegacy,
                                 const quint16& portaVisionLegacy);

    void clearAllNetworkConfiguration();

private:
    void run();
    void sendMessage(const Frame* const packet);
    void emitMessageLocal(const Frame* const packet);

    QScopedPointer<QElapsedTimer> elapsedTimer{nullptr};

    QList< QSharedPointer<Frame> > packets;
    QScopedPointer<Network> referee;
    QScopedPointer<Network> vision;
    QScopedPointer<Network> legacyVision;
    bool mayRun;
    qint32 currentFrame;
    bool network{false};
};

#endif // LOGPLAYER_H
