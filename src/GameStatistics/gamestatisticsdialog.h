/*
 * LogAnalyserRoboFEI-SSL
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef GAMESTATISTICSDIALOG_H
#define GAMESTATISTICSDIALOG_H

#include <QDialog>

#include "teamstatistics.h"

namespace Ui {
class GameStatisticsDialog;
}

class GameStatisticsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GameStatisticsDialog(QWidget *parent = nullptr);
    ~GameStatisticsDialog();

public slots:
    void SLOT_getStatisticsFromAnalysis(const TeamStatistics& yellow, const TeamStatistics& blue);
    void SLOT_getStatisticsFromReferee(const QString& yellowTeamName,
                                       const quint16 yellowGoals,
                                       const quint16 yellowYellowCards,
                                       const quint16 yellowRedCards,
                                       const quint16 yellowTimeoutLeft,
                                       const QString& blueTeamName,
                                       const quint16 blueGoals,
                                       const quint16 blueYellowCards,
                                       const quint16 blueRedCards,
                                       const quint16 blueTimeoutLeft);

private:
    Ui::GameStatisticsDialog *ui;
};

#endif // GAMESTATISTICSDIALOG_H
