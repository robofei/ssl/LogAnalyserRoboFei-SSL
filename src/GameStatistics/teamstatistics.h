/*
 * LogAnalyserRoboFEI-SSL
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef TEAMSTATISTICS_H
#define TEAMSTATISTICS_H

#include <QtGlobal>
#include <QMetaType>

struct TeamStatistics
{
    quint64 framesWithBal {1};
    quint16 shotsOnGoal {0};
    quint16 shotsOutOfTheGoal {0};
    quint16 correctPasses {0};
    quint16 wrongPasses {0};
    quint16 freeKicks {0};
};
Q_DECLARE_METATYPE(TeamStatistics);
#endif // TEAMSTATISTICS_H
