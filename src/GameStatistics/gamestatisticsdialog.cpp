/*
 * LogAnalyserRoboFEI-SSL
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "gamestatisticsdialog.h"
#include "ui_gamestatisticsdialog.h"

quint16 digitsOfNumber(quint16 number)
{
    if(number == 0)
        return 1;

    quint16 count = 0;

    while(number != 0)
    {
        number /= 10;
        ++count;
    }
    return count;
}

void setLcdNumber(QLCDNumber* lcd, const quint16 number)
{
    lcd->setDigitCount(digitsOfNumber(number));
    lcd->display(number);
}

GameStatisticsDialog::GameStatisticsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GameStatisticsDialog)
{
    ui->setupUi(this);
}

GameStatisticsDialog::~GameStatisticsDialog()
{
    delete ui;
}

void GameStatisticsDialog::SLOT_getStatisticsFromAnalysis(const TeamStatistics& yellow, const TeamStatistics& blue)
{
    // Ball Possession
    {
        const quint64 yellowBallPossession = yellow.framesWithBal * 100 / (blue.framesWithBal + yellow.framesWithBal);

        ui->progressBarBallPossession->setValue(yellowBallPossession);

        setLcdNumber(ui->lcdBallPossessionYellow, yellowBallPossession);
        setLcdNumber(ui->lcdBallPossessionBlue, 100 - yellowBallPossession);
    }

    // Shoots
    {
        const quint16 yellowShoots = yellow.shotsOnGoal + yellow.shotsOutOfTheGoal;
        const quint16 blueShoots = blue.shotsOnGoal + blue.shotsOutOfTheGoal;

        if(yellowShoots == 0 && blueShoots == 0)
        {
            ui->progressBarShoots->setValue(50);
        }
        else
        {
            ui->progressBarShoots->setValue(yellowShoots * 100/ (yellowShoots + blueShoots));
        }

        setLcdNumber(ui->lcdShootsYellow, yellowShoots);
        setLcdNumber(ui->lcdShootsBlue, blueShoots);

        const quint16 yellowShootsOnTarget = yellow.shotsOnGoal;
        const quint16 blueShootsOnTarget = blue.shotsOnGoal;

        if(yellowShootsOnTarget == 0 && blueShootsOnTarget == 0)
        {
            ui->progressBarShootsOnTarget->setValue(50);
        }
        else
        {
            ui->progressBarShootsOnTarget->setValue(yellowShootsOnTarget * 100/ (yellowShootsOnTarget + blueShootsOnTarget));
        }

        setLcdNumber(ui->lcdShootsOnTargetYellow, yellowShootsOnTarget);
        setLcdNumber(ui->lcdShootsOnTargetBlue, blueShootsOnTarget);

        const quint16 yellowShootPrecision = yellowShoots == 0 ? 0 : yellowShootsOnTarget * 100 / yellowShoots;
        const quint16 blueShootPrecision = blueShoots == 0 ? 0 : blueShootsOnTarget * 100 / blueShoots;

        if(yellowShootPrecision == 0 && blueShootPrecision == 0)
        {
            ui->progressBarShootPrecision->setValue(50);
        }
        else
        {
            ui->progressBarShootPrecision->setValue(yellowShootPrecision * 100/ (yellowShootPrecision + blueShootPrecision));
        }

        setLcdNumber(ui->lcdShootPrecisionYellow, yellowShootPrecision);
        setLcdNumber(ui->lcdShootPrecisionBlue, blueShootPrecision);
    }

    // Passes
    {
        const quint16 yellowCorrectPasses = yellow.correctPasses;
        const quint16 blueCorrectPasses = blue.correctPasses;

        if(yellowCorrectPasses == 0 && blueCorrectPasses == 0)
        {
            ui->progressBarCorrectPasses->setValue(50);
        }
        else
        {
            ui->progressBarCorrectPasses->setValue(yellowCorrectPasses * 100 / (yellowCorrectPasses + blueCorrectPasses));
        }

        setLcdNumber(ui->lcdCorrectPassesYellow, yellowCorrectPasses);
        setLcdNumber(ui->lcdCorrectPassesBlue, blueCorrectPasses);

        const quint16 yellowTotalPasses = yellowCorrectPasses + yellow.wrongPasses;
        const quint16 blueTotalPasses = blueCorrectPasses + blue.wrongPasses;

        const quint16 yellowPassPrecision = yellowTotalPasses == 0 ?
                                                0 : yellowCorrectPasses * 100 / yellowTotalPasses;
        const quint16 bluePassPrecision = blueTotalPasses == 0 ?
                                              0 : blueCorrectPasses * 100 / blueTotalPasses;

        if(yellowPassPrecision == 0 && bluePassPrecision == 0)
        {
            ui->progressBarPassPrecision->setValue(50);
        }
        else
        {
            ui->progressBarPassPrecision->setValue(yellowPassPrecision * 100/ (yellowPassPrecision + bluePassPrecision));
        }

        setLcdNumber(ui->lcdPassPrecisionYellow, yellowPassPrecision);
        setLcdNumber(ui->lcdPassPrecisionBlue, bluePassPrecision);
    }

    // Free kicks
    {
        if(yellow.freeKicks == 0 && blue.freeKicks == 0)
        {
            ui->progressBarFreeKicks->setValue(50);
        }
        else
        {
            ui->progressBarFreeKicks->setValue(yellow.freeKicks * 100 / (yellow.freeKicks + blue.freeKicks));
        }

        setLcdNumber(ui->lcdFreeKicksYellow, yellow.freeKicks);
        setLcdNumber(ui->lcdFreeKicksBlue, blue.freeKicks);
    }
}

void GameStatisticsDialog::SLOT_getStatisticsFromReferee(const QString& yellowTeamName,
                                                         const quint16 yellowGoals,
                                                         const quint16 yellowYellowCards,
                                                         const quint16 yellowRedCards,
                                                         const quint16 yellowTimeoutLeft,
                                                         const QString& blueTeamName,
                                                         const quint16 blueGoals,
                                                         const quint16 blueYellowCards,
                                                         const quint16 blueRedCards,
                                                         const quint16 blueTimeoutLeft)
{
    // Team name and goals
    ui->labelYellowTeam->setText(yellowTeamName);
    setLcdNumber(ui->lcdGoalsYellow, yellowGoals);

    ui->labelBlueTeam->setText(blueTeamName);
    setLcdNumber(ui->lcdGoalsBlue, blueGoals);

    // Yellow Cards
    if(yellowYellowCards == 0 && blueYellowCards == 0)
    {
        ui->progressBarYellowCards->setValue(50);
    }
    else
    {
        ui->progressBarYellowCards->setValue( yellowYellowCards * 100 / (yellowYellowCards + blueYellowCards));
    }

    setLcdNumber(ui->lcdYellowCardsYellow, yellowYellowCards);
    setLcdNumber(ui->lcdYellowCardsBlue, blueYellowCards);

    // Red Cards
    if(yellowRedCards == 0 && blueRedCards == 0)
    {
        ui->progressBarRedCards->setValue(50);
    }
    else
    {
        ui->progressBarRedCards->setValue( yellowRedCards * 100 / (yellowRedCards + blueRedCards));
    }

    setLcdNumber(ui->lcdRedCardsYellow, yellowRedCards);
    setLcdNumber(ui->lcdRedCardsBlue, blueRedCards);

    // Timeout left
    if(yellowTimeoutLeft == 0 && blueTimeoutLeft == 0)
    {
        ui->progressBarTimeoutLeft->setValue(50);
    }
    else
    {
        ui->progressBarTimeoutLeft->setValue( yellowTimeoutLeft * 100 / (yellowTimeoutLeft + blueTimeoutLeft));
    }

    setLcdNumber(ui->lcdTimeoutLeftYellow, yellowTimeoutLeft);
    setLcdNumber(ui->lcdTimeoutLeftBlue, blueTimeoutLeft);

}
