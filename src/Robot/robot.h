/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef ROBOT_H
#define ROBOT_H

#include "Atributtes/attributes.h"
#include <QDebug>


/**
 * @brief
 *
 */
class Robot
{

private:

    Attributes atributos; /**< TODO: describe */

public:


    /**
     * @brief
     *
     * @param _robotID
     */
    Robot(quint8 _robotID = 0);
    /**
     * @brief
     *
     */
    ~Robot();

    /**
     * @brief
     *
     * @param _atributos
     */
    void vUpdateAttributes(const Attributes &_atributos);//Atribui TODOS os atributos do robo

    /**
     * @brief
     *
     * @param _atributos
     */
    void vUpdateVisionAttributes(const Attributes& _atributos);//Aribui os dados alterados pela visao e etc (usado na estrategia)



    /**
     * @brief
     *
     * @return Atributos
     */
    const Attributes& getAttributes() const;

    /**
     * @brief
     *
     * @param _role
     */



    /**
     * @brief vSetaDadosKalman
     *
     * @param kalman
     */
    void vKalmanUpdate(const Attributes& kalman);


    /**
     * @brief
     *
     * @return QVector2D
     */
    QVector2D vt2dGetCurrentPosition() const;



    /**
     * @brief
     *
     * @return QVector3D
     */
    QVector3D vt3dGetVelocity() const; ///< : Retorna a velocidade Vx, Vy e W do robo


    /**
     * @brief bRoboEmCampo
     *
     * @return
     */
    bool isOnField() const;

    /**
     * @brief fAnguloRobo
     * @return
     */
    float getAngle() const;

    /**
     * @brief iID
     * @return
     */
    quint8 iID() const;

    void vSetPosition(QVector2D pos);

    void vResetFrameNumber();

    QVector2D vt2dPontoMiraRobo() const;

};






#endif // ROBOT_H
