/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "robot.h"

Robot::Robot(quint8 _robotID )
{
    atributos.id = _robotID;
    atributos.bOnField  = false;

    atributos.dTempoRobo  = 0;
    atributos.dVelocity = 0;



    atributos.vt2dCurrentPosition = QVector2D(99999999, 99999999);
    atributos.vt2dPreviousPosition = QVector2D(0, 0);
    atributos.vt3dVeloxity = QVector3D(0,0,0);


    for(unsigned int & i : atributos.iCameraFrame)
        i = 0;
}

Robot::~Robot()
= default;


void Robot::vUpdateAttributes(const Attributes &_atributos)
{
    this->atributos = _atributos;
}

void Robot::vUpdateVisionAttributes(const Attributes &_atributos)
{
    this->atributos.vt2dCurrentPosition    = _atributos.vt2dCurrentPosition;
    this->atributos.vt2dPreviousPosition = _atributos.vt2dPreviousPosition;
    this->atributos.rotation            = _atributos.rotation;
    this->atributos.bOnField            = _atributos.bOnField;
    this->atributos.iCameraRobo         = _atributos.iCameraRobo;
    this->atributos.dVelocity         = _atributos.dVelocity;
    this->atributos.vt3dVeloxity      = _atributos.vt3dVeloxity;
    this->atributos.dTempoRobo          = _atributos.dTempoRobo;
    //this->atributos.vt2dVelocidadeNaoConvertida = _atributos.vt2dVelocidadeNaoConvertida;

    for(unsigned int n = 0; n < NUMBER_OF_CAMERAS; ++n)
        this->atributos.iCameraFrame[n] = _atributos.iCameraFrame[n];
}


const Attributes& Robot::getAttributes() const
{
    return atributos;
}


void Robot::vKalmanUpdate(const Attributes& kalman)
{
    atributos.vt2dCurrentPosition = kalman.vt2dCurrentPosition;
    atributos.vt2dPreviousPosition = kalman.vt2dPreviousPosition;
    atributos.dVelocity = kalman.dVelocity;
    atributos.vt3dVeloxity = kalman.vt3dVeloxity;
}


bool Robot::isOnField()const
{
    return atributos.bOnField;
}


QVector2D Robot::vt2dGetCurrentPosition()const
{
    return atributos.vt2dCurrentPosition;
}


QVector3D Robot::vt3dGetVelocity()const
{
    return atributos.vt3dVeloxity;
}



float Robot::getAngle()const
{
    return atributos.rotation;
}

quint8 Robot::iID()const
{
    return atributos.id;
}

void Robot::vResetFrameNumber()
{
    for(unsigned int & i : atributos.iCameraFrame)
        i = 0;
}

void Robot::vSetPosition(QVector2D pos){
    atributos.vt2dCurrentPosition = pos;
}

QVector2D Robot::vt2dPontoMiraRobo()const
{
    float fAnguloRobo = atributos.rotation;

    return  (vt2dGetCurrentPosition()+ 100*QVector2D(qCos(fAnguloRobo), qSin(fAnguloRobo)));

}

