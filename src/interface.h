/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef INTERFACE_H
#define INTERFACE_H

#include "Vision/vision.h"
#include "Environment/environment.h"
#include "DrawMap/drawmap.h"
#include "Analysis/analysis.h"
#include "LogPlayer/logplayer.h"
#include "MirrorLogFiles/mirrorlogfiles.h"
#include "GameStatistics/gamestatisticsdialog.h"

#include <QSystemTrayIcon>
#include <QDialog>
#include <QtNetwork/QNetworkInterface>
#include <QUdpSocket>
#include <QProcess>
#include <QTextDocument>
#include <QTimer>
#include <QMainWindow>
#include <QShortcut>
#include <QStyleFactory>


namespace Ui {
class Interface;
}

class Interface : public QDialog
{
    Q_OBJECT

public:
    explicit Interface(QDialog *parent = nullptr);
    ~Interface();

    void vMessageOutput(const QtMsgType type,
                        const QMessageLogContext& context,
                        const QString& msg);

protected:
    void closeEvent(QCloseEvent *event = nullptr);
    void reject();

private:
    Ui::Interface *ui;

    QScopedPointer<QUdpSocket> udpRefbox {nullptr};
    QScopedPointer<Vision> vision {nullptr};
    RoboFeiController roboController ;
    QScopedPointer<QImage> fieldImage {nullptr};
    Environment *environmentInterface{nullptr};
    QScopedPointer<DrawMap> drawMapField{nullptr};

    QScopedPointer<Analysis> analysisEnvironment{nullptr};

    QScopedPointer<QTimer> tmrUpdateDrawing {nullptr};
    QScopedPointer<QTimer> tmrUpdateInterfaceInformation{nullptr};

    QScopedPointer<MirrorLogFiles> mirror{nullptr};

    qint32 iCurrentFrameLogFile{0};
    qint32 iMaximumFrameLogFile{0};

    bool bInicializaInterface{false};

    bool logPlayerStopped{false};
    LogPlayer *logPlayer{nullptr};

    bool bNetworkConfiguration{false};
    bool bLogFileLoaded{false};

    QProcess *process{nullptr};

    QString sPathToOutputDirectory{"$HOME/Download"};

    QThread *threadVision{nullptr};
    QThread *threadAnalysis{nullptr};

    Jogada jogadaAtualAnalise{};
    Jogada jogadaParalelaAnalise{};
    bool bConsiderarJogadaSecundariaAnalise{false};
    ESTADOS_DE_JOGO estadoDeJogoAnalise{JOGADA_NENHUMA};

    QString sPathToAnalysisFile{"$HOME"};
    QString sAnalysisFileName{"testes"};

    QScopedPointer<GameStatisticsDialog> statisticsDialog{nullptr};

    QSystemTrayIcon* tray;

    void vDrawField();
    void vDesenharCampos();
    void vDesenharMiraBola();

    void vDesenharPredicoesKalman();
    void vDesenhaPosicaoDesignada();
    void vDesenhaDestacaReceptor();
    void vDesenhaVetorDaBola();
    void vDesenhaRoboComABola();

    void vInicializaDesenhos();

    void vSetLogPlayerStopped(bool p);
    void vLoadFile(const QString& filename);

    void vEnableAnalysis();

    void vDisableAnalysis();

    void vStartLocalPlay();

    void vFinishLocalPlay();

    QString sEstadoDeJogoAtual()const;

    void configureDarkStyle();


private slots:

    void SLOT_connectNetwork();
    void SLOT_disconnectNetwork();
    void SLOT_ChegouGeometria();
    void SLOT_RecebeDadosVisao(const Environment& ambiente,
                               bool mudouGeometria);
    void SLOT_ProcessaReferee();
    void SLOT_AtualizaDesenho();

    void SLOT_openLogFile();
    void SLOT_toggleLogPlayerStopped();
    void SLOT_stopLogPlayer();
    void SLOT_restartLogPlayer();

    void SLOT_updateLogPlayerCurrentFrame(quint32 frame, double time);
    void SLOT_setNetworkConfiguration();

    void SLOT_getTerminalOutput();

    void SLOT_downloadMirrorTigers();

    void SLOT_downloadMirrorJackets();

    void SLOT_downloadLinks(const QList<QString>& listLinks);

    void SLOT_finishedDownloadMirror();

    void SLOT_setDownloadOutputDirectory();

    void SLOT_setDownloadOutputDirectoryReadOnly();

    void SLOT_mirrorInterfaceClose();

    void SLOT_deleteMirror();

    void SLOT_deleteProcess();

    void SLOT_jogadaAtualAnalise(Jogada jogadaAnalise);

    void SLOT_jogadaParalelaAnalise(Jogada jogadaAnalise);

    void SLOT_setarJogadaEmAndamentoAnalise(bool habilitar);

    void SLOT_estadoDeJogoAtualAnalise(ESTADOS_DE_JOGO _estadoDeJogoAnalise);

    void on_rb_TransmitirRemotamente_clicked();

    void on_rb_ReproduzirEmTempoReal_clicked();

    void on_pb_AtivarAnalises_clicked();

    void SLOT_processLocalReferee(const QByteArray& data);

    void SLOT_switchToTab1();
    void SLOT_switchToTab2();

    void SLOT_logPlayerForward();
    void SLOT_logPlayerBackwards();

    void SLOT_refreshStatusLoadLogFile(const QString& status);

    void SLOT_setOutputDirectoryAnalises();
    void SLOT_setOutputDirectoryAnalisesReadOnly();

    void SLOT_notifyLogFileEnded();

signals:
    void SIGNAL_networkConfigurationVision(QString ipVisao,
                                           int portaVisao,
                                           int interfaceIndice);
    void SIGNAL_disconnectVision();

    void SIGNAL_deleteMirror();

    void SIGNAL_deleteProcess();

    void SIGNAL_setAnalysis(bool);

    void SIGNAL_refereeInformation(const int,
                                   const quint8 ,
                                   const quint8 ,
                                   const qint8 iLadoCampoTimeAzul);

    void SIGNAL_setLocalVisionEnabled(bool);

    void SIGNAL_statisticsData(const QString& yellowTeamName,
                               const quint16 yellowGoals,
                               const quint16 yellowYellowCards,
                               const quint16 yellowRedCards,
                               const quint16 yellowTimeoutLeft,
                               const QString& blueTeamName,
                               const quint16 blueGoals,
                               const quint16 blueYellowCards,
                               const quint16 blueRedCards,
                               const quint16 blueTimeoutLeft);


};

#endif // INTERFACE_H
