/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "vision.h"

Vision::Vision()
{
    environment = new Environment();
    bMudouGeometria = false;


    kalmanBall = KalmanFilter();

    for(int n=0; n < PLAYERS_PER_SIDE; ++n)
    {
        kalmanBlue[n] = KalmanFilter();
        kalmanYellow[n] = KalmanFilter();
    }

}

Vision::~Vision()
{
    delete environment;
}

void Vision::vHandleWithVisionPackage(const SSL_WrapperPacket& _pkgUltimoPacoteRecebido)
{
    if(_pkgUltimoPacoteRecebido.ByteSize() > 0)
    {
        //Atualiza as informacoes dos robos e da bola
        //Aqui sera detectado se o robo saiu da visao ou entao, caso ele esteja no campo sera adicionada sua posicao atual
        //no vetor utilizado pelo kalman para calcular sua posicao real, o mesmo vale para a bola.
        if(_pkgUltimoPacoteRecebido.has_detection())
        {
            const SSL_DetectionFrame &frameDeteccao = _pkgUltimoPacoteRecebido.detection();
            vDetectObjectsOutOfTheField(frameDeteccao);
            vDetectObjectsOnTheField(frameDeteccao);
        }

        //Recebe os dados de geometria do campo
        if(_pkgUltimoPacoteRecebido.has_geometry())
        {
            const SSL_GeometryData &frameGeometria = _pkgUltimoPacoteRecebido.geometry();
            vUpdateFieldSize(frameGeometria);
        }
    }

    if(!tmrKalman->isActive())
    {
        tmrKalman->start(TIME_LOOP_KALMAN_FILTER);
    }
}

void Vision::vDetectObjectsOutOfTheField(const SSL_DetectionFrame& frameDeteccao)
{
    Attributes atbAtributosRobo;

    Ball& blBola = environment->getBall();
    //Se o objeto esta na mesma camera e ja se passou o numero de frames para sair da visao, consideramos que ele esta fora de campo

    if(frameDeteccao.camera_id() == blBola.iCameraBola &&
       frameDeteccao.frame_number() > (blBola.iCameraFrame[blBola.iCameraBola] + FRAMES_LOST_TO_GET_OUT_OF_THE_FIELD))
    {
        blBola.bEmCampo = false;
    }
    else if(frameDeteccao.camera_id() == blBola.iCameraBola &&
            frameDeteccao.frame_number()  - blBola.iCameraFrame[blBola.iCameraBola]  > static_cast<unsigned int>(FRAMES_LOST_TO_GET_OUT_OF_THE_FIELD))
    {
        environment->vBallResetFrame();
    }


    for(int n=0; n < PLAYERS_PER_SIDE; ++n)
    {
        atbAtributosRobo = environment->rbtBlue[n].getAttributes();


        if(frameDeteccao.camera_id() == atbAtributosRobo.iCameraRobo &&
           abs(static_cast<int>(frameDeteccao.frame_number()) - static_cast<int>(atbAtributosRobo.iCameraFrame[blBola.iCameraBola])) > FRAMES_LOST_TO_GET_OUT_OF_THE_FIELD)
        {
            atbAtributosRobo.bOnField = false;

            environment->rbtBlue[n].vUpdateAttributes(atbAtributosRobo);
        }

        atbAtributosRobo = environment->rbtYellow[n].getAttributes();

        if(frameDeteccao.camera_id() == atbAtributosRobo.iCameraRobo &&
           abs(static_cast<int>(frameDeteccao.frame_number()) - static_cast<int>(atbAtributosRobo.iCameraFrame[blBola.iCameraBola])) > FRAMES_LOST_TO_GET_OUT_OF_THE_FIELD)
        {
            atbAtributosRobo.bOnField = false;

            environment->rbtYellow[n].vUpdateAttributes(atbAtributosRobo);
        }
    }
}


void Vision::vDetectObjectsOnTheField(const SSL_DetectionFrame& frameDeteccao)
{
    //Atualiza as informacoes da bola
    if(frameDeteccao.balls_size() > 0)
    {
        for(int n=0; n < frameDeteccao.balls_size(); ++n)
        {
            vUpdateBall(frameDeteccao, n);
        }
    }
    environment->vIncreaseBallTime(1.0/62.5);

    for(int n=0; n < frameDeteccao.robots_blue_size(); ++n)
    {
        vUpdateBlueRobot(frameDeteccao, n);
    }

    for(int n=0; n < frameDeteccao.robots_yellow_size(); ++n)
    {
        vUpdateYellowRobot(frameDeteccao, n);
    }

    //Conta quantos robos Azuis tem em campo
    int nAux = 0;
    for(int n=0; n < PLAYERS_PER_SIDE; ++n)
    {
        if(environment->rbtBlue[n].getAttributes().bOnField == true)
            nAux++;
    }
}


void Vision::vUpdateBall(const SSL_DetectionFrame& frameDeteccao, int nBola)
{
    const SSL_DetectionBall& bola = frameDeteccao.balls(nBola);
    Ball& blBola = environment->getBall();

    //Atualiza se a bola esta no campo e o seu frame number e salva a posicao recebida no vetor que sera usado no kalman

    if(blBola.vt2dCurrentPosition.distanceToPoint(QVector2D(bola.x(), bola.y())) < 700 ||
       !blBola.bEmCampo)
    {
        //            if(bola.confidence() > 0.9 && !qIsNaN(bola.confidence()))
        //            {

        //            }
        blBola.bEmCampo = true;
        //Buffer da bola
        environment->vtBolaVisao.prepend(QVector2D(bola.x(), bola.y()));
        blBola.iCameraBola = frameDeteccao.camera_id();
        blBola.iCameraFrame[frameDeteccao.camera_id()] = frameDeteccao.frame_number();

    }

    if(environment->vtBolaVisao.size() > KALMAN_BUFFER_SIZE)
    {
        environment->vtBolaVisao.pop_back();
    }
}

void Vision::vUpdateBlueRobot(const SSL_DetectionFrame& frameDeteccao, int nRobo)
{
    {
        const SSL_DetectionRobot& roboAzul = frameDeteccao.robots_blue(nRobo);
        if(roboAzul.robot_id() >= PLAYERS_PER_SIDE)
            return;

        {
            Attributes atbAtributosRobo = environment->rbtBlue[roboAzul.robot_id()].getAttributes();

            if(atbAtributosRobo.vt2dCurrentPosition.distanceToPoint(QVector2D(roboAzul.x(), roboAzul.y())) <= 140 ||
               !atbAtributosRobo.bOnField)
            {

                atbAtributosRobo.rotation = roboAzul.orientation();

                atbAtributosRobo.bOnField = true;

                atbAtributosRobo.dTempoRobo += 1/62.5;
                if(atbAtributosRobo.dTempoRobo > 9.6)
                {
                    atbAtributosRobo.dTempoRobo = 0;
                }

                atbAtributosRobo.iCameraRobo = frameDeteccao.camera_id();
                atbAtributosRobo.iCameraFrame[frameDeteccao.camera_id()] = frameDeteccao.frame_number();

                //Buffer utilizado no calculo do kalman
                environment->vtRobosAzuisVisao[atbAtributosRobo.id].prepend(QVector2D(roboAzul.x(), roboAzul.y()));

                if(environment->vtRobosAzuisVisao[atbAtributosRobo.id].size() > 50)
                    environment->vtRobosAzuisVisao[atbAtributosRobo.id].pop_back();
            }

            environment->rbtBlue[atbAtributosRobo.id].vUpdateAttributes(atbAtributosRobo);

        }

    }
}

void Vision::vUpdateYellowRobot(const SSL_DetectionFrame& frameDeteccao, int nRobo)
{
    const SSL_DetectionRobot& roboAmarelo = frameDeteccao.robots_yellow(nRobo);
    if(roboAmarelo.robot_id() >= PLAYERS_PER_SIDE)
        return;

    Attributes atbAtributosRobo = environment->rbtYellow[roboAmarelo.robot_id()].getAttributes();

    if(atbAtributosRobo.vt2dCurrentPosition.distanceToPoint(QVector2D(roboAmarelo.x(), roboAmarelo.y())) <= 140 ||
       !atbAtributosRobo.bOnField)
    {
        atbAtributosRobo.iCameraRobo = frameDeteccao.camera_id();
        atbAtributosRobo.iCameraFrame[frameDeteccao.camera_id()] = frameDeteccao.frame_number();

        atbAtributosRobo.rotation = roboAmarelo.orientation();
        atbAtributosRobo.bOnField = true;

        atbAtributosRobo.dTempoRobo += 1/62.5;
        if(atbAtributosRobo.dTempoRobo > 9.6)
        {
            atbAtributosRobo.dTempoRobo = 0;
        }


        //Buffer utilizado no calculo do kalman
        environment->vtRobosAmarelosVisao[atbAtributosRobo.id].prepend(QVector2D(roboAmarelo.x(), roboAmarelo.y()));

        if(environment->vtRobosAmarelosVisao[atbAtributosRobo.id].size() > 50)
            environment->vtRobosAmarelosVisao[atbAtributosRobo.id].pop_back();
    }

    environment->rbtYellow[atbAtributosRobo.id].vUpdateAttributes(atbAtributosRobo);

}

void Vision::vUpdateFieldSize(const SSL_GeometryData& frameGeometria)
{
    const SSL_GeometryFieldSize& geometriaCampo = frameGeometria.field();
    const google::protobuf::RepeatedPtrField<SSL_FieldCicularArc>& arcosCampo = geometriaCampo.field_arcs();
    const google::protobuf::RepeatedPtrField<SSL_FieldLineSegment>& retasCampo = geometriaCampo.field_lines();

    QVector<SSL_FieldCicularArc> circulosCampo(arcosCampo.size());
    QVector<SSL_FieldLineSegment> linhasCampo(retasCampo.size());

    //Recebe os circulos do campo
    for(int n=0; n < arcosCampo.size(); ++n)
    {
        circulosCampo[n] = arcosCampo.Get(n);

        if(circulosCampo[n].name() == "CenterCircle")
            environment->vSetCenterRadius(circulosCampo[n].radius());
    }

    //Recebe linhas do campo
#if LOG_ANALYSER_LOG_VERSION <= 2017
    environment->vSetDefenseAreaWidth(2500);
    environment->vSetDefenseAreaHeight(1000);
    // Solution for the log files from 2017, when the defense area had a different format
    //  https://github.com/RoboCup-SSL/ssl-rules-legacy/releases/download/robocup2017/2017_ssl-rules.pdf
#else
    for(int n=0; n < retasCampo.size(); ++n)
    {
        linhasCampo[n] = retasCampo.Get(n);
        if(linhasCampo[n].name() == "LeftPenaltyStretch" || linhasCampo[n].name() == "RightPenaltyStretch")
            environment->vSetDefenseAreaWidth( QVector2D(linhasCampo[n].p1().x(), linhasCampo[n].p1().y()).distanceToPoint(
                                                 QVector2D(linhasCampo[n].p2().x(), linhasCampo[n].p2().y())) );

        else if(linhasCampo[n].name() == "LeftFieldLeftPenaltyStretch" || linhasCampo[n].name() == "LeftFieldRightPenaltyStretch" ||
                linhasCampo[n].name() == "RightFieldRightPenaltyStretch" || linhasCampo[n].name() == "RightFieldLeftPenaltyStretch" )
            environment->vSetDefenseAreaHeight( QVector2D(linhasCampo[n].p1().x(), linhasCampo[n].p1().y()).distanceToPoint(
                                                      QVector2D(linhasCampo[n].p2().x(), linhasCampo[n].p2().y())) );

        //Dimensoes da area do penalty
    }
#endif

    //Dimensoes do gol
    environment->vSetGoalWidth(geometriaCampo.goal_width());
    environment->vSetGoalDepth(geometriaCampo.goal_depth());

    //Atualiza o tamanho do campo
    if(environment->getFieldSize() != QVector2D(geometriaCampo.field_length(), geometriaCampo.field_width()))
    {
        bMudouGeometria = true;
        environment->setFieldSize(QVector2D(geometriaCampo.field_length(), geometriaCampo.field_width()));
    }

    if(bMudouGeometria == true)
    {
        environment->vBallResetFrame();

        for(int n=0; n < PLAYERS_PER_SIDE; ++n)
        {
            environment->rbtBlue[n].vResetFrameNumber();
            environment->rbtYellow[n].vResetFrameNumber();
        }
    }
}

void Vision::vKalmanRobots(KalmanFilter &kalmanRobos, Robot &rbtRobo, QVector<QVector2D> &vtBufferPosVisao, QVector<QVector2D> &vtBufferPosKalman, QVector<QVector2D> *vtPredicao)
{
    Attributes atbRobo;
    QVector2D vt2dPosicaoKalmanRobos,vt2dVar;

    atbRobo = rbtRobo.getAttributes();
    if(vtBufferPosVisao.size() > 2 && atbRobo.bOnField)
    {
        vt2dPosicaoKalmanRobos = vtBufferPosVisao.constFirst();
        vt2dVar = vt2dCalculaVariancia(vtBufferPosVisao);

        vt2dVar.setX(qPow(vt2dVar.x(),2));
        vt2dVar.setY(qPow(vt2dVar.y(),2));

        kalmanRobos.vAtualizaVariancia(0.01,0.01);//vt2dVar.x(),vt2dVar.y()); comentado p/ testes

        // Reset do filtro quando a bola bater em alguma coisa

        if( qAbs(vtBufferPosVisao.at(0).y() - vtBufferPosVisao.at(1).y()) < 2 &&
            qAbs(vtBufferPosVisao.at(0).x() - vtBufferPosVisao.at(1).x()) < 2 )
            kalmanRobos.dAngAtual = 0.0;
        else
            kalmanRobos.dAngAtual = qAtan( (vtBufferPosVisao.at(0).y() - vtBufferPosVisao.at(1).y())/
                                           (vtBufferPosVisao.at(0).x() - vtBufferPosVisao.at(1).x()) );

        if( qAbs(qAbs(kalmanRobos.dAngAtual) - qAbs(kalmanRobos.dAngAnterior)) > 0.52f &&
            qAbs((vtBufferPosVisao.at(0) - vtBufferPosVisao.at(1)).length()) > 15.0f )
            kalmanRobos.vResetKalman(vt2dPosicaoKalmanRobos, atbRobo.id);


        // Reset do filtro quando a bola bater em alguma coisa

        //Velocidade do robô em m/s

        kalmanRobos.vAtualizaMatrizes(21e-3);
        kalmanRobos.vPredizKalman();
        kalmanRobos.vAtualizaKalman(vt2dPosicaoKalmanRobos);

        kalmanRobos.xPred = kalmanRobos.x;
        kalmanRobos.pPred = kalmanRobos.P;

        vt2dPosicaoKalmanRobos = QVector2D(kalmanRobos.x(0,0),kalmanRobos.x(1,0));

        if(vt2dPosicaoKalmanRobos.length() < 10e6 && qIsFinite(vt2dPosicaoKalmanRobos.length()))
        {
            rbtRobo.vSetPosition(vt2dPosicaoKalmanRobos);

            atbRobo = rbtRobo.getAttributes();

            if(vtBufferPosKalman.size() == 50)
            {
                double distance = 0;
                auto initPos = vtBufferPosKalman.constFirst();
                foreach (auto pos, vtBufferPosKalman)
                {
                    distance += initPos.distanceToPoint(pos);
                    initPos = pos;
                }
                atbRobo.dVelocity = distance/(2*50*5);
            }

            rbtRobo.vUpdateAttributes(atbRobo);

        }
        kalmanRobos.dAngAnterior = kalmanRobos.dAngAtual;

        // -------------------- prediçao
        //        if(vtPredicao != nullptr)
        //        {
        //            QVector2D vt2dPontoFuturo = vtBufferPosVisao.constFirst();
        //            vt2dPontoFuturo = kalmanRobos.vt2dPredizFuturo(vtBufferPosVisao,
        //                                                           kalmanRobos.xPred,kalmanRobos.pPred,1/62.5, 15);

        //            vtPredicao->prepend(vt2dPontoFuturo);
        //            if(vtPredicao->size() > 4)
        //                vtPredicao->pop_back();
        //        }
        // --------------------

        vtBufferPosKalman.prepend(vt2dPosicaoKalmanRobos);

        if(vtBufferPosKalman.size() > 50)
            vtBufferPosKalman.pop_back();
    }
}

void Vision::vKalmanBall(bool _bEstadoBola)
{
    QVector2D vt2dKalmanBola;

    //Atualiza a posicao da bola com o filtro de kalman
    if(environment->vtBolaVisao.size() >= 2)
    {
        // ------------- RESET
        if( qAbs(environment->vtBolaVisao.at(0).y() - environment->vtBolaVisao.at(1).y()) < 2 &&
            qAbs(environment->vtBolaVisao.at(0).x() - environment->vtBolaVisao.at(1).x()) < 2 )
            kalmanBall.dAngAtual = 0.0;
        else
            kalmanBall.dAngAtual = qAtan( (environment->vtBolaVisao.at(0).y() - environment->vtBolaVisao.at(1).y())/
                                          (environment->vtBolaVisao.at(0).x() - environment->vtBolaVisao.at(1).x()) );

        if( qAbs(qAbs(kalmanBall.dAngAtual) - qAbs(kalmanBall.dAngAnterior)) > 0.26f &&
            qAbs((environment->vtBolaVisao.at(0) - environment->vtBolaVisao.at(1)).length()) > 5.0f )
        {
            kalmanBall.vResetKalman(environment->vtBolaVisao.first());
        }
        // ------------- RESET

        // ------------- CALCULO VELOCIDADE
        //        double dVel;
        //        vt2dkb.prepend( QVector2D(kalmanBola.x(0),kalmanBola.x(1)) );

        //        if(vt2dkb.size() > 1)
        //        {
        //            if(kalmanBola.dAngAtual == 0.0 || (acVisao->vtBufferPosBolaVisao.at(0) - acVisao->vtBufferPosBolaVisao.at(1)).length() <= 10 )
        //                dVel = 0.0;
        //            else
        //                dVel = (QVector2D(acVisao->vtBufferPosBolaVisao.at(0) - acVisao->vtBufferPosBolaVisao.at(1))*62.5).length();

        //            dVelTeste = (dVelocidadeBola(dVel))/1e3;
        //        }

        //        if(vt2dkb.size() > 2)
        //            vt2dkb.pop_back();
        // ------------- CALCULO VELOCIDADE

        kalmanBall.vAtualizaVariancia(0.1,0.1);

        if(_bEstadoBola == false)
        {
            kalmanBall.vAtualizaMatrizes(21e-3);
            kalmanBall.vPredizKalman();
            kalmanBall.x = kalmanBall.x_k_1;
            kalmanBall.P = kalmanBall.P_k_1;
        }
        else
        {
            kalmanBall.vAtualizaMatrizes(21e-3/*1/62.5*/);
            kalmanBall.vPredizKalman();
            kalmanBall.vAtualizaKalman(environment->vtBolaVisao.first());
        }

        kalmanBall.xPred = kalmanBall.x;
        kalmanBall.pPred = kalmanBall.P;

        vt2dKalmanBola = QVector2D(kalmanBall.x(0),kalmanBall.x(1));

        // -------------------- prediçao
        QVector2D vt2dPontoFuturo = kalmanBall.vt2dPredizFuturo(environment->vtBolaKALMAN,
                                                                kalmanBall.xPred,kalmanBall.pPred,1/62.5, 12);

        environment->vtPredicaoBola.prepend(vt2dPontoFuturo);


        if(environment->vtPredicaoBola.size() > KALMAN_BUFFER_SIZE)
            environment->vtPredicaoBola.removeLast();
        // --------------------

        environment->vSetBallPosition(vt2dKalmanBola);//Esta funcao ja atribui a posicao atual na anterior e a atual na atual
        // Calcula a velocidade da bola se existem pelo menos duas posiçoes
        if(!environment->vtBolaKALMAN.isEmpty())
        {
            // A
            const double dTempo = TIME_LOOP_KALMAN_FILTER*(environment->vtBolaKALMAN.size() - 1);
            QVector2D vt2dDeslocamentosSomados = QVector2D(0,0) ;

            for (quint16 i=0; i<environment->vtBolaKALMAN.size()-1; i++){

                vt2dDeslocamentosSomados += (environment->vtBolaKALMAN.at(i) - environment->vtBolaKALMAN.at(i+1));

            }

            QVector2D vt2dVelocidadeBola = vt2dDeslocamentosSomados/(dTempo);


            if(iContadorReiniciaVelocidade > 0){

                vt2dVelocidadeBola=QVector2D(0,0);
                iContadorReiniciaVelocidade--;
            }

            environment->vSetBallVelocity( vt2dVelocidadeBola );
            environment->vSetBallVelocity( vt2dVelocidadeBola.length() );

            //            acVisao->vSetaVelocidadeBola((vt2dKalmanBola - acVisao->vtBolaKALMAN.constFirst())/(100/acVisao->vtBolaKALMAN.size()));
            //            acVisao->vSetaVelocidadeBola((vt2dKalmanBola - acVisao->vtBolaKALMAN.constFirst()).length()/(100/acVisao->vtBolaKALMAN.size()));
        }

        environment->vtBolaKALMAN.prepend(vt2dKalmanBola);
        if(environment->vtBolaKALMAN.size() > KALMAN_BUFFER_SIZE)
            environment->vtBolaKALMAN.removeLast();

        kalmanBall.dAngAnterior = kalmanBall.dAngAtual;
    }
}

QVector2D Vision::vt2dCalculaVariancia(QVector<QVector2D> _vtBufferPosicoes)
{
    float fMediaPosX = 0, fMediaPosY = 0;
    float varX = 0.7, varY = 0.7;

    if(_vtBufferPosicoes.size() >= 0)
    {
        foreach(QVector2D pos, _vtBufferPosicoes)//for(int i=0;i<(_iqtdAmostras);i++)
        {
            fMediaPosX += pos.x();
            fMediaPosY += pos.y();
        }

        fMediaPosX = fMediaPosX/_vtBufferPosicoes.size();
        fMediaPosY = fMediaPosY/_vtBufferPosicoes.size();
        varX = varY = 0;

        foreach(QVector2D pos, _vtBufferPosicoes)
        {
            varX += qPow((pos.x() - fMediaPosX),2);
            varY += qPow((pos.y() - fMediaPosY),2);
        }

        varX = varX/(_vtBufferPosicoes.size()-1);
        varY = varY/(_vtBufferPosicoes.size()-1);
    }

    //    std::cout<<"id: "<<_atbRobo.id<<"varX: "<<varX<<" || varY: "<<varY<<std::endl;
    return QVector2D(varX,varY);
}


void Vision::SLOT_connectNetwork(QString ipVisao, int portaVisao, int indiceInterface)
{
    QList<QNetworkInterface> ifInterfaceLocal = QNetworkInterface::allInterfaces();

    udpSSLVision.reset( new QUdpSocket(this) ) ;

    // Coneta-se à porta que envia os pacotes da visão
    udpSSLVision->bind(QHostAddress::AnyIPv4, portaVisao, QUdpSocket::ShareAddress);
    udpSSLVision->setMulticastInterface(ifInterfaceLocal.at(indiceInterface));
    udpSSLVision->joinMulticastGroup(QHostAddress(ipVisao));

    // Conecta o sinal de chegada de pacote ao slot que irá tratá-los
    connect(udpSSLVision.get(), &QUdpSocket::readyRead,
            this, &Vision::SLOT_getPackageFromSocket);

    // Inicializa o timer do Kalman
    tmrKalman.reset( new QTimer(this) );

    tmrKalman->setTimerType(Qt::PreciseTimer);

    // Conecta o slot do Kalman com o timer
    connect(tmrKalman.get(), &QTimer::timeout,
            this, &Vision::SLOT_kalmanFilter);
}

void Vision::SLOT_disconnectNetwork()
{
    // Desconecta-se do sinal de chegada de pacote e fecha a porta de recebimento de dados
    udpSSLVision->disconnect(SIGNAL(readyRead() ) );
    udpSSLVision->close();

    // Desconecta o slot do Kalman
    tmrKalman->stop();
    tmrKalman->disconnect(SIGNAL(timeout() ) );

}

void Vision::SLOT_toggleLocalPlayer(bool bConectar)
{
    if(bConectar){

        tmrKalman.reset( new QTimer(this) );

        tmrKalman->setTimerType(Qt::PreciseTimer);

        // Conecta o slot do Kalman com o timer
        connect(tmrKalman.get(), &QTimer::timeout,
                this, &Vision::SLOT_kalmanFilter);

    }
    else {

        tmrKalman->disconnect(SIGNAL(timeout() ) );

    }
}

void Vision::SLOT_getPackagesFromLocalVision(const QByteArray& data)
{
    SSL_WrapperPacket sslvision;
    sslvision.ParseFromArray(data, data.size());

    vHandleWithVisionPackage(sslvision);

}

void Vision::SLOT_jump(qint64 sleepTime)
{
    iContadorReiniciaVelocidade = static_cast<quint16>(2*sleepTime*1e-3/TIME_LOOP_KALMAN_FILTER);

}

void Vision::SLOT_getPackageFromSocket()
{
    while(udpSSLVision->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udpSSLVision->pendingDatagramSize());
        datagram.fill(0, udpSSLVision->pendingDatagramSize());
        udpSSLVision->readDatagram(datagram.data(), datagram.size());

        SSL_WrapperPacket sslvision;
        sslvision.ParseFromArray(datagram, datagram.size());

        vHandleWithVisionPackage(sslvision);
    }
}



void Vision::SLOT_kalmanFilter()
{
    QElapsedTimer elapsedTempoDeExecucao;
    elapsedTempoDeExecucao.start();

    // Kalman para os robos
    for(int n=0; n < PLAYERS_PER_SIDE; ++n)
    {
        if(environment->rbtBlue[n].isOnField())
            vKalmanRobots(kalmanBlue[n],
                         environment->rbtBlue[n],
                         environment->vtRobosAzuisVisao[n],
                         environment->vtRobosAzuisKALMAN[n],
                         &environment->vtPredicaoAzul[n]);

        if(environment->rbtYellow[n].isOnField())
            vKalmanRobots(kalmanYellow[n],
                         environment->rbtYellow[n],
                         environment->vtRobosAmarelosVisao[n],
                         environment->vtRobosAmarelosKALMAN[n],
                         &environment->vtPredicaoAmarelo[n]);
    }

    // Kalman para bola

    vKalmanBall(environment->bBallOnField());


    emit SIGNAL_sendVisionData(*environment, bMudouGeometria);

    //Assim que chega o pacote de geometria habilita os botoes de comecar jogo e comecar teste
    if(bMudouGeometria == true)
    {
        bMudouGeometria = false;
        emit SIGNAL_fieldSize();
    }

    double tempoTotal = elapsedTempoDeExecucao.nsecsElapsed()*1e-6;

    if(tempoTotal < TIME_LOOP_KALMAN_FILTER)
        return;

    if(tempoTotal < 1.2*TIME_LOOP_KALMAN_FILTER)
        qWarning() << "Tempo Kalman " << QString::number(tempoTotal, 'f', 2) << " ms";
    else
        qCritical() << "Tempo Kalman " << QString::number(tempoTotal, 'f', 2) << " ms";
}


