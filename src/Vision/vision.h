/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef VISION_H
#define VISION_H

#include <QThread>
#include <QtCore>
#include <QDebug>

#include <QNetworkInterface>
#include <QUdpSocket>
#include <QTimer>
#include <QPointer>

#include "Rede/robofeicontroller.h"
#include "Constants/constants.h"
#include "Environment/environment.h"
#include "Vision/Kalman_Filter/kalmanfilter.h"
#include "LogVersion/logversion.h"

class Vision : public QObject
{
    Q_OBJECT

    QScopedPointer<QUdpSocket> udpSSLVision;
    QScopedPointer<QTimer> tmrKalman;
    Environment *environment;
    bool bMudouGeometria;

    KalmanFilter kalmanBall; /**< Objeto do filtro de Kalman para a Bola */
    KalmanFilter kalmanBlue[PLAYERS_PER_SIDE];   /**< Objeto do filtro de Kalman para os robôs Azuis. */
    KalmanFilter kalmanYellow[PLAYERS_PER_SIDE]; /**< Objeto do filtro de Kalman para os robôs oponentes. */

    // Should de removed
    quint16 iContadorReiniciaVelocidade{0};

public:
    Vision();
    ~Vision();

private:
    void vHandleWithVisionPackage(const SSL_WrapperPacket& _pkgUltimoPacoteRecebido);
    void vDetectObjectsOutOfTheField(const SSL_DetectionFrame& frameDeteccao);
    void vDetectObjectsOnTheField(const SSL_DetectionFrame& frameDeteccao);
    void vUpdateBall(const SSL_DetectionFrame& frameDeteccao, int nBola);
    void vUpdateBlueRobot(const SSL_DetectionFrame& frameDeteccao, int nRobo);
    void vUpdateYellowRobot(const SSL_DetectionFrame& frameDeteccao, int nRobo);
    void vUpdateFieldSize(const SSL_GeometryData& frameGeometria);
    void vKalmanRobots(KalmanFilter &kalmanRobos, Robot &rbtRobo, QVector<QVector2D> &vtBufferPosVisao, QVector<QVector2D> &vtBufferPosKalman, QVector<QVector2D> *vtPredicao = nullptr);
    void vKalmanBall(bool _bEstadoBola);
    QVector2D vt2dCalculaVariancia(QVector<QVector2D> _vtBufferPosicoes);

public slots:
    void SLOT_connectNetwork(QString ipVisao, int portaVisao, int indiceInterface);
    void SLOT_disconnectNetwork();

    void SLOT_toggleLocalPlayer(bool bConectar);

    void SLOT_getPackagesFromLocalVision(const QByteArray& data);

    void SLOT_jump(qint64 sleepTime);


private slots:
    void SLOT_getPackageFromSocket();
    void SLOT_kalmanFilter();


signals:
    void SIGNAL_sendVisionData(const Environment& ambiente, bool mudouGeometria);
    void SIGNAL_fieldSize();
};

#endif // VISION_H
