/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef KALMANFILTER_H
#define KALMANFILTER_H

#include <iostream>
#include <QDebug>
#include <QRandomGenerator>
#include <QTimer>
#include <QFileDialog>
#include <QApplication>

#include "eigen3/Eigen/Core"
#include "eigen3/Eigen/LU"      // Eigen p/ matrizes
#include "eigen3/Eigen/Dense"   // Eigen p/ matrizes
#include "qvector2d.h"


/**
 * @brief
 *
 */
class KalmanFilter
{
public:
    /**
     * @brief
     *
     */
    KalmanFilter();

    ~KalmanFilter();

    /**
     * @brief
     *
     * @param _vt2dUltimaPos
     * @param _id
     */
    void vResetKalman(QVector2D _vt2dUltimaPos, int _id = -1);
    /**
     * @brief
     *
     * @param _vt2dPosicaoAtual
     */
    void vAtualizaKalman(QVector2D _vt2dPosicaoAtual);
    /**
     * @brief
     *
     */
    void vPredizKalman();
    /**
     * @brief
     *
     * @param _deltaT
     */
    void vAtualizaMatrizes(double _deltaT);
    /**
     * @brief
     *
     * @param _varX
     * @param _varY
     */
    void vAtualizaVariancia(double _varX, double _varY);

    /**
     * @brief
     *
     * @param _vtKalman
     * @param Eigen::Matrix<double
     * @param _Tp2
     * @param _xPred
     * @param Eigen::Matrix<double
     * @param _Tp2
     * @param _pPred
     * @param _dT
     * @param num
     * @return QVector2D
     */
    QVector2D vt2dPredizFuturo(QVector<QVector2D> _vtKalman, Eigen::Matrix<double, 4, 1> _xPred, Eigen::Matrix<double, 4, 4> _pPred, double _dT, int num);

    // Atributos Kalman ---------------------------------------------------------------
public:
    double posX,posY,velX,velY;         // Variáveis de Estado escolhidas /**< TODO: describe */
    double dt;                          // Variacao do tempo (1/fps) /**< TODO: describe */

    double dAngAtual,dAngAnterior; /**< TODO: describe */

    QVector<QVector2D> vtVelocidade; /**< TODO: describe */
    QVector<QVector2D> vt2dVelKalman; /**< TODO: describe */


    Eigen::Matrix<double,4,1> x;        // Vetor de Estados /**< TODO: describe */
    Eigen::Matrix<double,4,4> P;        // Ganho de Kalman /**< TODO: describe */
    Eigen::Matrix<double,4,1> x_k_1;    // x(k-1) Estado anterior /**< TODO: describe */
    Eigen::Matrix<double,4,4> P_k_1;    // P(k-1) Ganho no tempo anterior /**< TODO: describe */
    Eigen::Matrix<double,4,2> K;       // Kk Ganho de Kalman /**< TODO: describe */

    Eigen::Matrix<double,4,1> xPred;    // Matriz de Transicao /**< TODO: describe */
    Eigen::Matrix<double,4,4> pPred;    // Matriz de Transicao /**< TODO: describe */

    Eigen::Array<double,1,1> varX;      // Variacao da posicao em X /**< TODO: describe */
    Eigen::Array<double,1,1> varY;      // Variacao da posicao em Y /**< TODO: describe */

    Eigen::Matrix<double,4,4> ident;        // identidade /**< TODO: describe */

private:
    Eigen::Matrix<double,4,4> A;    // Matriz de Transicao /**< TODO: describe */
    Eigen::Matrix<double,4,2> B;    // Matriz de Controle /**< TODO: describe */
    Eigen::Matrix<double,4,4> Q;    // Matriz da covariancia do ruido do processo /**< TODO: describe */
    Eigen::Matrix<double,2,2> R;    // Matriz da covariancia do ruida da medicao /**< TODO: describe */
    Eigen::Matrix<double,2,4> H;    // Matriz Jacobiana do modelo /**< TODO: describe */
    Eigen::Matrix<double,2,1> u;    // Matriz dos coeficientes de desaceleracao(atrito) /**< TODO: describe */

    //
    // Atributos Kalman ---------------------------------------------------------------
};

#endif // KALMANFILTER_H
