﻿/*
 * SSL-Strategy
 * Copyright (C) 2020-2021  RoboFEI / Small Size League
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "kalmanfilter.h"

KalmanFilter::KalmanFilter()
{
    dt = 1.0/60.0;

    A << 1,0,dt,0,
         0,1,0,dt,
         0,0,1,0,
         0,0,0,1;

//    std::cout<<"A"<<std::endl;
//    std::cout<<A<<std::endl;
//    std::cout<<" "<<std::endl;


    // eram 0.7 VVVV

    varX << 0.1;
//    std::cout<<"varX "<<varX<<std::endl;
//    std::cout<<" "<<std::endl;

    varY << 0.1;
//    std::cout<<"vary "<<varY<<std::endl;
//    std::cout<<" "<<std::endl;

    u << -0.05,//-0.386,
         -0.05;//-0.386; // era -0.2 tbm kk// era -0.5
//    std::cout<<"u"<<std::endl;
//    std::cout<<u<<std::endl;
//    std::cout<<" "<<std::endl;

    B << (0.5)*(dt*dt) , 0 ,
         0             , (0.5)*(dt*dt) ,
         dt            , 0 ,
         0             , dt;

//    std::cout<<"B"<<std::endl;
//    std::cout<<B<<std::endl;
//    std::cout<<" "<<std::endl;

    Q << (1.0/4)*(pow(dt,4))   , 0                        , (1.0/2)*(pow(dt,3)) , 0 ,
         0                        , (1.0/4)*(pow(dt,4)) , 0                        , (1.0/2)*(pow(dt,3)),
         (1.0/2)*(pow(dt,3))   , 0                        , pow(dt,2)         , 0,
         0                        , (1.0/2)*(pow(dt,3)) , 0                        , pow(dt,2);

//    Q << 0.07   , 0     , 0     , 0 ,
//         0      , 0.07  , 0     , 0 ,
//         0      , 0     , 0.07  , 0 ,
//         0      , 0     , 0     , 0.07;

//    std::cout<<"Q"<<std::endl;
//    std::cout<<Q<<std::endl;
//    std::cout<<" "<<std::endl;


    R <<  0.1 , 0  ,
            0 , 0.1 ;
//    std::cout<<"R"<<std::endl;
//    std::cout<<R<<std::endl;
//    std::cout<<" "<<std::endl;


    H << 1 , 0 , 0 , 0,
         0 , 1 , 0 , 0;
//    std::cout<<"H"<<std::endl;
//    std::cout<<H<<std::endl;
//    std::cout<<" "<<std::endl;

    posX = 0.0;
    posY = 0.0;
    velX = 0.5;
    velY = 0.5;

    P = Q;

    x << posX,posY,velX,velY;
//    std::cout<<"x"<<std::endl;
//    std::cout<<x<<std::endl;
//    std::cout<<" "<<std::endl;


    x_k_1 << 0.0,0.0,0.0,0.0;
//    std::cout<<"x_k_1"<<std::endl;
//    std::cout<<x_k_1<<std::endl;
//    std::cout<<" "<<std::endl;

    P_k_1.setIdentity(4,4);
//    std::cout<<"P_k_1"<<std::endl;
//    std::cout<<P_k_1<<std::endl;
//    std::cout<<" "<<std::endl;

    ident << 1,0,0,0,
             0,1,0,0,
             0,0,1,0,
             0,0,0,1;

    xPred = x;
    pPred = P;

    //    std::cout<<"indet"<<ident<<std::endl;
}

KalmanFilter::~KalmanFilter()
{

}

void KalmanFilter::vResetKalman(QVector2D _vt2dUltimaPos, int _id){

    x(0,0) = (double)_vt2dUltimaPos.x();
    x(1,0) = (double)_vt2dUltimaPos.y();

    QVector2D vtAux;
    double dModulo = sqrt(pow(x(2),2) + pow(x(3),2));

    vtAux.setX(_vt2dUltimaPos.x()-x(0));
    vtAux.setY(_vt2dUltimaPos.y()-x(1));

    x(2)= (vtAux*dModulo).x();
    x(3)= (vtAux*dModulo).y();

    R(0,0) = 1.0;
    R(1,1) = 1.0;

    P = ident;

//    if(_id != -1)
//        std::cout<<" RESET - ID: - "<<_id<<std::endl;
}

void KalmanFilter::vAtualizaMatrizes(double _deltaT)
{
    dt = _deltaT;
    A(0,2) = dt;
    A(1,3) = dt;

    B(0,0) = (0.5)*(dt*dt);
    B(1,1) = (0.5)*(dt*dt);
    B(2,0) = dt;
    B(3,1) = dt;

    Q(0,0) = (1.0/4)*(pow(dt,4));
    Q(0,2) = (1.0/2)*(pow(dt,3));
    Q(1,1) = (1.0/4)*(pow(dt,4));
    Q(1,3) = (1.0/2)*(pow(dt,3));
    Q(2,0) = (1.0/2)*(pow(dt,3));
    Q(2,2) = (pow(dt,2));
    Q(3,1) = (1.0/2)*(pow(dt,3));
    Q(3,3) = (pow(dt,2));

    vtVelocidade.prepend(QVector2D(x(2),x(3)));

//     rever vvv
//    double dModulo = sqrt(pow(x(2),2) + pow(x(3),2));
//    qDebug()<<"vel"<<dModulo/1e3;

//    if(dModulo/1e3 <= 0.2)
//    {
//        u(0) = 0.0;
//        u(1) = 0.0;
//    }
//    else
//    {
        if(vtVelocidade.size() > 1)
        {
            u(0) = (vtVelocidade.at(0).x() - vtVelocidade.at(1).x())*(dt);//-0.05;
            u(1) = (vtVelocidade.at(0).y() - vtVelocidade.at(1).y())*(dt);//-0.05;
        }else {
            u(0) = 0.0;
            u(1) = 0.0;
        }
//    }


    if(vtVelocidade.size() > 2)
        vtVelocidade.pop_back();
}

void KalmanFilter::vAtualizaVariancia(double _varX, double _varY)
{
    R(0,0) = _varX;
    R(1,1) = _varY;
}

QVector2D KalmanFilter::vt2dPredizFuturo(QVector<QVector2D> _vtKalman, Eigen::Matrix<double,4,1> _xPred, Eigen::Matrix<double,4,4> _pPred,double _dT, int num)
{

    Eigen::Matrix<double,4,1> x_k_1_ = xPred,x_k_1_aux;    // x(k-1) Estado anterior
    Eigen::Matrix<double,4,4> P_k_1_ = pPred,P_k_1_aux;    // P(k-1) Ganho no tempo anterior
    Eigen::Matrix<double,4,2> K_ = K;       // Kk Ganho de Kalman

    double dModulo;

    Eigen::Matrix<double,2,2> Sk;
    Eigen::Matrix<double,2,1> zk,yk;

    vAtualizaVariancia(99,99);

    for(int i=1;i<=num;i++)
    {
        dModulo = sqrt(pow(x_k_1_(2),2) + pow(x_k_1_(3),2))/1e3;
        vAtualizaMatrizes(i*_dT);

        if(dModulo*10 > 0.05)
        {
            x_k_1_aux = A*x_k_1_ + B*u;
            P_k_1_aux = (A*P_k_1_)*A.transpose() + Q;

            zk << x_k_1_aux(0), x_k_1_aux(1);

            yk = zk - H*x_k_1_;
            Sk = H*P_k_1_aux*(H.transpose()) + R;
            K_ = P_k_1_aux*(H.transpose())*(Sk.inverse());
            x_k_1_  = x_k_1_aux + K_*yk;
            P_k_1_  = (ident - K_*H)*P_k_1_aux;
        }
        else
        {
            //            QApplication::beep();
            return QVector2D(x_k_1_(0),x_k_1_(1));
        }
    }


//    qDebug()<< "pos : "<<QVector2D(x_k_1_aux(0),x_k_1_aux(1));


//    for(int i=1;i<=num;i++)
//    {
//        dModulo = sqrt(pow(x_k_1_aux(2),2) + pow(x_k_1_aux(3),2))/1e3;
//        vAtualizaMatrizes(i*_dT);

//        if(dModulo*10 > 0.05)
//        {

//            x_k_1_aux = A*xPred + B*u;
//            P_k_1_aux = (A*pPred)*A.transpose() + Q;
//            xPred = x_k_1_aux;
//            pPred = P_k_1_aux;
//        }
//        else
//        {
////            QApplication::beep();
//            xPred(0) = x_k_1_aux(0);
//            xPred(1) = x_k_1_aux(1);
//        }
//    }

    return QVector2D(x_k_1_(0),x_k_1_(1));
}

void KalmanFilter::vAtualizaKalman(QVector2D _vt2dPosicaoAtual)
{    

    Eigen::Matrix<double,2,2> Sk;
    Eigen::Matrix<double,2,1> zk,yk;

    zk << (double)_vt2dPosicaoAtual.x(),(double)_vt2dPosicaoAtual.y();

    yk = zk - H*x_k_1;
    Sk = H*P_k_1*(H.transpose()) + R;
    K = P_k_1*(H.transpose())*(Sk.inverse());
    x  = x_k_1 + K*yk;
    P  = (ident - K*H)*P_k_1;

    if(x(0)+x(1)+x(2)+x(3) > 10e9 || !qIsFinite(x(0)+x(1)+x(2)+x(3)))
    {
        vResetKalman(QVector2D(0,0));
    }

}

void KalmanFilter::vPredizKalman()
{
    x_k_1 = A*x + B*u;
    P_k_1 = (A*P)*A.transpose() + Q;

//    std::cout<< "x_k1 = \n"<<x_k_1<<std::endl;
//    std::cout<< "p_k1 = \n"<<P_k_1<<std::endl;
//    std::cout<< "A = \n"<<A<<std::endl;
//    std::cout<< "B = \n"<<B<<std::endl;

}


