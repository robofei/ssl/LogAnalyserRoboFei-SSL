#QMAKE_CXXFLAGS += -std=gnu++11

#QMAKE_CXXFLAGS += -nostdlib
# 👆 apenas para rodar LOGs de 2018


QT += \
    core \
    gui \
    network \
    widgets

TARGET = LogAnalyserRoboFEI-SSL
TEMPLATE = app

CONFIG += \
    c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32 {
    VCPKG_LOCATION *= "C:/dev/vcpkg/installed/x64-windows"
}

include(include/protofiles/proto_compile.pri)
include(include/LogFile/LogFile.pri)


HEADERS += \
    src/Analysis/analysis.h \
    src/Analysis/analysiscommon.h \
    src/Atributtes/attributes.h \
    src/Common/common.h \
    src/Constants/constants.h \
    src/DrawMap/drawmap.h \
    src/Environment/environment.h \
    src/GameStatistics/gamestatisticsdialog.h \
    src/GameStatistics/teamstatistics.h \
    src/LogPlayer/logplayer.h \
    src/LogVersion/logversion.h \
    src/MirrorLogFiles/mirrorlogfiles.h \
    src/Rede/robofeicontroller.h \
    src/Robot/robot.h \
    src/Vision/Kalman_Filter/kalmanfilter.h \
    src/Vision/vision.h \
    src/interface.h


SOURCES += \
    src/Analysis/analysis.cpp \
    src/Common/common.cpp \
    src/DrawMap/drawmap.cpp \
    src/Environment/environment.cpp \
    src/GameStatistics/gamestatisticsdialog.cpp \
    src/LogPlayer/logplayer.cpp \
    src/MirrorLogFiles/mirrorlogfiles.cpp \
    src/Rede/robofeicontroller.cpp \
    src/Robot/robot.cpp \
    src/Vision/Kalman_Filter/kalmanfilter.cpp \
    src/Vision/vision.cpp \
    src/interface.cpp \
    src/main.cpp


FORMS += \
    src/GameStatistics/gamestatisticsdialog.ui \
    src/MirrorLogFiles/mirrorlogfiles.ui \
    src/interface.ui

unix {
    LIBS += \
        -lprotobuf \
        -lz
}

win32 {
    LIBS += \
        $${VCPKG_LOCATION}/lib/libprotobuf.lib \
        $${VCPKG_LOCATION}/lib/zlib.lib \


    INCLUDEPATH += \
        $${VCPKG_LOCATION}/include

}

INCLUDEPATH += \
    src \
    include

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    share/icon.qrc
